/* modules/m_sasl.c
 *   Copyright (C) 2006 Michael Tharp <gxti@partiallystapled.com>
 *   Copyright (C) 2006 charybdis development team
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1.Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * 2.Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 * 3.The name of the author may not be used to endorse or promote products
 *   derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * $Id$
 */

#include "stdinc.h"
#include "client.h"
#include "irc_string.h"
#include "ircd.h"
#include "numeric.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "s_serv.h"
#include "s_user.h"
#include "hash.h"
#include "conf.h"

static void
mr_authenticate(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *agent_p = NULL;
  struct Client *services;

  /* They really should use CAP for their own sake. */
  if (!HasCap(source_p, CAP_SASL))
    return;

  if(source_p->localClient->sasl_complete)
  {
    sendto_one(source_p, form_str(ERR_SASLALREADY), me.name, EmptyString(source_p->name) ? "*" : source_p->name);
    return;
  }

  if(strlen(parv[1]) > 400)
  {
    sendto_one(source_p, form_str(ERR_SASLTOOLONG), me.name, EmptyString(source_p->name) ? "*" : source_p->name);
    return;
  }

  if (EmptyString(parv[1]) || strchr(parv[1], ' ') != NULL || *parv[1] == ':')
  {
    exit_client(source_p, &me, "Malformed AUTHENTICATE");
    return;
  }

  if (!EmptyString(source_p->id) && !valid_uid(source_p->id))
  {
    exit_client(source_p, &me, "Mixing client and server protocol");
    return;
  }

  services = ConfigFileEntry.sasl_name ? hash_find_client(ConfigFileEntry.sasl_name) : NULL;
  if (services == NULL || !IsClient(services) || !HasFlag(services, FLAGS_SERVICE))
  {
    sendto_one(source_p, form_str(ERR_SASLABORTED), me.name, EmptyString(source_p->name) ? "*" : source_p->name);
    return;
  }

  if (EmptyString(source_p->id))
  {
    const char *id;

    /* Allocate a UID. */
    while (hash_find_id((id = uid_get())) != NULL)
      ;

    strlcpy(source_p->id, id, sizeof(source_p->id));
    hash_add_id(source_p);
  }

  if (!EmptyString(source_p->localClient->sasl_agent))
    agent_p = hash_find_id(source_p->localClient->sasl_agent);

  if(agent_p == NULL)
  {
    sendto_one(services, ":%s ENCAP %s SASL %s * H %s %s", me.id,
               services->servptr->name, source_p->id, source_p->realhost,
               (IsIPSpoof(source_p) && ConfigFileEntry.hide_spoof_ips) ? "0" : source_p->sockhost);

    if (source_p->certfp != NULL)
      sendto_one(services, ":%s ENCAP %s SASL %s * S %s %s", me.id,
                 services->servptr->name, source_p->id, parv[1],
                 source_p->certfp);
    else
      sendto_one(services, ":%s ENCAP %s SASL %s * S %s", me.id,
                 services->servptr->name, source_p->id, parv[1]);
  }
  else
  {
    sendto_one(agent_p, ":%s ENCAP %s SASL %s %s C %s", me.id, agent_p->servptr->name,
        source_p->id, agent_p->id, parv[1]);
  }

  source_p->localClient->sasl_out++;
}

static void
me_sasl(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p, *agent_p;

  /* Let propagate if not addressed to us, or if broadcast.
   * Only SASL agents can answer global requests.
   */
  if(strncmp(parv[2], me.id, IRC_MAXSID))
    return;

  if((target_p = hash_find_id(parv[2])) == NULL)
    return;

  if (!IsUnknown(target_p) || !MyConnect(target_p))
    return;

  if((agent_p = hash_find_id(parv[1])) == NULL)
    return;

  if(source_p != agent_p->servptr) /* WTF?! */
    return;

  /* We only accept messages from services */
  if (!HasFlag(agent_p, FLAGS_SERVICE))
    return;

  if (EmptyString(target_p->localClient->sasl_agent))
    strlcpy(target_p->localClient->sasl_agent, parv[1], sizeof(target_p->localClient->sasl_agent));
  /* Reject if someone has already answered. */
  else if (strcmp(parv[1], target_p->localClient->sasl_agent))
    return;

  if(*parv[3] == 'C')
    sendto_one(target_p, "AUTHENTICATE %s", parv[4]);
  else if(*parv[3] == 'D')
  {
    if(*parv[4] == 'F')
      sendto_one(target_p, form_str(ERR_SASLFAIL), me.name, EmptyString(target_p->name) ? "*" : target_p->name);
    else if(*parv[4] == 'S')
    {
      sendto_one(target_p, form_str(RPL_SASLSUCCESS), me.name, EmptyString(target_p->name) ? "*" : target_p->name);
      target_p->localClient->sasl_complete = 1;
    }
    *target_p->localClient->sasl_agent = '\0'; /* Blank the stored agent so someone else can answer */
  }
  else if(*parv[3] == 'M')
    sendto_one(target_p, form_str(RPL_SASLMECHS), me.name, EmptyString(target_p->name) ? "*" : target_p->name, parv[4]);
}

static void
me_svslogin(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  if (!HasFlag(source_p, FLAGS_SERVICE))
    return;

  struct Client *target_p = hash_find_id(parv[1]);

  if (target_p == NULL || !MyConnect(target_p) || !IsUnknown(target_p))
    return;

  if (valid_hostname(parv[4]))
    strlcpy(target_p->host, parv[4], sizeof(target_p->host));

  strlcpy(target_p->svid, parv[5], sizeof(target_p->svid));
  strlcpy(target_p->suser, parv[5], sizeof(target_p->suser));

  sendto_one(target_p, form_str(RPL_LOGGEDIN), me.name,
             EmptyString(target_p->name) ? "*" : target_p->name,
             EmptyString(target_p->name) ? "*" : target_p->name,
             target_p->username,
             target_p->host,
             target_p->suser, target_p->suser);
}

static struct Message authenticate_msgtab =
{
  .cmd = "AUTHENTICATE",
  .args_min = 2,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = mr_authenticate,
  .handlers[CLIENT_HANDLER] = m_registered,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = m_registered,
};

static struct Message sasl_msgtab =
{
  .cmd = "SASL",
  .args_min = 5,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_ignore,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_sasl,
  .handlers[OPER_HANDLER] = m_ignore,
};

static struct Message svslogin_msgtab =
{
  .cmd = "SVSLOGIN",
  .args_min = 6,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_ignore,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_svslogin,
  .handlers[OPER_HANDLER] = m_ignore,
};

static void
module_init(void)
{
  mod_add_cmd(&authenticate_msgtab);
  mod_add_cmd(&sasl_msgtab);
  mod_add_cmd(&svslogin_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&authenticate_msgtab);
  mod_del_cmd(&sasl_msgtab);
  mod_del_cmd(&svslogin_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};

