/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "client.h"
#include "irc_string.h"
#include "ircd.h"
#include "numeric.h"
#include "restart.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "conf.h"


static void
mo_upgrade(struct Client *client_p, struct Client *source_p,
           int parc, char *parv[])
{
  if (!HasOFlag(source_p, OPER_FLAG_RESTART))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVS), me.name,
               source_p->name, "restart");
    return;
  }

  if (EmptyString(parv[1]))
  {
    sendto_one(source_p, ":%s NOTICE %s :Need server name /upgrade %s",
               me.name, source_p->name, me.name);
    return;
  }

  if (irccmp(parv[1], me.name))
  {
    sendto_one(source_p, ":%s NOTICE %s :Mismatch on /upgrade %s",
               me.name, source_p->name, me.name);
    return;
  }

#ifdef HAVE_LIBJANSSON
  sendto_snomask(SNO_ALL, L_ALL, "Received UPGRADE command from %s", get_oper_name(source_p));

  server_upgrade();
#else
  sendto_one(source_p, ":%s NOTICE %s :Unable to upgrade, libplexus is not compiled with JSON support.",
             me.name, source_p->name);
#endif
}

static struct Message upgrade_msgtab =
{
  .cmd = "UPGRADE",
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_not_oper,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = mo_upgrade,
};

static void
module_init(void)
{
  mod_add_cmd(&upgrade_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&upgrade_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
