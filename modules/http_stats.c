/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "conf.h"
#include "modules.h"
#include "resv.h"
#include "whowas.h"
#include "watch.h"
#include "httpd.h"
#include "ircd.h"
#include "s_serv.h"
#include "listener.h"

#ifdef HAVE_LIBJANSSON
#ifdef HAVE_LIBMICROHTTPD

#include <jansson.h>

static int
http_stats_memory(struct MHD_Connection *connection, const char *url,
                  const char *method, const char *version, const char *upload_data,
                  size_t *upload_data_size)
{
  const dlink_node *gptr = NULL;
  const dlink_node *dlink = NULL;

  unsigned int local_client_conf_count = 0;      /* local client conf links */
  unsigned int users_counted = 0;                /* user structs */

  unsigned int channel_members = 0;
  unsigned int channel_invites = 0;
  unsigned int channel_bans = 0;
  unsigned int channel_except = 0;
  unsigned int channel_invex = 0;

  unsigned int wwu = 0;                  /* whowas users */
  unsigned int class_count = 0;          /* classes */
  unsigned int aways_counted = 0;
  unsigned int number_ips_stored;        /* number of ip addresses hashed */

  uint64_t channel_memory = 0;
  uint64_t channel_invite_memory = 0;
  uint64_t channel_ban_memory = 0;
  uint64_t channel_except_memory = 0;
  uint64_t channel_invex_memory = 0;

  unsigned int safelist_count = 0;
  uint64_t safelist_memory = 0;

  uint64_t wwm = 0;               /* whowas array memory used       */
  uint64_t mem_ips_stored;        /* memory used by ip address hash */

  uint64_t total_channel_memory = 0;
  uint64_t totww = 0;

  unsigned int local_client_count  = 0;
  unsigned int remote_client_count = 0;

  uint64_t local_client_memory_used  = 0;
  uint64_t remote_client_memory_used = 0;

  uint64_t total_memory = 0;
  unsigned int topic_count = 0;

  unsigned int watch_list_headers = 0;   /* watchlist headers     */
  unsigned int watch_list_entries = 0;   /* watchlist entries     */
  uint64_t watch_list_memory = 0; /* watchlist memory used */


  DLINK_FOREACH(gptr, global_client_list.head)
  {
    struct Client *target_p = gptr->data;

    if (MyConnect(target_p))
    {
      ++local_client_count;
      local_client_conf_count += dlink_list_length(&target_p->localClient->confs);
      watch_list_entries += dlink_list_length(&target_p->localClient->watches);
    }
    else
      ++remote_client_count;

    if (IsClient(target_p))
    {
      ++users_counted;

      if (target_p->away[0])
        ++aways_counted;
    }
  }

  /* Count up all channels, ban lists, except lists, Invex lists */
  channel_memory = dlink_list_length(&global_channel_list) *
                   sizeof(struct Channel);
  DLINK_FOREACH(gptr, global_channel_list.head)
  {
    const struct Channel *chptr = gptr->data;

    channel_members += dlink_list_length(&chptr->members);

    channel_invites += dlink_list_length(&chptr->invites);
    channel_invite_memory += dlink_list_length(&chptr->invites) * sizeof(struct Invite);

    if (chptr->topic[0])
      ++topic_count;

    channel_bans += dlink_list_length(&chptr->banlist);
    channel_ban_memory += dlink_list_length(&chptr->banlist) * sizeof(struct Ban);

    channel_except += dlink_list_length(&chptr->exceptlist);
    channel_except_memory += dlink_list_length(&chptr->exceptlist) * sizeof(struct Ban);

    channel_invex += dlink_list_length(&chptr->invexlist);
    channel_invex_memory += dlink_list_length(&chptr->invexlist) * sizeof(struct Ban);
  }

  if ((safelist_count = dlink_list_length(&listing_client_list)))
  {
    safelist_memory = safelist_count * sizeof(struct ListTask);
    DLINK_FOREACH(gptr, listing_client_list.head)
    {
      const struct Client *acptr = gptr->data;

      DLINK_FOREACH(dlink, acptr->localClient->list_task->show_mask.head)
        safelist_memory += strlen(dlink->data);

      DLINK_FOREACH(dlink, acptr->localClient->list_task->hide_mask.head)
        safelist_memory += strlen(dlink->data);
    }
  }

  /* count up all classes */
  class_count = dlink_list_length(class_get_list());

  whowas_count_memory(&wwu, &wwm);
  watch_count_memory(&watch_list_headers, &watch_list_memory);
  count_ip_hash(&number_ips_stored, &mem_ips_stored);

  total_channel_memory = channel_memory
                         + channel_ban_memory
                         + channel_except_memory
                         + channel_invex_memory
                         + channel_members * sizeof(struct Membership)
                         + channel_invite_memory;

  totww = wwu * sizeof(struct Client) + wwm;
  total_memory = totww + total_channel_memory + class_count *
                 sizeof(struct ClassItem);

  local_client_memory_used = local_client_count*(sizeof(struct Client) + sizeof(struct LocalUser));
  total_memory += local_client_memory_used;

  remote_client_memory_used = remote_client_count * sizeof(struct Client);
  total_memory += remote_client_memory_used;


  json_t *root = json_object();

  json_t *watch = json_object();
  json_object_set_new(watch, "headers", json_integer(watch_list_headers));
  json_object_set_new(watch, "entries", json_integer(watch_list_entries));
  json_object_set_new(root, "watch", watch);

  json_object_set_new(root, "clients", json_integer(users_counted));
  json_object_set_new(root, "local clients", json_integer(local_client_count));
  json_object_set_new(root, "remote clients", json_integer(remote_client_count));

  json_object_set_new(root, "user aways", json_integer(aways_counted));

  json_object_set_new(root, "attached confs", json_integer(local_client_conf_count));

  json_t *resvs = json_object();
  json_object_set_new(resvs, "channels", json_integer(dlink_list_length(&cresv_items)));
  json_object_set_new(resvs, "nicks", json_integer(dlink_list_length(&nresv_items)));
  json_object_set_new(root, "resvs", resvs);

  json_object_set_new(root, "classes", json_integer(class_count));

  json_t *channels = json_object();
  json_object_set_new(channels, "channels", json_integer(dlink_list_length(&global_channel_list)));
  json_object_set_new(channels, "topics", json_integer(topic_count));
  json_object_set_new(channels, "bans", json_integer(channel_bans));
  json_object_set_new(channels, "exceptions", json_integer(channel_except));
  json_object_set_new(channels, "invex", json_integer(channel_invex));
  json_object_set_new(channels, "members", json_integer(channel_members));
  json_object_set_new(channels, "invites", json_integer(channel_invites));
  json_object_set_new(root, "channels", channels);

  json_object_set_new(root, "safelist", json_integer(safelist_count));

  json_object_set_new(root, "whowas", json_integer(wwu));

  json_object_set_new(root, "iphash", json_integer(number_ips_stored));

  json_t *memory = json_object();
  json_object_set_new(memory, "watch list headers", json_integer(watch_list_memory));
  json_object_set_new(memory, "watch list entries", json_integer(watch_list_entries * sizeof(dlink_node) * 2));
  json_object_set_new(memory, "clients", json_integer(users_counted * sizeof(struct Client)));
  json_object_set_new(memory, "confs", json_integer((unsigned long long)(local_client_conf_count * sizeof(dlink_node))));
  json_object_set_new(memory, "resv channels", json_integer(dlink_list_length(&cresv_items) * sizeof(struct MaskItem)));
  json_object_set_new(memory, "resv nicks", json_integer(dlink_list_length(&nresv_items) * sizeof(struct MaskItem)));
  json_object_set_new(memory, "classes", json_integer((unsigned long long)(class_count * sizeof(struct ClassItem))));
  json_object_set_new(memory, "channels", json_integer(channel_memory));
  json_object_set_new(memory, "topics", json_integer(topic_count * (TOPICLEN + 1 + USERHOST_REPLYLEN)));
  json_object_set_new(memory, "bans", json_integer(channel_ban_memory));
  json_object_set_new(memory, "exceptions", json_integer(channel_except_memory));
  json_object_set_new(memory, "invex", json_integer(channel_invex_memory));
  json_object_set_new(memory, "members", json_integer((unsigned long long)(channel_members * sizeof(struct Membership))));
  json_object_set_new(memory, "invites", json_integer(channel_invite_memory));
  json_object_set_new(memory, "safelist", json_integer(safelist_memory));
  json_object_set_new(memory, "whowas", json_integer(totww));
  json_object_set_new(memory, "total_channel", json_integer(total_channel_memory));
  json_object_set_new(memory, "local clients", json_integer(local_client_memory_used));
  json_object_set_new(memory, "remote clients", json_integer(remote_client_memory_used));
  json_object_set_new(memory, "total", json_integer(total_memory));
  json_object_set_new(root, "memory", memory);

  json_t *pools = json_array();
  for (mp_pool_t *mpool = mp_get_pools(); mpool; mpool = mpool->next)
  {
    json_t *pool = json_object();
    json_object_set_new(pool, "name", json_string(mpool->name));
    json_object_set_new(pool, "item alloc size", json_integer(mpool->item_alloc_size));

    json_t *empty_chunks = json_array();
    for (struct mp_chunk_t *chunk = mpool->empty_chunks; chunk; chunk = chunk->next)
    {
      json_t *c = json_object();
      json_object_set_new(c, "items allocated", json_integer(chunk->n_allocated));
      json_object_set_new(c, "mem size", json_integer(chunk->mem_size));
      json_array_append_new(empty_chunks, c);
    }
    json_object_set_new(pool, "empty chunks", empty_chunks);

    json_t *used_chunks = json_array();
    for (struct mp_chunk_t *chunk = mpool->used_chunks; chunk; chunk = chunk->next)
    {
      json_t *c = json_object();
      json_object_set_new(c, "items allocated", json_integer(chunk->n_allocated));
      json_object_set_new(c, "mem size", json_integer(chunk->mem_size));
      json_array_append_new(used_chunks, c);
    }
    json_object_set_new(pool, "used chunks", used_chunks);

    json_t *full_chunks = json_array();
    for (struct mp_chunk_t *chunk = mpool->full_chunks; chunk; chunk = chunk->next)
    {
      json_t *c = json_object();
      json_object_set_new(c, "items allocated", json_integer(chunk->n_allocated));
      json_object_set_new(c, "mem size", json_integer(chunk->mem_size));
      json_array_append_new(full_chunks, c);
    }
    json_object_set_new(pool, "full chunks", full_chunks);

    json_array_append_new(pools, pool);
  }
  json_object_set_new(root, "pools", pools);

  const char *dump = json_dumps(root, JSON_INDENT(2));

  json_decref(root);
  root = NULL;

  if (dump == NULL)
    return MHD_NO;

  // this frees() dump
  struct MHD_Response *response = MHD_create_response_from_buffer(strlen(dump), (void *) dump, MHD_RESPMEM_MUST_FREE);
  int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
  MHD_destroy_response(response);

  return ret;
}

static int
http_stats_links(struct MHD_Connection *connection, const char *url,
                  const char *method, const char *version, const char *upload_data,
                  size_t *upload_data_size)
{
  dlink_node *ptr = NULL;

  json_t *root = json_array();

  DLINK_FOREACH(ptr, serv_list.head)
  {
    struct Client *target_p = ptr->data;

    json_t *link = json_object();
    json_object_set_new(link, "name", json_string(target_p->name));
    json_object_set_new(link, "sendq length", json_integer(dbuf_length(&target_p->localClient->buf_sendq)));
    json_object_set_new(link, "sent messages", json_integer(target_p->localClient->send.messages));
    json_object_set_new(link, "sent bytes", json_integer(target_p->localClient->send.bytes));
    json_object_set_new(link, "recv messages", json_integer(target_p->localClient->recv.messages));
    json_object_set_new(link, "recv bytes", json_integer(target_p->localClient->recv.bytes));
    json_object_set_new(link, "connected for", json_integer(CurrentTime - target_p->localClient->firsttime));
    json_object_set_new(link, "since", json_integer(CurrentTime - target_p->localClient->since));
    json_object_set_new(link, "capabilities", json_string(show_capabilities(target_p)));
    json_array_append_new(root, link);
  }

  const char *dump = json_dumps(root, JSON_INDENT(2));

  json_decref(root);
  root = NULL;

  if (dump == NULL)
    return MHD_NO;

  struct MHD_Response *response = MHD_create_response_from_buffer(strlen(dump), (void *) dump, MHD_RESPMEM_MUST_FREE);
  int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
  MHD_destroy_response(response);

  return ret;
}

static void
recurse_report_message(json_t *root, const struct MessageTree *msgtree)
{
  if (msgtree->msg)
  {
    json_t *object = json_object();
    json_object_set_new(object, "name", json_string(msgtree->msg->cmd));
    json_object_set_new(object, "count", json_integer(msgtree->msg->count));
    json_object_set_new(object, "bytes", json_integer(msgtree->msg->bytes));
    json_object_set_new(object, "rcount", json_integer(msgtree->msg->rcount));
    json_array_append_new(root, object);
  }

  for (int i = 0; i < MAXPTRLEN; ++i)
    if (msgtree->pointers[i] != NULL)
      recurse_report_message(root, msgtree->pointers[i]);
}

static int
http_stats_messages(struct MHD_Connection *connection, const char *url,
                  const char *method, const char *version, const char *upload_data,
                  size_t *upload_data_size)
{
  json_t *root = json_array();

  recurse_report_message(root, &msg_tree);

  const char *dump = json_dumps(root, JSON_INDENT(2));

  json_decref(root);
  root = NULL;

  if (dump == NULL)
    return MHD_NO;

  struct MHD_Response *response = MHD_create_response_from_buffer(strlen(dump), (void *) dump, MHD_RESPMEM_MUST_FREE);
  int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
  MHD_destroy_response(response);

  return ret;
}

static int
http_stats_listeners(struct MHD_Connection *connection, const char *url,
                     const char *method, const char *version, const char *upload_data,
                     size_t *upload_data_size)
{
  dlink_list *listeners = get_listeners();
  dlink_node *ptr;

  if (listeners == NULL)
    return MHD_NO;

  json_t *root = json_array();

  DLINK_FOREACH(ptr, listeners->head)
  {
    struct Listener *listener = ptr->data;

    json_t *object = json_object();
    json_object_set_new(object, "name", json_string(listener->name));
    json_object_set_new(object, "port", json_integer(listener->port));
    json_object_set_new(object, "ref_count", json_integer(listener->ref_count));
    if (listener->flags & LISTENER_HIDDEN)
      json_object_set_new(object, "hidden",  json_true());
    if (listener->flags & LISTENER_SERVER)
      json_object_set_new(object, "server", json_true());
    if (listener->flags & LISTENER_SSL)
      json_object_set_new(object, "ssl", json_true());
    if (listener->flags & LISTENER_HTTP)
      json_object_set_new(object, "http", json_true());
    json_array_append_new(root, object);
  }

  const char *dump = json_dumps(root, JSON_INDENT(2));

  json_decref(root);
  root = NULL;

  if (dump == NULL)
    return MHD_NO;

  struct MHD_Response *response = MHD_create_response_from_buffer(strlen(dump), (void *) dump, MHD_RESPMEM_MUST_FREE);
  int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
  MHD_destroy_response(response);

  return ret;
}

static void
ltrace_list(json_t *root, dlink_list *list)
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, list->head)
  {
    struct Client *target_p = ptr->data;

    json_t *object = json_object();
    json_object_set_new(object, "name", json_string(get_client_name(target_p, SHOW_IP)));
    json_object_set_new(object, "sendq_length", json_integer(dbuf_length(&target_p->localClient->buf_sendq)));
    json_object_set_new(object, "send.messages", json_integer(target_p->localClient->send.messages));
    json_object_set_new(object, "send.kbytes", json_integer(target_p->localClient->send.bytes>>10));
    json_object_set_new(object, "recv.messages", json_integer(target_p->localClient->recv.messages));
    json_object_set_new(object, "recv.kbytes", json_integer(target_p->localClient->recv.bytes>>10));
    json_object_set_new(object, "firstime", json_integer(target_p->localClient->firsttime));
    json_object_set_new(object, "since", json_integer(target_p->localClient->since));
    json_array_append_new(root, object);
  }
}

static int
http_stats_ltrace(struct MHD_Connection *connection, const char *url,
                     const char *method, const char *version, const char *upload_data,
                     size_t *upload_data_size)
{
  json_t *root = json_array();
  ltrace_list(root, &unknown_list);
  ltrace_list(root, &local_client_list);
  ltrace_list(root, &serv_list);

  const char *dump = json_dumps(root, JSON_INDENT(2));

  json_decref(root);
  root = NULL;

  if (dump == NULL)
    return MHD_NO;

  struct MHD_Response *response = MHD_create_response_from_buffer(strlen(dump), (void *) dump, MHD_RESPMEM_MUST_FREE);
  int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
  MHD_destroy_response(response);

  return ret;
}

static struct HttpResource stats_http_memory =
{
  .method = MHD_HTTP_METHOD_GET,
  .url = "/stats/memory",
  .handler = http_stats_memory
};

static struct HttpResource stats_http_links =
{
  .method = MHD_HTTP_METHOD_GET,
  .url = "/stats/links",
  .handler = http_stats_links
};

static struct HttpResource stats_http_messages =
{
  .method = MHD_HTTP_METHOD_GET,
  .url = "/stats/messages",
  .handler = http_stats_messages
};

static struct HttpResource stats_http_listeners =
{
  .method = MHD_HTTP_METHOD_GET,
  .url = "/stats/listeners",
  .handler = http_stats_listeners
};

static struct HttpResource stats_http_ltrace =
{
  .method = MHD_HTTP_METHOD_GET,
  .url = "/stats/ltrace",
  .handler = http_stats_ltrace
};

#endif
#endif

static void
module_init(void)
{
#ifdef HAVE_LIBJANSSON
#ifdef HAVE_LIBMICROHTTPD
  httpd_register(&stats_http_memory);
  httpd_register(&stats_http_links);
  httpd_register(&stats_http_messages);
  httpd_register(&stats_http_listeners);
  httpd_register(&stats_http_ltrace);
#endif
#endif
}

static void
module_exit(void)
{
#ifdef HAVE_LIBJANSSON
#ifdef HAVE_LIBMICROHTTPD
  httpd_unregister(&stats_http_memory);
  httpd_unregister(&stats_http_links);
  httpd_unregister(&stats_http_messages);
  httpd_unregister(&stats_http_listeners);
  httpd_unregister(&stats_http_ltrace);
#endif
#endif
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
