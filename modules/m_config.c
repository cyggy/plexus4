/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2017 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "parse.h"
#include "modules.h"
#include "conf.h"
#include "memory.h"
#include "hook.h"
#include "send.h"
#include "cloak.h"
#include "irc_string.h"

static void me_config(struct Client *client_p, struct Client *source_p, int parc, char *parv[]);

static void
config_cloak_key(struct Client *source_p, const char *value)
{
  if (ConfigFileEntry.cloak_key != NULL)
  {
    if (cloak_key != NULL && value != NULL && strcmp(cloak_key, value))
    {
      sendto_snomask(SNO_ALL, L_ALL, "Cloak key for link %s differs from ours!", source_p->name);
    }

    return; // don't update cloak key if we have one in our config
  }

  MyFree(cloak_key);
  cloak_key = NULL;

  if (!EmptyString(value))
  {
    cloak_key = xstrdup(value);
  }
}

struct ConfigSettings
{
  const char *name;
  void (*handler)(struct Client *source_p, const char *value);
};

static struct ConfigSettings settings[] = {
  {
    .name = "cloak_key",
    .handler = config_cloak_key
  },
  { NULL, NULL }
};

static void
ms_config(struct Client *client_p, struct Client *source_p,
          int parc, char *parv[])
{
  const char *setting = parv[1];
  const char *value = parv[2];

  me_config(client_p, source_p, parc, parv);

  sendto_server(client_p, NOCAPS, NOCAPS,
      ":%s CONFIG %s :%s",
      source_p->name, setting, value);
}

static void
me_config(struct Client *client_p, struct Client *source_p,
          int parc, char *parv[])
{
  const char *setting = parv[1];
  const char *value = parv[2];
  const struct ConfigSettings *tab = settings;

  for (; tab->handler; ++tab)
  {
    if (!strcasecmp(setting, tab->name))
    {
      tab->handler(source_p, value);
      break;
    }
  }
}

static void
on_pre_server_burst(struct pre_server_burst_data *data)
{
  struct Client *client_p = data->client;

  if (ConfigFileEntry.cloak_key)
  {
    sendto_one(client_p, ":%s CONFIG cloak_key :%s", me.name, ConfigFileEntry.cloak_key);
  }
}

static struct Message config_msgtab =
{
  .cmd = "CONFIG",
  .args_min = 3,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = ms_config,
  .handlers[ENCAP_HANDLER] = me_config,
  .handlers[OPER_HANDLER] = m_ignore,
};

static struct Event pre_server_burst_event =
{
  .event = &pre_server_burst_hook,
  .handler = on_pre_server_burst
};

static void
module_init(void)
{
  mod_add_cmd(&config_msgtab);
  hook_add(&pre_server_burst_event);
}

static void
module_exit(void)
{
  mod_del_cmd(&config_msgtab);
  hook_del(&pre_server_burst_event);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
