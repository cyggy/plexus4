/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2015-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "client.h"
#include "irc_string.h"
#include "modules.h"
#include "memory.h"
#include "hook.h"
#include "extban.h"
#include "channel_mode.h"

static enum extban_match
extban_account_matches(struct Client *client_p, struct Ban *ban)
{
  return (!EmptyString(client_p->suser) && !match(ban->host, client_p->suser)) ? EXTBAN_MATCH : EXTBAN_NO_MATCH;
}

static struct Extban extban_account =
{
  .character = 'a',
  .type = EXTBAN_MATCHING,
  .types = CHFL_BAN | CHFL_EXCEPTION | CHFL_INVEX,
  .matches = extban_account_matches
};

static void
module_init(void)
{
  extban_add(&extban_account);
}

static void
module_exit(void)
{
  extban_del(&extban_account);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
