/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_rehash.c
 * \brief Includes required functions for processing the REHASH command.
 * \version $Id$
 */

#include "stdinc.h"
#include "client.h"
#include "irc_string.h"
#include "ircd.h"
#include "list.h"
#include "numeric.h"
#include "irc_res.h"
#include "conf.h"
#include "log.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "motd.h"


/*
 * mo_rehash - REHASH message handler
 *
 */
static void
mo_rehash(struct Client *client_p, struct Client *source_p,
          int parc, char *parv[])
{
  int found = 0;

  if (!HasOFlag(source_p, OPER_FLAG_REHASH))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVS),
               me.name, source_p->name, "rehash");
    return;
  }

  if (!EmptyString(parv[1]))
  {
    if (irccmp(parv[1], "DNS") == 0)
    {
      sendto_one(source_p, form_str(RPL_REHASHING), me.name, source_p->name, "DNS");
      sendto_snomask(SNO_ALL, L_ALL | SNO_ROUTE,
                     "%s is rehashing DNS",
                     get_oper_name(source_p));
      stop_resolver();
      init_resolver();
      found = 1;
    }
    else if (irccmp(parv[1], "MOTD") == 0)
    {
      sendto_snomask(SNO_ALL, L_ALL | SNO_ROUTE,
                     "%s is forcing re-reading of MOTD files",
                     get_oper_name(source_p));
      motd_recache();
      found = 1;
    }
    else if (irccmp(parv[1], "FDLIMIT") == 0)
    {
      int old;
      sendto_snomask(SNO_ALL, L_ALL | SNO_ROUTE,
                     "%s is updating FDLIMIT",
                     get_oper_name(source_p));
      old = hard_fdlimit;
      set_fdlimit();
      found = 1;
      sendto_one(source_p, ":%s NOTICE %s :FDLIMIT rehashed from %d to %d", me.name, source_p->name, old, hard_fdlimit);
    }

    if (found)
      ilog(LOG_TYPE_IRCD, "REHASH %s From %s",
           parv[1], get_oper_name(source_p));
    else
      sendto_one(source_p, ":%s NOTICE %s :%s is not a valid option. "
                 "Choose from DNS, MOTD",
                 me.name, source_p->name, parv[1]);
  }
  else
  {
    sendto_one(source_p, form_str(RPL_REHASHING),
               me.name, source_p->name, ConfigFileEntry.configfile);
    sendto_snomask(SNO_ALL, L_ALL | SNO_ROUTE,
                   "%s is rehashing configuration file(s)",
                   get_oper_name(source_p));
    ilog(LOG_TYPE_IRCD, "REHASH From %s",
         get_oper_name(source_p));
    rehash(0);
  }
}

static struct Message rehash_msgtab =
{
  .cmd = "REHASH",
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_not_oper,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = mo_rehash,
};

static void
module_init(void)
{
  mod_add_cmd(&rehash_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&rehash_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
