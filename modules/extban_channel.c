/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2015-2016 plexus development team
 *  Copyright (c) 2019-2020 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
 *  USA
 */


#include "stdinc.h"
#include "list.h"
#include "channel.h"
#include "channel_mode.h"
#include "client.h"
#include "hash.h"
#include "extban.h"
#include "hook.h"
#include "memory.h"
#include "modules.h"

static enum extban_match
extban_channel_matches(struct Client *client, struct Channel *channel, struct Ban *ban)
{
  const char *name = ban->host;
  unsigned int flags;

  switch (*name)
  {
    case '+':
      flags = CHFL_CHANOP | CHFL_HALFOP | CHFL_VOICE;
      ++name;
      break;
    case '%':
      flags = CHFL_HALFOP | CHFL_VOICE;
      ++name;
      break;
    case '@':
      flags = CHFL_CHANOP;
      ++name;
      break;
    default:
      flags = 0;
  }

  struct Channel *tmp = hash_find_channel(name);
  if (tmp == NULL)
    return EXTBAN_NO_MATCH;

  /*
   * If the channel in question is either +s, or +p, only allow a match against
   * the source channel to prevent channel probing.
   */
  if (!PubChannel(tmp) && tmp != channel)
    return EXTBAN_NO_MATCH;

  struct Membership *member = find_channel_link(client, tmp);
  if (member)
  {
    if (flags && (member->flags & flags) == 0)
      return EXTBAN_NO_MATCH;
    return EXTBAN_MATCH;
  }

  return EXTBAN_NO_MATCH;
}

struct Extban extban_channel =
{
  .character = 'c',
  .type = EXTBAN_MATCHING,
  .types = CHFL_BAN | CHFL_EXCEPTION | CHFL_INVEX,
  .matches = extban_channel_matches
};

static void
module_init(void)
{
  extban_add(&extban_channel);
}

static void
module_exit(void)
{
  extban_del(&extban_channel);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};