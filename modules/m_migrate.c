/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "client.h"
#include "irc_string.h"
#include "ircd.h"
#include "numeric.h"
#include "restart.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "conf.h"
#include "s_serv.h"
#include "upgrade.h"
#include "log.h"
#include "hash.h"
#include "listener.h"
#include "s_bsd.h"
#include "packet.h"

static void
mr_migrate(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  const char *subcmd = parv[1];
  const char *server;
  struct MaskItem *conf;
  struct Client *target;

  /* this has to be accepted by us */
  if (!IsListenerServer(client_p->localClient->listener))
  {
    exit_client(source_p, &me, "Invalid migration");
    return;
  }

  if (strcmp(subcmd, "RESUME"))
  {
    exit_client(source_p, &me, "Invalid migration");
    return;
  }

  server = parv[2];
  if (EmptyString(server))
  {
    exit_client(source_p, &me, "Invalid migration");
    return;
  }

  target = hash_find_server(server);
  if (target == NULL || !MyConnect(target) || IsMe(target))
  {
    exit_client(source_p, &me, "Invalid migration");
    return;
  }

  if (!HasSFlag(target, SERVER_FLAGS_MIGRATING))
  {
    exit_client(source_p, &me, "Invalid migration");
    return;
  }

  conf = find_matching_name_conf(CONF_SERVER, target->name, NULL, NULL, 0);
  if (!conf)
  {
    sendto_snomask(SNO_LINK, L_ALL, "Invalid migration for %s - no conf", target->name);
    exit_client(source_p, &me, "Invalid migration - no conf");
    return;
  }

  if (match(conf->host, source_p->host) && match(conf->host, source_p->sockhost))
  {
    sendto_snomask(SNO_LINK, L_ALL, "Invalid migration for %s - invalid IP", target->name);
    exit_client(source_p, &me, "Invalid migration - invalid IP");
    return;
  }

  fd_swap(&source_p->localClient->fd, &target->localClient->fd);

  /* swap any pending recvq data out of the client's recvq. target's recvq should be empty,
   * if it isn't it means that something came in after the initial migration, which is their
   * problem and we should ignore it (or quit them?)
   */
  dbuf_swap(&source_p->localClient->buf_recvq, &target->localClient->buf_recvq);

  /* sendq should be empty unless they performed the migration prior to us flushing the ack,
   * which is bad, but nothing to assert over, so append migrate queue
   */
  dbuf_append_queue(&target->localClient->buf_sendq, &target->serv->migrate_sendq);

  /* exit old server */
  exit_client(source_p, &me, "Migration complete");

  /* they are no longer migrating */
  DelSFlag(target, SERVER_FLAGS_MIGRATING);
  /* unfreeze writes */
  DelSFlag(target, SERVER_FLAGS_MIGRATING_QUEUE_WRITE);
  /* unfreeze read */
  DelSFlag(target, SERVER_FLAGS_MIGRATING_BLOCK_READ);

  /* poll for read and write and update associated data with fds */
  comm_setselect(&target->localClient->fd, COMM_SELECT_READ, read_packet, target);
  comm_setselect(&target->localClient->fd, COMM_SELECT_WRITE, (PF *) sendq_unblocked, target);

  sendto_snomask(SNO_ALL, L_ALL, "Migration for %s complete", target->name);
}

static void
mo_migrate(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  if (!HasUMode(source_p, UMODE_ROUTING))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVS), me.name,
               source_p->name, "migrate");
    return;
  }

  if (EmptyString(parv[1]))
  {
    sendto_one(source_p, ":%s NOTICE %s :Need server name /migrate %s",
               me.name, source_p->name, me.name);
    return;
  }

  if (irccmp(parv[1], me.name))
  {
    sendto_one(source_p, ":%s NOTICE %s :Mismatch on /migrate %s",
               me.name, source_p->name, me.name);
    return;
  }

  if (dlink_list_length(&serv_list) == 0)
  {
    sendto_one(source_p, ":%s NOTICE %s :Unable to migrate, I have no links",
               me.name, source_p->name);
    return;
  }

#ifndef HAVE_LIBJANSSON
  sendto_one(source_p, ":%s NOTICE %s :Unable to migrate, no JSON support",
             me.name, source_p->name);
  return;
#endif

  if (HasSFlag(&me, SERVER_FLAGS_MIGRATING))
  {
    sendto_one(source_p, ":%s NOTICE %s :I am already migrating",
               me.name, source_p->name);
    return;
  }

  dlink_node *ptr, *ptr_next;
  DLINK_FOREACH(ptr, serv_list.head)
  {
    struct Client *target = ptr->data;

    if (!IsCapable(target, CAP_MIGRATE) || !IsCapable(target, CAP_TS6))
    {
      sendto_one(source_p, ":%s NOTICE %s :Unable to migrate, %s does not support MIGRATE and/or TS6",
                 me.name, source_p->name, target->name);
      return;
    }
  }

  sendto_snomask(SNO_ALL, L_ALL, "Received MIGRATE command from %s", get_oper_name(source_p));

  /* no new connections */
  close_listeners();

  /* exit clients */
  DLINK_FOREACH_SAFE(ptr, ptr_next, local_client_list.head)
  {
    struct Client *target = ptr->data;

    if (!MyClient(target) && !IsUnknown(target))
      continue;

    exit_client(target, &me, "Server migration in progress");
  }

  sendto_server(NULL, CAP_MIGRATE | CAP_TS6, NOCAPS, ":%s MIGRATE START", ID(&me));

  AddSFlag(&me, SERVER_FLAGS_MIGRATING);

  DLINK_FOREACH(ptr, serv_list.head)
  {
    struct Client *target = ptr->data;
    /* do not send anymore */
    AddSFlag(target, SERVER_FLAGS_MIGRATING_QUEUE_WRITE);
  }
}

static void
do_migrate()
{
#ifdef HAVE_LIBJANSSON
  unlink(pidFileName);

  FILE *script = popen(LIBEXECPATH "/migrate.sh", "w");
  if (script == NULL)
  {
    server_die("Unable to execute migration script", 0);
    return;
  }

  json_t *state = upgrade_serialize(1);
  if (state == NULL)
  {
    pclose(script);
    server_die("Unable to serialize state for migration", 0);
    return;
  }

  int ret = json_dumpf(state, script, JSON_COMPACT | JSON_ENSURE_ASCII);
  if (ret == -1)
  {
    json_decref(state);
    pclose(script);
    server_die("Unable to flush state over pipe", 0);
    return;
  }

  json_decref(state);
  pclose(script);

  ilog(LOG_TYPE_IRCD, "Migrating");
  exit(0);
#endif
}

static void
ms_migrate(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  const char *subcmd = parv[1];

  if (client_p != source_p)
    return;

  if (!strcmp(subcmd, "START"))
  {
    /* this ack has to go back to the other server before it starts the migration, so our recvq will empty
     * after its sent. we know the other server didn't send anything after this */
    sendto_one(source_p, ":%s MIGRATE ACK", ID(&me));

    /* they are migrating */
    AddSFlag(source_p, SERVER_FLAGS_MIGRATING);
    /* freeze read and write */
    AddSFlag(source_p, SERVER_FLAGS_MIGRATING_BLOCK_READ);
    AddSFlag(source_p, SERVER_FLAGS_MIGRATING_QUEUE_WRITE);
  }
  else if (!strcmp(subcmd, "ACK"))
  {
    if (!HasSFlag(&me, SERVER_FLAGS_MIGRATING))
    {
      sendto_snomask(SNO_ALL, L_ALL, "MIGRATE ACK from %s, but I am not migrating", source_p->name);
      return;
    }

    if (HasSFlag(source_p, SERVER_FLAGS_CORKED))
    {
      sendto_snomask(SNO_ALL, L_ALL, "MIGRATE ACK from already corked server %s", source_p->name);
      return;
    }

    AddSFlag(source_p, SERVER_FLAGS_CORKED);

    sendto_snomask(SNO_ALL, L_ALL, "Received MIGRATE ACK from %s", source_p->name);

    dlink_node *ptr;
    DLINK_FOREACH(ptr, serv_list.head)
    {
      struct Client *target = ptr->data;

      if (!HasSFlag(target, SERVER_FLAGS_CORKED))
        return;
    }

    sendto_snomask(SNO_ALL, L_ALL, "All servers have acknowledged migration");

    do_migrate();
  }
}

static struct Message migrate_msgtab =
{
  .cmd = "MIGRATE",
  .args_min = 2,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = mr_migrate,
  .handlers[CLIENT_HANDLER] = m_not_oper,
  .handlers[SERVER_HANDLER] = ms_migrate,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = mo_migrate,
};

static void
module_init(void)
{
  mod_add_cmd(&migrate_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&migrate_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
