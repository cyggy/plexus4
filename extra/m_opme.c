/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *  m_opme.c: Regains ops on opless channels
 *
 *  Copyright (C) 2002 by the past and present ircd coders, and others.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 *  $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "channel.h"
#include "client.h"
#include "ircd.h"
#include "numeric.h"
#include "send.h"
#include "irc_string.h"
#include "hash.h"
#include "s_serv.h"
#include "modules.h"
#include "list.h"
#include "channel_mode.h"
#include "parse.h"
#include "log.h"

/*
 * mo_opme()
 *      parv[0] = sender prefix
 *      parv[1] = channel
 */
static void
mo_opme(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Channel *chptr = NULL;
  struct Membership *member = NULL;

  if (!HasUMode(source_p, UMODE_NETADMIN))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVILEGES), me.name, source_p->name);
    return;
  }

  if((chptr = hash_find_channel(parv[1])) == NULL)
  {
    sendto_one(source_p, form_str(ERR_NOSUCHCHANNEL), me.name, source_p->name, parv[1]);
    return;
  }

  if((member = find_channel_link(source_p, chptr)) == NULL)
  {
    sendto_one(source_p, form_str(ERR_NOTONCHANNEL),
         me.name, source_p->name, chptr->chname);
    return;
  }

  if (has_member_flags(member, CHFL_CHANOP))
    return;

  sendto_snomask(SNO_ALL, L_ALL | SNO_ROUTE,
                 "OPME called for [%s] by %s!%s@%s",
                 chptr->chname, source_p->name, source_p->username,
                 source_p->host);

  AddMemberFlag(member, CHFL_CHANOP);

  sendto_server(NULL, CAP_TS6, NOCAPS, ":%s TMODE %lu %s +o %s", me.id, (unsigned long)chptr->channelts,
                chptr->chname, source_p->id);
  sendto_server(NULL, NOCAPS, CAP_TS6, ":%s MODE %s +o %s", me.name,
                chptr->chname, source_p->name);

  sendto_channel_local(ALL_MEMBERS, 0, chptr, ":%s MODE %s +o %s",
           me.name, chptr->chname, source_p->name);
}

struct Message opme_msgtab = {
  .cmd = "OPME",
  .args_min = 2,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_not_oper,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = mo_opme,
};

static void
module_init(void)
{
  mod_add_cmd(&opme_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&opme_msgtab);
}

struct module module_entry = {
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};

