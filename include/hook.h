/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

struct hook
{
  const char *name;
  dlink_list listeners;
};

struct Event
{
  struct hook *event;
  void (*handler)();
  dlink_node node;
};

extern void hook_add(struct Event *);
extern void hook_del(struct Event *);
extern void hook_execute(struct hook *, void *arg);

struct iline_attach_data
{
  struct Client *client;
  struct MaskItem *mask;
  int ret;
  int check_klines;
};

extern struct hook iline_attach_hook;

struct can_send_data
{
  struct Client *client;
  struct Channel *chptr;
  struct Membership *membership;
  const char *message;
  int notice;
  int ret;
};

extern struct hook can_send_hook;

struct local_user_register_data
{
  struct Client *client;
};

extern struct hook local_user_register_hook;

struct pre_server_burst_data
{
  struct Client *client;
};

extern struct hook pre_server_burst_hook;

