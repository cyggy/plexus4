/*
 * ircd-hybrid: an advanced Internet Relay Chat Daemon (ircd).
 * cloak.c: Provides hostname cloaking for clients.
 *
 *  Copyright (c) 2017 Adam <Adam@anope.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at you option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILILTY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have recieved a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#include "stdinc.h"
#include "list.h"
#include "cloak.h"
#include "conf.h"
#include "memory.h"
#include "log.h"
#include "rng_mt.h"
#include "irc_string.h"

static dlink_list cloak_systems;
static mp_pool_t *cloak_pool;

char *cloak_key = NULL;

void
cloak_register(struct CloakSystem *c)
{
  dlinkAdd(c, &c->node, &cloak_systems);
}

void
cloak_unregister(struct CloakSystem *c)
{
  dlink_node *ptr;

  dlinkDelete(&c->node, &cloak_systems);

  DLINK_FOREACH(ptr, cloak_items.head)
  {
    struct cloak_config *config = ptr->data;

    if (config->sys == c)
    {
      config->sys = NULL;
    }
  }
}

struct CloakSystem *
cloak_find_system(const char *name)
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, cloak_systems.head)
  {
    struct CloakSystem *cs = ptr->data;

    if (!strcasecmp(cs->name, name))
    {
      return cs;
    }
  }

  return NULL;
}

static const char *
generate_key()
{
  static const char base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  static char buf[101];

  for (int i = 0; i < sizeof(buf) - 1; ++i)
    buf[i] = base64[genrand_int32() & 0x3F];

  return buf;
}

void
cloak_reload(void)
{
  dlink_node *ptr;

  if (cloak_key == NULL)
  {
    if (ConfigFileEntry.cloak_key != NULL)
    {
      /* use key from conf */
      cloak_key = xstrdup(ConfigFileEntry.cloak_key);
    }
    else
    {
      /* generate a new key */
      ilog(LOG_TYPE_IRCD, "Generating random cloak key");

      cloak_key = xstrdup(generate_key());
    }
  }

  /* set system on config options */
  DLINK_FOREACH(ptr, cloak_items.head)
  {
    struct cloak_config *config = ptr->data;

    struct CloakSystem *sys = cloak_find_system(config->name);
    config->sys = sys;
  }

  if (cloak_pool == NULL)
  {
    cloak_pool = mp_pool_new("cloak", sizeof(struct UserCloak), sizeof(struct UserCloak) * 1024);
  }
}

void
cloak_client(struct UserCloaks *cloaks, const char *realhost, const char *sockhost)
{
  dlink_node *ptr;

  cloak_free(cloaks);

  if (cloak_pool == NULL)
  {
    return;
  }

  DLINK_FOREACH(ptr, cloak_items.head)
  {
    struct cloak_config *config = ptr->data;
    struct CloakSystem *cs = config->sys;

    if (!cs)
      continue;

    struct UserCloak *cloak = mp_pool_get(cloak_pool);
    memset(cloak, 0, sizeof(*cloak));

    if (cs->cloak(config, realhost, cloak->host, sizeof(cloak->host)) ||
        cs->cloak(config, sockhost, cloak->ip, sizeof(cloak->ip)))
    {
      mp_pool_release(cloak);
      continue;
    }

    /* default goes first */
    if (config->default_cloak)
      dlinkAdd(cloak, &cloak->node, &cloaks->cloaks);
    else
      dlinkAddTail(cloak, &cloak->node, &cloaks->cloaks);
  }
}

void
cloak_free(struct UserCloaks *cloaks)
{
  dlink_node *ptr, *ptr_next;

  DLINK_FOREACH_SAFE(ptr, ptr_next, cloaks->cloaks.head)
  {
    struct UserCloak *cloak = ptr->data;

    dlinkDelete(&cloak->node, &cloaks->cloaks);
    mp_pool_release(cloak);
  }
}

int
cloak_match(struct UserCloaks *cloaks, const char *mask)
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, cloaks->cloaks.head)
  {
    struct UserCloak *cloak = ptr->data;

    if (!match(mask, cloak->host) || !match(mask, cloak->ip))
      return 0;
  }

  return -1;
}

struct UserCloak *
cloak_get(struct UserCloaks *cloaks)
{
  if (dlink_list_length(&cloaks->cloaks) == 0)
    return NULL;

  /* default cloak is at the head */
  return cloaks->cloaks.head->data;
}
