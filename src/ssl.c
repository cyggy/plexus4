/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2015-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "fdlist.h"
#include "conf.h"
#include "log.h"
#include "memory.h"
#include "irc_string.h"
#include "ssl.h"
#include "listener.h"

static int
always_accept_verify_cb(int preverify_ok, X509_STORE_CTX *x509_ctx)
{
  return 1;
}

static int
ssl_servername_cb(SSL *s, int *ad, struct sslprofile *profile)
{
  const char *servername = SSL_get_servername(s, TLSEXT_NAMETYPE_host_name);
  struct sslprofile_identity *identity;

  if (servername == NULL || profile == NULL)
    return SSL_TLSEXT_ERR_NOACK;

  identity = sslprofile_find_identity(profile, servername);
  if (identity != NULL)
  {
    SSL_set_SSL_CTX(s, identity->server_ctx);
    return SSL_TLSEXT_ERR_OK;
  }

  return SSL_TLSEXT_ERR_NOACK;
}

void
ssl_init(void)
{
  SSL_load_error_strings();
  SSLeay_add_ssl_algorithms();
}

struct sslprofile *
sslprofile_create(void)
{
  struct sslprofile *profile = MyMalloc(sizeof(struct sslprofile));

  return profile;
}

static void
identity_free(struct sslprofile_identity *id)
{
  dlink_node *node, *next;

  DLINK_FOREACH_SAFE(node, next, id->server_names.head)
  {
    char *str = node->data;

    MyFree(str);
    dlinkDelete(node, &id->server_names);
    free_dlink_node(node);
  }

  if (id->rsa_private_key != NULL)
    RSA_free(id->rsa_private_key);

  MyFree(id->rsa_private_key_file);

  if (id->server_ctx)
  {
    SSL_CTX_set_verify(id->server_ctx, SSL_VERIFY_PEER|SSL_VERIFY_CLIENT_ONCE, NULL);

    SSL_CTX_set_tlsext_servername_callback(id->server_ctx, NULL);
    SSL_CTX_set_tlsext_servername_arg(id->server_ctx, NULL);

    SSL_CTX_free(id->server_ctx);
  }
  if (id->client_ctx)
  {
    SSL_CTX_set_verify(id->client_ctx, SSL_VERIFY_PEER|SSL_VERIFY_CLIENT_ONCE, NULL);

    SSL_CTX_free(id->client_ctx);
  }

  MyFree(id);
}

struct sslprofile_identity *
sslprofile_identity_create(struct sslprofile *profile)
{
  struct sslprofile_identity *identity = MyMalloc(sizeof(struct sslprofile_identity));

  if ((identity->server_ctx = SSL_CTX_new(SSLv23_server_method())) == NULL)
  {
    const char *s = ERR_lib_error_string(ERR_get_error());

    ilog(LOG_TYPE_IRCD, "ERROR: Could not initialize the SSL Server context -- %s", s);
    identity_free(identity);
    return NULL;
  }

  SSL_CTX_set_options(identity->server_ctx, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TICKET);
  SSL_CTX_set_options(identity->server_ctx, SSL_OP_SINGLE_DH_USE | SSL_OP_CIPHER_SERVER_PREFERENCE);
#ifdef SSL_OP_NO_RENEGOTIATION
  SSL_CTX_set_options(identity->server_ctx, SSL_OP_NO_RENEGOTIATION);
#endif
  SSL_CTX_set_verify(identity->server_ctx, SSL_VERIFY_PEER | SSL_VERIFY_CLIENT_ONCE, always_accept_verify_cb);
  SSL_CTX_set_session_cache_mode(identity->server_ctx, SSL_SESS_CACHE_OFF);

  SSL_CTX_set_tlsext_servername_callback(identity->server_ctx, ssl_servername_cb);
  SSL_CTX_set_tlsext_servername_arg(identity->server_ctx, profile);

#if OPENSSL_VERSION_NUMBER >= 0x009080FFL && !defined(OPENSSL_NO_ECDH)
  SSL_CTX_set_options(identity->server_ctx, SSL_OP_SINGLE_ECDH_USE);
#endif

  if ((identity->client_ctx = SSL_CTX_new(SSLv23_client_method())) == NULL)
  {
    const char *s = ERR_lib_error_string(ERR_get_error());

    ilog(LOG_TYPE_IRCD, "ERROR: Could not initialize the SSL Client context -- %s", s);
    sslprofile_free(profile);
    return NULL;
  }

  SSL_CTX_set_options(identity->client_ctx, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_TICKET);
  SSL_CTX_set_options(identity->client_ctx, SSL_OP_SINGLE_DH_USE);
  SSL_CTX_set_verify(identity->client_ctx, SSL_VERIFY_PEER | SSL_VERIFY_CLIENT_ONCE, always_accept_verify_cb);
  SSL_CTX_set_session_cache_mode(identity->client_ctx, SSL_SESS_CACHE_OFF);

  return identity;
}

void
sslprofile_free(struct sslprofile *profile)
{
  dlink_node *node, *next;

  listener_remove_sslprofile(profile);

  DLINK_FOREACH_SAFE(node, next, profile->identities.head)
  {
    struct sslprofile_identity *id = node->data;
    identity_free(id);
  }

  if (profile->dh)
  {
    DH_free(profile->dh);
    profile->dh = NULL;
  }

#if OPENSSL_VERSION_NUMBER >= 0x009080FFL && !defined(OPENSSL_NO_ECDH)
  if (profile->curve)
  {
    EC_KEY_free(profile->curve);
    profile->curve = NULL;
  }
#endif

  MyFree(profile);
}

struct sslprofile *
sslprofile_default(void)
{
  dlink_node *node;

  DLINK_FOREACH(node, sslprofile_items.head)
  {
    struct sslprofile *profile = node->data;
    return profile;
  }

  return NULL;
}

struct sslprofile *
sslprofile_find(const char *name)
{
  dlink_node *node;

  if (EmptyString(name))
    return sslprofile_default();

  DLINK_FOREACH(node, sslprofile_items.head)
  {
    struct sslprofile *profile = node->data;

    if (!strcmp(profile->name, name))
      return profile;
  }

  return sslprofile_default();
}

struct sslprofile_identity *
sslprofile_find_identity(struct sslprofile *profile, const char *name)
{
  dlink_node *node;

  if (EmptyString(name))
    return NULL;

  DLINK_FOREACH(node, profile->identities.head)
  {
    struct sslprofile_identity *id = node->data;
    dlink_node *node2;

    DLINK_FOREACH(node2, id->server_names.head)
    {
      const char *server_name = node2->data;

      if (!strcmp(server_name, name))
        return id;
    }
  }

  return NULL;
}

struct sslprofile_identity *
sslprofile_identity_get_default_from_profile(const char *profile_name)
{
  return sslprofile_identity_get_default(sslprofile_find(profile_name));
}

struct sslprofile_identity *
sslprofile_identity_get_default(struct sslprofile *profile)
{
  dlink_node *node;

  if (profile == NULL)
    return NULL;

  DLINK_FOREACH(node, profile->identities.head)
  {
    struct sslprofile_identity *id = node->data;
    return id;
  }

  return NULL;
}

