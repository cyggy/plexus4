/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file s_misc.c
 * \brief Yet another miscellaneous functions file.
 * \version $Id$
 */

#include "stdinc.h"
#include "s_misc.h"
#include "client.h"
#include "irc_string.h"
#include "ircd.h"
#include "numeric.h"
#include "irc_res.h"
#include "fdlist.h"
#include "s_bsd.h"
#include "conf.h"
#include "s_serv.h"
#include "send.h"
#include "memory.h"
#include "log.h"


static const char *const months[] =
{
  "January",   "February", "March",   "April",
  "May",       "June",     "July",    "August",
  "September", "October",  "November","December"
};

static const char *const weekdays[] =
{
  "Sunday",   "Monday", "Tuesday", "Wednesday",
  "Thursday", "Friday", "Saturday"
};

const char *
date(time_t lclock)
{
  static char buf[80], plus;
  struct tm *lt, *gm;
  struct tm gmbuf;
  int minswest;

  if (!lclock)
    lclock = CurrentTime;
  gm = gmtime(&lclock);
  memcpy(&gmbuf, gm, sizeof(gmbuf));
  gm = &gmbuf;
  lt = localtime(&lclock);

  /*
   * There is unfortunately no clean portable way to extract time zone
   * offset information, so do ugly things.
   */
  minswest = (gm->tm_hour - lt->tm_hour) * 60 + (gm->tm_min - lt->tm_min);

  if (lt->tm_yday != gm->tm_yday)
  {
    if ((lt->tm_yday > gm->tm_yday && lt->tm_year == gm->tm_year) ||
        (lt->tm_yday < gm->tm_yday && lt->tm_year != gm->tm_year))
      minswest -= 24 * 60;
    else
      minswest += 24 * 60;
  }

  plus = (minswest > 0) ? '-' : '+';
  if (minswest < 0)
    minswest = -minswest;

  snprintf(buf, sizeof(buf), "%s %s %d %d -- %02u:%02u:%02u %c%02u:%02u",
           weekdays[lt->tm_wday], months[lt->tm_mon],lt->tm_mday,
           lt->tm_year + 1900, lt->tm_hour, lt->tm_min, lt->tm_sec,
           plus, minswest/60, minswest%60);
  return buf;
}

const char *
smalldate(time_t lclock, int uselocal)
{
  static char buf[MAX_DATE_STRING];
  struct tm *tm;
  struct tm tmbuf;

  if (!lclock)
    lclock = CurrentTime;

  tm = uselocal ? localtime(&lclock) : gmtime(&lclock);
  memcpy(&tmbuf, tm, sizeof(tmbuf));
  tm = &tmbuf;

  snprintf(buf, sizeof(buf), "%d/%d/%d %02d.%02d",
           tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
           tm->tm_hour, tm->tm_min);

  return buf;
}

/*
 * myctime - This is like standard ctime()-function, but it zaps away
 *   the newline from the end of that string. Also, it takes
 *   the time value as parameter, instead of pointer to it.
 *   Note that it is necessary to copy the string to alternate
 *   buffer (who knows how ctime() implements it, maybe it statically
 *   has newline there and never 'refreshes' it -- zapping that
 *   might break things in other places...)
 *
 *
 * Thu Nov 24 18:22:48 1986
 */
const char *
myctime(time_t value)
{
  static char buf[32];
  char *p;

  strlcpy(buf, ctime(&value), sizeof(buf));

  if ((p = strchr(buf, '\n')) != NULL)
    *p = '\0';
  return buf;
}

const char *
ssl_get_cipher(const SSL *ssl)
{
  static char buffer[IRCD_BUFSIZE / 4];
  int bits = 0;

  SSL_CIPHER_get_bits(SSL_get_current_cipher(ssl), &bits);

  snprintf(buffer, sizeof(buffer), "%s %s-%d", SSL_get_version(ssl),
           SSL_get_cipher(ssl), bits);
  return buffer;
}

void
ssl_get_cert(struct Client *client_p)
{
  X509 *cert;

  if ((cert = SSL_get_peer_certificate(client_p->localClient->fd.ssl)))
  {
    int res = SSL_get_verify_result(client_p->localClient->fd.ssl);

    if (res == X509_V_OK || res == X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN ||
        res == X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE ||
        res == X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT)
    {
      unsigned char md[EVP_MAX_MD_SIZE];
      char buf[EVP_MAX_MD_SIZE * 2 + 1];
      unsigned int n = sizeof(md);

      if (X509_digest(cert, ConfigFileEntry.message_digest_algorithm, md, &n))
      {
        base16_encode(buf, sizeof(buf), (char *) md, n);
        MyFree(client_p->certfp);
        client_p->certfp = xstrdup(buf);
      }

      if (client_p->localClient->cert)
        X509_free(client_p->localClient->cert);
      client_p->localClient->cert = cert;
    }
    else
    {
      ilog(LOG_TYPE_DEBUG, "Client %s gave bad SSL client certificate: %d",
           get_client_name(client_p, HIDE_IP), res);
      X509_free(cert);
    }
  }
}

/*  The following functions are: 
 *  Copyright (c) 2001-2004, Roger Dingledine
 *  Copyright (c) 2004-2007, Roger Dingledine, Nick Mathewson
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *
 *  Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the distribution.
 *
 * Neither the names of the copyright owners nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define SIZE_T_CEILING (sizeof(char)<<(sizeof(size_t)*8 - 1))

/** Encode the <b>srclen</b> bytes at <b>src</b> in a NUL-terminated,
 * uppercase hexadecimal string; store it in the <b>destlen</b>-byte buffer
 * <b>dest</b>.
 */
void
base16_encode(char *dest, size_t destlen, const char *src, size_t srclen)
{
  const char *end;
  char *cp;

  assert(destlen >= srclen*2+1);

  cp = dest;
  end = src+srclen;
  while (src<end)
  {
    *cp++ = "0123456789ABCDEF"[ (*(const uint8_t*)src) >> 4 ];
    *cp++ = "0123456789ABCDEF"[ (*(const uint8_t*)src) & 0xf ];
    ++src;
  }
  *cp = '\0';
}

/** Helper: given a hex digit, return its value, or -1 if it isn't hex. */
static int
hex_decode_digit(char c)
{
  switch (c) {
    case '0': return 0;
    case '1': return 1;
    case '2': return 2;
    case '3': return 3;
    case '4': return 4;
    case '5': return 5;
    case '6': return 6;
    case '7': return 7;
    case '8': return 8;
    case '9': return 9;
    case 'A': case 'a': return 10;
    case 'B': case 'b': return 11;
    case 'C': case 'c': return 12;
    case 'D': case 'd': return 13;
    case 'E': case 'e': return 14;
    case 'F': case 'f': return 15;
    default:
      return -1;
  }
}

/** Given a hexadecimal string of <b>srclen</b> bytes in <b>src</b>, decode it
 * and store the result in the <b>destlen</b>-byte buffer at <b>dest</b>.
 * Return 0 on success, -1 on failure. */
int
base16_decode(char *dest, size_t destlen, const char *src, size_t srclen)
{
  const char *end;

  int v1,v2;
  if ((srclen % 2) != 0)
    return -1;
  if (destlen < srclen/2 || destlen > SIZE_T_CEILING)
    return -1;
  end = src+srclen;
  while (src<end) {
    v1 = hex_decode_digit(*src);
    v2 = hex_decode_digit(*(src+1));
    if (v1<0||v2<0)
      return -1;
    *(uint8_t*)dest = (v1<<4)|v2;
    ++dest;
    src+=2;
  }
  return 0;
}

const char *
str_replace(const char *in, char *out, size_t sz, const char *find, const char *replace)
{
  char tmp[IRCD_BUFSIZE];
  const char *start = tmp;
  const char *p;

  strlcpy(tmp, in, sizeof(tmp));

  assert(sz);

  while (sz && (p = strstr(start, find)) != NULL)
  {
    const char *r;

    /* copy from start to p into 'out', if there is room */
    while (sz && start < p)
    {
      *out++ = *start++;
      --sz;
    }

    /* append replacement */
    r = replace;
    while (sz && *r)
    {
      *out++ = *r++;
      --sz;
    }

    /* start moves past find */
    r = find;
    while (*r++ && *start)
      ++start;
  }

  /* copy from start to end */
  while (sz && *start)
  {
    *out++ = *start++;
    --sz;
  }

  if (sz)
    *out = '\0';
  else
    *(out - 1) = '\0';

  return out;
}


static const char *const base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const char pad64 = '=';

/* (From RFC1521 and draft-ietf-dnssec-secext-03.txt)
   The following encoding technique is taken from RFC 1521 by Borenstein
   and Freed.  It is reproduced here in a slightly edited form for
   convenience.

   A 65-character subset of US-ASCII is used, enabling 6 bits to be
   represented per printable character. (The extra 65th character, "=",
   is used to signify a special processing function.)

   The encoding process represents 24-bit groups of input bits as output
   strings of 4 encoded characters. Proceeding from left to right, a
   24-bit input group is formed by concatenating 3 8-bit input groups.
   These 24 bits are then treated as 4 concatenated 6-bit groups, each
   of which is translated into a single digit in the base64 alphabet.

   Each 6-bit group is used as an index into an array of 64 printable
   characters. The character referenced by the index is placed in the
   output string.

             Table 1: The Base64 Alphabet

    Value Encoding  Value Encoding  Value Encoding  Value Encoding
      0 A      17 R      34 i      51 z
      1 B      18 S      35 j      52 0
      2 C      19 T      36 k      53 1
      3 D      20 U      37 l      54 2
      4 E      21 V      38 m      55 3
      5 F      22 W      39 n      56 4
      6 G      23 X      40 o      57 5
      7 H      24 Y      41 p      58 6
      8 I      25 Z      42 q      59 7
      9 J      26 a      43 r      60 8
     10 K      27 b      44 s      61 9
     11 L      28 c      45 t      62 +
     12 M      29 d      46 u      63 /
     13 N      30 e      47 v
     14 O      31 f      48 w     (pad) =
     15 P      32 g      49 x
     16 Q      33 h      50 y

   Special processing is performed if fewer than 24 bits are available
   at the end of the data being encoded.  A full encoding quantum is
   always completed at the end of a quantity.  When fewer than 24 input
   bits are available in an input group, zero bits are added (on the
   right) to form an integral number of 6-bit groups.  Padding at the
   end of the data is performed using the '=' character.

   Since all base64 input is an integral number of octets, only the
     -------------------------------------------------
   following cases can arise:

     (1) the final quantum of encoding input is an integral
       multiple of 24 bits; here, the final unit of encoded
     output will be an integral multiple of 4 characters
     with no "=" padding,
     (2) the final quantum of encoding input is exactly 8 bits;
       here, the final unit of encoded output will be two
     characters followed by two "=" padding characters, or
     (3) the final quantum of encoding input is exactly 16 bits;
       here, the final unit of encoded output will be three
     characters followed by one "=" padding character.
   */

int base64_encode(const char *src, char *target, size_t sz)
{
  size_t src_pos = 0, src_len = strlen(src);
  unsigned char input[3];

  while (src_len - src_pos > 2)
  {
    input[0] = src[src_pos++];
    input[1] = src[src_pos++];
    input[2] = src[src_pos++];

    if (sz < 4)
      return -1;

    *target++ = base64[input[0] >> 2];
    *target++ = base64[((input[0] & 0x03) << 4) + (input[1] >> 4)];
    *target++ = base64[((input[1] & 0x0f) << 2) + (input[2] >> 6)];
    *target++ = base64[input[2] & 0x3f];
    sz -= 4;
  }

  /* Now we worry about padding */
  if (src_pos != src_len)
  {
    input[0] = input[1] = input[2] = 0;
    for (size_t i = 0; i < src_len - src_pos; ++i)
      input[i] = src[src_pos + i];

    if (sz < 4)
      return -1;

    *target++ = base64[input[0] >> 2];
    *target++ = base64[((input[0] & 0x03) << 4) + (input[1] >> 4)];
    if (src_pos == src_len - 1)
      *target++ = pad64;
    else
      *target++ = base64[((input[1] & 0x0f) << 2) + (input[2] >> 6)];
    *target++ = pad64;
    sz -= 4;
  }

  *target = 0;
  return 0;
}

/* skips all whitespace anywhere.
   converts characters, four at a time, starting at (or after)
   src from base - 64 numbers into three 8 bit bytes in the target area.
 */

int base64_decode(const char *src, char *target, size_t sz)
{
  unsigned state = 0;
  for (; *src; ++src)
  {
    if (isspace(*src)) /* Skip whitespace anywhere */
      continue;

    if (*src == pad64)
      break;

    const char *pos = strchr(base64, *src);
    if (!pos) /* A non-base64 character */
      return -1;

    switch (state)
    {
      case 0:
        if (!sz)
          return -1;
        *target++ = (pos - base64) << 2;
        --sz;
        state = 1;
        break;
      case 1:
        if (!sz)
          return -1;
        *(target - 1) |= (pos - base64) >> 4;
        *target++ = ((pos - base64) & 0x0f) << 4;
        --sz;
        state = 2;
        break;
      case 2:
        if (!sz)
          return -1;
        *(target - 1) |= (pos - base64) >> 2;
        *target++ = ((pos - base64) & 0x03) << 6;
        --sz;
        state = 3;
        break;
      case 3:
        *(target - 1) |= (pos - base64);
        state = 0;
    }
  }

  if (sz)
  {
    *target = 0;
    return 0;
  }

  return -1;
}

