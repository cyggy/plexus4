/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file irc_res.c
 * \brief ircd resolver functions
 * \version $Id$
 */

/*
 * A rewrite of Darren Reeds original res.c As there is nothing
 * left of Darrens original code, this is now licensed by the hybrid group.
 * (Well, some of the function names are the same, and bits of the structs..)
 * You can use it where it is useful, free even. Buy us a beer and stuff.
 *
 * The authors takes no responsibility for any damage or loss
 * of property which results from the use of this software.
 *
 * $Id$
 *
 * July 1999 - Rewrote a bunch of stuff here. Change hostent builder code,
 *     added callbacks and reference counting of returned hostents.
 *     --Bleep (Thomas Helvey <tomh@inxpress.net>)
 *
 * This was all needlessly complicated for irc. Simplified. No more hostent
 * All we really care about is the IP -> hostname mappings. Thats all.
 *
 * Apr 28, 2003 --cryogen and Dianora
 */

#include "stdinc.h"
#include "list.h"
#include "client.h"
#include "event.h"
#include "irc_string.h"
#include "ircd.h"
#include "numeric.h"
#include "rng_mt.h"
#include "fdlist.h"
#include "s_bsd.h"
#include "log.h"
#include "s_misc.h"
#include "send.h"
#include "memory.h"
#include "mempool.h"
#include "irc_res.h"
#include "irc_reslib.h"

#if (CHAR_BIT != 8)
#error this code needs to be able to address individual octets
#endif

static PF res_readreply;

#define MAXPACKET      1024  /* rfc sez 512 but we expand names so ... */
#define AR_TTL         600   /* TTL in seconds for dns cache entries */

/* RFC 1104/1105 wasn't very helpful about what these fields
 * should be named, so for now, we'll just name them this way.
 * we probably should look at what named calls them or something.
 */
#define TYPE_SIZE         (size_t)2
#define CLASS_SIZE        (size_t)2
#define TTL_SIZE          (size_t)4
#define RDLENGTH_SIZE     (size_t)2
#define ANSWER_FIXED_SIZE (TYPE_SIZE + CLASS_SIZE + TTL_SIZE + RDLENGTH_SIZE)

struct reslist
{
  struct irc_ns *ns;      /* nameserver queried */
  dlink_node tnode;       /* node into request_list_timeout */
  int id;
  int sent;                /* number of requests sent */
  time_t ttl;
  char type;
  char retries;            /* retry counter */
  char sends;              /* number of sends (>1 means resent) */
  char resend;             /* send flag. 0 == dont resend */
  time_t sentat;
  time_t timeout;
  struct irc_ssaddr addr;
  char *name;
  char queryname[RFC1035_MAX_DOMAIN_LENGTH + 1];
  dns_callback_fnc callback;
  void *callback_ctx;
  dns_handle *handle;
};

static fde_t resolver_fd_4, resolver_fd_6;

/* request list. ordered with oldest queries first, newest last */
static dlink_list request_list_timeout;

static mp_pool_t *dns_pool = NULL;

static void resend_query(struct reslist *);

static int
retryfreq(struct irc_ns *ns)
{
  switch (ns->failure_count)
  {
    case 1:
      return 3;
    case 2:
      return 9;
    case 3:
      return 27;
    case 4:
      return 81;
    default:
      return 243;
  }
}

static struct irc_ns *
find_available_ns()
{
  dlink_node *node;
  struct irc_ns *least = NULL;
  static int retrycnt;

  ++retrycnt;

  DLINK_FOREACH(node, irc_ns_list.head)
  {
    struct irc_ns *srv = node->data;

    if (srv->num_queries == MAX_ID)
      continue;

    if (srv->failure_count && retrycnt % retryfreq(srv))
      continue;

    return srv;
  }

  /* try a bad ns */
  DLINK_FOREACH(node, irc_ns_list.head)
  {
    struct irc_ns *srv = node->data;

    if (srv->num_queries == MAX_ID)
      continue;

    if (!srv->failure_count)
      continue;

    if (least == NULL || srv->failure_count < least->failure_count)
      least = srv;
  }

  return least;
}

/*
 * struct irc_ns *
 * res_ourserver(inp)
 *      looks up "inp" in irc_ns_list
 * returns: server if found
 * author:
 *      paul vixie, 29may94
 *      revised for ircd, cryogen(stu) may03
 */
static struct irc_ns *
res_ourserver(const struct irc_ssaddr *inp)
{
  dlink_node *node;

  DLINK_FOREACH(node, irc_ns_list.head)
  {
    struct irc_ns *srv = node->data;

    /* could probably just memcmp(srv, inp, srv.ss_len) here
     * but we'll air on the side of caution - stu
     *
     */
    switch (srv->addr.ss.ss_family)
    {
#ifdef IPV6
      case AF_INET6:
      {
        const struct sockaddr_in6 *v6in = (const struct sockaddr_in6 *) inp;
        const struct sockaddr_in6 *v6 = (const struct sockaddr_in6 *) &srv->addr;

        if (srv->addr.ss.ss_family == inp->ss.ss_family)
          if (v6->sin6_port == v6in->sin6_port)
            if (!memcmp(&v6->sin6_addr.s6_addr, &v6in->sin6_addr.s6_addr,
                        sizeof(struct in6_addr)))
              return srv;
        break;
      }
#endif
      case AF_INET:
      {
        const struct sockaddr_in *v4in = (const struct sockaddr_in *) inp;
        const struct sockaddr_in *v4 = (const struct sockaddr_in *) &srv->addr;

        if (srv->addr.ss.ss_family == inp->ss.ss_family)
          if (v4->sin_port == v4in->sin_port)
            if (v4->sin_addr.s_addr == v4in->sin_addr.s_addr)
              return srv;
        break;
      }
      default:
        break;
    }
  }

  return NULL;
}

/*
 * timeout_query_list - Remove queries from the list which have been
 * there too long without being resolved.
 */
void
timeout_query_list(time_t now)
{
  dlink_node *ptr;
  dlink_node *next_ptr;
  struct reslist *request;
  time_t timeout   = 0;

  DLINK_FOREACH_SAFE(ptr, next_ptr, request_list_timeout.head)
  {
    request = ptr->data;
    timeout = request->sentat + request->timeout;

    if (now >= timeout)
    {
      ++request->ns->failure_count;
      request->timeout += request->timeout;
      resend_query(request); /* this may delete the request if there are no more retries */
    }
    else
      break;
  }
}

/*
 * timeout_resolver - check request list
 */
static void
timeout_resolver(void *notused)
{
  timeout_query_list(CurrentTime);
}

/*
 * start_resolver - do everything we need to read the resolv.conf file
 * and initialize the resolver file descriptor if needed
 */
static void
start_resolver(void)
{
  irc_res_init();

  comm_open(&resolver_fd_4, AF_INET, SOCK_DGRAM, 0, "Resolver socket (4)");
  comm_open(&resolver_fd_6, AF_INET6, SOCK_DGRAM, 0, "Resolver socket (6)");

  if (resolver_fd_4.flags.open)
    comm_setselect(&resolver_fd_4, COMM_SELECT_READ, res_readreply, NULL);

  if (resolver_fd_6.flags.open)
    comm_setselect(&resolver_fd_6, COMM_SELECT_READ, res_readreply, NULL);

  eventAdd("timeout_resolver", timeout_resolver, NULL, 1);
}

/*
 * init_resolver - initialize resolver and resolver library
 */
void
init_resolver(void)
{
  dns_pool = mp_pool_new("reslist", sizeof(struct reslist), MP_CHUNK_SIZE_DNS);
  start_resolver();
}

void
stop_resolver(void)
{
  irc_res_stop();

  if (resolver_fd_4.flags.open)
    fd_close(&resolver_fd_4);
  if (resolver_fd_6.flags.open)
    fd_close(&resolver_fd_6);

  eventDelete(timeout_resolver, NULL); /* -ddosen */
}

/*
 * find_id - find a dns request id (id is determined by dn_mkquery)
 */
static struct reslist *
find_id(struct irc_ns *server, int id)
{
  if (id < 0 || id >= MAX_ID)
    return NULL;

  return server->request_list[id];
}

static void
unlink_request(struct reslist *request)
{
  if (request->id < 0 || request->id >= MAX_ID)
    return;

  assert(request->handle->ns == request->ns);
  assert(request->handle->id == request->id);

  assert(dlinkFind(&request_list_timeout, request));
  dlinkDelete(&request->tnode, &request_list_timeout);

  assert(request->ns->request_list[request->id] == request);
  request->ns->request_list[request->id] = NULL;
  --request->ns->num_queries;

  request->handle->id = 0;
  request->handle->ns = NULL;

  request->id = -1;
}

/*
 * rem_request - remove a request from the list.
 * This must also free any memory that has been allocated for
 * temporary storage of DNS results.
 */
void
rem_request(struct reslist *request)
{
  unlink_request(request);

  MyFree(request->name);
  mp_pool_release(request);
}

/*
 * make_request - Create a DNS request record for the server.
 */
static struct reslist *
make_request(dns_handle *handle, dns_callback_fnc callback, void *ctx)
{
  struct irc_ns *ns;
  struct reslist *request;

  ns = find_available_ns();
  if (ns == NULL)
  {
    ilog(LOG_TYPE_IRCD, "Unable to create dns request, no available nameservers!");
    callback(ctx, NULL, NULL);
    return NULL;
  }

  request = mp_pool_get(dns_pool);

  memset(request, 0, sizeof(*request));
  request->ns           = ns;
  request->sentat       = CurrentTime;
  request->retries      = 0;
  request->resend       = 0;    /* Disable retries */
  request->timeout      = 4;    /* start at 4 and exponential inc. */
  request->callback     = callback;
  request->callback_ctx = ctx;
  request->handle = handle;

  /*
   * generate an unique id
   * NOTE: we don't have to worry about converting this to and from
   * network byte order, the nameserver does not interpret this value
   * and returns it unchanged
   */
  do
    request->id = (request->id + genrand_int32()) & 0xffff;
  while (find_id(ns, request->id));

  request->handle->ns = ns;
  request->handle->id = request->id;

  assert(ns->request_list[request->id] == NULL);
  ns->request_list[request->id] = request;
  ++ns->num_queries;

  dlinkAddTail(request, &request->tnode, &request_list_timeout);

  return request;
}

/*
 * delete_resolver_queries - cleanup outstanding queries
 * for which there no longer exist clients or conf lines.
 */
void
delete_resolver_query(dns_handle *handle)
{
  struct reslist *res;

  if (handle->ns == NULL || handle->id < 0 || handle->id >= MAX_ID)
    return;

  res = find_id(handle->ns, handle->id);
  if (res == NULL)
    return;

  assert(handle == res->handle);
  rem_request(res);
}

static int
send_res_msg(struct reslist *req, const char *msg, int len)
{
  fde_t *F = NULL;

  switch (req->ns->addr.ss.ss_family)
  {
    case AF_INET:
      F = &resolver_fd_4;
      break;
    case AF_INET6:
      F = &resolver_fd_6;
      break;
  }

  if (F && sendto(F->fd, msg, len, 0, (struct sockaddr *) &req->ns->addr, req->ns->addr.ss_len) == len)
    return 1;

  return 0;
}

/*
 * query_name - generate a query based on class, type and name.
 */
static void
query_name(const char *name, int query_class, int type,
           struct reslist *request)
{
  char buf[MAXPACKET];
  int request_len = 0;

  memset(buf, 0, sizeof(buf));

  if ((request_len = irc_res_mkquery(name, query_class, type,
      (unsigned char *)buf, sizeof(buf))) > 0)
  {
    HEADER *header = (HEADER *)buf;
    header->id = request->id;
    ++request->sends;

    request->sent += send_res_msg(request, buf, request_len);
  }
}

/*
 * do_query_name - nameserver lookup name
 */
static void
do_query_name(dns_handle *handle, dns_callback_fnc callback, void *ctx, const char *name,
              struct reslist *request, int type)
{
  char host_name[RFC1035_MAX_DOMAIN_LENGTH + 1];

  strlcpy(host_name, name, sizeof(host_name));

  if (request == NULL)
  {
    request = make_request(handle, callback, ctx);
    if (request == NULL)
      return;

    request->name = MyMalloc(strlen(host_name) + 1);
    request->type = type;
    strcpy(request->name, host_name);
    strlcpy(request->queryname, host_name, sizeof(request->queryname));
  }

  request->type = type;
  query_name(host_name, C_IN, type, request);
}

/*
 * gethost_byname_type - get host address from name
 *
 */
void
gethost_byname_type(dns_handle *handle, dns_callback_fnc callback, void *ctx, const char *name, int type)
{
  assert(name != NULL);
  do_query_name(handle, callback, ctx, name, NULL, type);
}

/*
 * do_query_number - Use this to do reverse IP# lookups.
 */
static void
do_query_number(dns_handle *handle, dns_callback_fnc callback, void *ctx,
                const struct irc_ssaddr *addr,
                struct reslist *request)
{
  const unsigned char *cp;

  if (request == NULL)
  {
    request = make_request(handle, callback, ctx);
    if (request == NULL)
      return;

    request->type = T_PTR;
    memcpy(&request->addr, addr, sizeof(struct irc_ssaddr));
    request->name = MyMalloc(RFC1035_MAX_DOMAIN_LENGTH + 1);
  }

  if (addr->ss.ss_family == AF_INET)
  {
    const struct sockaddr_in *v4 = (const struct sockaddr_in *)addr;
    cp = (const unsigned char *)&v4->sin_addr.s_addr;

    snprintf(request->queryname, sizeof(request->queryname), "%u.%u.%u.%u.in-addr.arpa",
             (unsigned int)(cp[3]), (unsigned int)(cp[2]),
             (unsigned int)(cp[1]), (unsigned int)(cp[0]));
  }
#ifdef IPV6
  else if (addr->ss.ss_family == AF_INET6)
  {
    const struct sockaddr_in6 *v6 = (const struct sockaddr_in6 *)addr;
    cp = (const unsigned char *)&v6->sin6_addr.s6_addr;

    snprintf(request->queryname, sizeof(request->queryname),
             "%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x."
             "%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.%x.ip6.arpa",
             (unsigned int)(cp[15] & 0xf), (unsigned int)(cp[15] >> 4),
             (unsigned int)(cp[14] & 0xf), (unsigned int)(cp[14] >> 4),
             (unsigned int)(cp[13] & 0xf), (unsigned int)(cp[13] >> 4),
             (unsigned int)(cp[12] & 0xf), (unsigned int)(cp[12] >> 4),
             (unsigned int)(cp[11] & 0xf), (unsigned int)(cp[11] >> 4),
             (unsigned int)(cp[10] & 0xf), (unsigned int)(cp[10] >> 4),
             (unsigned int)(cp[9] & 0xf), (unsigned int)(cp[9] >> 4),
             (unsigned int)(cp[8] & 0xf), (unsigned int)(cp[8] >> 4),
             (unsigned int)(cp[7] & 0xf), (unsigned int)(cp[7] >> 4),
             (unsigned int)(cp[6] & 0xf), (unsigned int)(cp[6] >> 4),
             (unsigned int)(cp[5] & 0xf), (unsigned int)(cp[5] >> 4),
             (unsigned int)(cp[4] & 0xf), (unsigned int)(cp[4] >> 4),
             (unsigned int)(cp[3] & 0xf), (unsigned int)(cp[3] >> 4),
             (unsigned int)(cp[2] & 0xf), (unsigned int)(cp[2] >> 4),
             (unsigned int)(cp[1] & 0xf), (unsigned int)(cp[1] >> 4),
             (unsigned int)(cp[0] & 0xf), (unsigned int)(cp[0] >> 4));
  }
#endif

  query_name(request->queryname, C_IN, T_PTR, request);
}

/*
 * gethost_byaddr - get host name from address
 */
void
gethost_byaddr(dns_handle *handle, dns_callback_fnc callback, void *ctx, const struct irc_ssaddr *addr)
{
  do_query_number(handle, callback, ctx, addr, NULL);
}

static void
resend_query(struct reslist *request)
{
  if (request->resend == 0 || --request->retries <= 0)
  {
    unlink_request(request);
    (*request->callback)(request->callback_ctx, NULL, NULL);
    rem_request(request);
    return;
  }

  /* move query to the end of the list */
  assert(dlinkFind(&request_list_timeout, request));
  dlinkDelete(&request->tnode, &request_list_timeout);
  dlinkAddTail(request, &request->tnode, &request_list_timeout);

  request->sentat = CurrentTime;

  switch (request->type)
  {
    case T_PTR:
      do_query_number(NULL, NULL, NULL, &request->addr, request);
      break;
    case T_A:
    case T_AAAA:
      do_query_name(NULL, NULL, NULL, request->name, request, request->type);
      break;
    default:
      break;
  }
}

static int
check_question(struct reslist *request, HEADER *header, unsigned char *buf, unsigned char *eob)
{
  unsigned char *current;
  char hostbuf[RFC1035_MAX_DOMAIN_LENGTH + 1];
  int n;

  if (header->qdcount != 1)
    return 0;

  current = buf + sizeof(HEADER);
  n = irc_dn_expand(buf, eob, current, hostbuf, sizeof(hostbuf));

  if (n <= 0)
    return 0;

  if (strcasecmp(hostbuf, request->queryname))
    return 0;

  return 1;
}

/*
 * proc_answer - process name server reply
 */
static int
proc_answer(struct reslist *request, HEADER *header, unsigned char *buf, unsigned char *eob)
{
  char hostbuf[RFC1035_MAX_DOMAIN_LENGTH + 100]; /* working buffer */
  unsigned char *current;      /* current position in buf */
  int query_class;             /* answer class */
  unsigned int type;           /* answer type */
  int n;                       /* temp count */
  unsigned int rd_length;
  struct sockaddr_in *v4;      /* conversion */
#ifdef IPV6
  struct sockaddr_in6 *v6;
#endif
  current = (unsigned char *)buf + sizeof(HEADER);

  for (; header->qdcount > 0; --header->qdcount)
  {
    if ((n = irc_dn_skipname(current, (unsigned char *)eob)) < 0)
      break;

    current += (size_t)n + QFIXEDSZ;
  }

  /*
   * process each answer sent to us blech.
   */
  while (header->ancount > 0 && current < eob)
  {
    header->ancount--;

    n = irc_dn_expand(buf, eob, current,
        hostbuf, sizeof(hostbuf));

    if (n < 0 /* broken message */ || n == 0 /* no more answers left */)
      return 0;

    hostbuf[RFC1035_MAX_DOMAIN_LENGTH] = '\0';

    /* With Address arithmetic you have to be very anal
     * this code was not working on alpha due to that
     * (spotted by rodder/jailbird/dianora)
     */
    current += (size_t) n;

    if (!((current + ANSWER_FIXED_SIZE) < eob))
      break;

    type = irc_ns_get16(current);
    current += TYPE_SIZE;

    query_class = irc_ns_get16(current);
    (void) query_class;
    current += CLASS_SIZE;

    request->ttl = irc_ns_get32(current);
    current += TTL_SIZE;

    rd_length = irc_ns_get16(current);
    current += RDLENGTH_SIZE;

    /*
     * Wait to set request->type until we verify this structure
     */
    switch (type)
    {
      case T_A:
        if (request->type != T_A)
          return 0;

        /*
         * check for invalid rd_length or too many addresses
         */
        if (rd_length != sizeof(struct in_addr))
          return 0;

        v4 = (struct sockaddr_in *)&request->addr;
        request->addr.ss_len = sizeof(struct sockaddr_in);
        v4->sin_family = AF_INET;
        memcpy(&v4->sin_addr, current, sizeof(struct in_addr));
        return 1;
        break;
#ifdef IPV6
      case T_AAAA:
        if (request->type != T_AAAA)
          return 0;

        if (rd_length != sizeof(struct in6_addr))
          return 0;

        request->addr.ss_len = sizeof(struct sockaddr_in6);
        v6 = (struct sockaddr_in6 *)&request->addr;
        v6->sin6_family = AF_INET6;
        memcpy(&v6->sin6_addr, current, sizeof(struct in6_addr));
        return 1;
        break;
#endif
      case T_PTR:
        if (request->type != T_PTR)
          return 0;

        n = irc_dn_expand(buf, eob,
            current, hostbuf, sizeof(hostbuf));
        if (n < 0 /* broken message */ || n == 0 /* no more answers left */)
          return 0;

        strlcpy(request->name, hostbuf, RFC1035_MAX_DOMAIN_LENGTH + 1);
        return 1;
        break;
      case T_CNAME:
        current += rd_length;
        break;

      default:
        /* XXX I'd rather just throw away the entire bogus thing
         * but its possible its just a broken nameserver with still
         * valid answers. But lets do some rudimentary logging for now...
         */
        ilog(LOG_TYPE_IRCD, "irc_res.c bogus type %d", type);
        break;
    }
  }

  return 1;
}

/*
 * res_readreply - read a dns reply from the nameserver and process it.
 */
static void
res_readreply(fde_t *fd, void *data)
{
  unsigned char buf[sizeof(HEADER) + MAXPACKET]
	/* Sparc and alpha need 16bit-alignment for accessing header->id
	 * (which is uint16_t). Because of the header = (HEADER*) buf;
	 * lateron, this is neeeded. --FaUl
	 */
#if defined(__sparc__) || defined(__alpha__)
	  __attribute__((aligned (16)))
#endif
	  ;
  HEADER *header;
  struct reslist *request = NULL;
  int rc;
  socklen_t len = sizeof(struct irc_ssaddr);
  struct irc_ns *server;
  struct irc_ssaddr lsin;

  rc = recvfrom(fd->fd, buf, sizeof(buf), 0, (struct sockaddr *)&lsin, &len);

  /* Re-schedule a read *after* recvfrom, or we'll be registering
   * interest where it'll instantly be ready for read :-) -- adrian
   */
  comm_setselect(fd, COMM_SELECT_READ, res_readreply, NULL);

  /* Better to cast the sizeof instead of rc */
  if (rc <= (int)(sizeof(HEADER)))
    return;

  /*
   * check against possibly fake replies
   */
  server = res_ourserver(&lsin);
  if (!server)
    return;

  /*
   * convert DNS reply reader from Network byte order to CPU byte order.
   */
  header = (HEADER *)buf;
  header->ancount = ntohs(header->ancount);
  header->qdcount = ntohs(header->qdcount);
  header->nscount = ntohs(header->nscount);
  header->arcount = ntohs(header->arcount);

  /*
   * response for an id which we have already received an answer for
   * just ignore this response.
   */
  if (!(request = find_id(server, header->id)))
    return;

  if (!check_question(request, header, buf, buf + rc))
    return;

  if (request->ns != server)
  {
    /* late reply */
    server->failure_count += 3;
  }

  if (header->rcode != NO_ERRORS || header->ancount == 0)
  {
    switch (header->rcode)
    {
      case FMTERROR:
      case SERVFAIL:
      case NOTIMP:
      case REFUSED:
        ++server->failure_count;
        resend_query(request);
        break;
      case NXDOMAIN:
        /* this is a good answer, domain doesn't exist */
        server->failure_count /= 4;
        /* FALLTHROUGH */
      default:
        /*
         * If a bad error was returned, stop here and don't
         * send any more (no retries granted).
         */
        unlink_request(request);
        (*request->callback)(request->callback_ctx, NULL, NULL);
        rem_request(request);
    }

    return;
  }

  /*
   * If this fails there was an error decoding the received packet,
   * try it again and hope it works the next time.
   */
  if (!proc_answer(request, header, buf, buf + rc))
  {
    ++server->failure_count;
    resend_query(request);
    return;
  }

  if (request->type == T_PTR)
  {
    if (request->name == NULL)
    {
      /*
       * got a PTR response with no name, something bogus is happening
       * don't bother trying again, the client address doesn't resolve
       */
      ++server->failure_count;
      unlink_request(request);
      (*request->callback)(request->callback_ctx, NULL, NULL);
      rem_request(request);
      return;
    }
    else
    {
      unlink_request(request);

      /*
       * Lookup the 'authoritative' name that we were given for the
       * ip#.
       *
       */
#ifdef IPV6
      if (request->addr.ss.ss_family == AF_INET6)
        gethost_byname_type(request->handle, request->callback, request->callback_ctx, request->name, T_AAAA);
      else
#endif
        gethost_byname_type(request->handle, request->callback, request->callback_ctx, request->name, T_A);

      /* remove request */
      rem_request(request);
    }
  }
  else
  {
    /*
     * got a name and address response, client resolved
     */
    unlink_request(request);
    (*request->callback)(request->callback_ctx, &request->addr, request->name);
    rem_request(request);
  }

  server->failure_count /= 4;
}

void
report_dns_servers(struct Client *source_p)
{
  dlink_node *node;
  char ipaddr[HOSTIPLEN + 1];

  DLINK_FOREACH(node, irc_ns_list.head)
  {
    struct irc_ns *server = node->data;

    getnameinfo((struct sockaddr *) &server->addr,
                server->addr.ss_len, ipaddr,
                sizeof(ipaddr), NULL, 0, NI_NUMERICHOST);
    sendto_one(source_p, form_str(RPL_STATSALINE),
               me.name, source_p->name, ipaddr, server->failure_count);
  }
}
