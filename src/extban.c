/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2015-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "ircd_defs.h"
#include "list.h"
#include "extban.h"
#include "irc_string.h"
#include "channel.h"
#include "channel_mode.h"
#include "conf.h"
#include "s_user.h"

static dlink_list extbans;

static unsigned int matching_mask, acting_mask;

struct Extban extban_forward =
{
  .character = 'f',
  .type = EXTBAN_MATCHING,
  .types = CHFL_EXCEPTION
};

static unsigned int
find_mask()
{
  dlink_node *ptr;
  unsigned int used = 0;
  unsigned int i;

  DLINK_FOREACH(ptr, extbans.head)
  {
    struct Extban *s = ptr->data;
    used |= s->flag;
  }

  for (i = 1; i < EXTBAN_MASK && used & i; i <<= 1);

  return i;
}

void
extban_init(void)
{
  const char *ptr;

  extban_add(&extban_forward);

  if (ConfigFileEntry.enable_extbans && (ptr = extban_get_isupport()))
    add_isupport("EXTBAN", ptr, -1);
}

void
extban_add(struct Extban *ext)
{
  unsigned int mask = find_mask();
  if (!mask)
    return;

  ext->flag = mask;
  dlinkAdd(ext, &ext->node, &extbans);

  if (ext->type & EXTBAN_MATCHING)
    matching_mask |= mask;
  if (ext->type & EXTBAN_ACTING)
    acting_mask |= mask;
}

void
extban_del(struct Extban *ext)
{
  if (!ext->flag)
    return;

  dlinkDelete(&ext->node, &extbans);

  matching_mask &= ~ext->flag;
  acting_mask &= ~ext->flag;
}

struct Extban *
extban_find(char c)
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, extbans.head)
  {
    struct Extban *extban = ptr->data;

    if (extban->character == c)
      return extban;
  }

  return NULL;
}

struct Extban *
extban_find_flag(unsigned int flag)
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, extbans.head)
  {
    struct Extban *extban = ptr->data;

    if (extban->flag == flag)
      return extban;
  }

  return NULL;
}

enum extban_type
extban_parse(const char *mask, unsigned int *input_extbans, unsigned int *offset)
{
  *input_extbans = 0;
  *offset = 0;

  if (IsAlpha(*mask) && *(mask + 1) == ':')
  {
    struct Extban *ext = extban_find(*mask);

    if (ext == NULL)
      return EXTBAN_INVALID;

    *input_extbans |= ext->flag;
    *offset += 2;
    mask += 2;

    /* matching extbans take a special parameter, so stop reading */
    if (ext->type == EXTBAN_MATCHING)
      return EXTBAN_MATCHING;

    if (IsAlpha(*mask) && *(mask + 1) == ':')
    {
      ext = extban_find(*mask);
      if (ext == NULL)
        return EXTBAN_INVALID;

      /* two acting extbans make no sense */
      if (ext->type == EXTBAN_ACTING)
        return EXTBAN_INVALID;

      /* check parameter */
      if (ext->is_valid && ext->is_valid(mask) == EXTBAN_INVALID)
        return EXTBAN_INVALID;

      *input_extbans |= ext->flag;
      *offset += 2;
      mask += 2;

      return EXTBAN_MATCHING;
    }

    return EXTBAN_ACTING;
  }

  return EXTBAN_NONE;
}

unsigned int
extban_format(unsigned int e, char *buf)
{
  unsigned int written = 0;
  dlink_node *ptr;

  DLINK_FOREACH(ptr, extbans.head)
  {
    struct Extban *extban = ptr->data;

    if (extban->type != EXTBAN_ACTING)
      continue;

    if (e & extban->flag)
    {
      *buf++ = extban->character;
      *buf++ = ':';

      written += 2;
      break;
    }
  }

  DLINK_FOREACH(ptr, extbans.head)
  {
    struct Extban *extban = ptr->data;

    if (extban->type != EXTBAN_MATCHING)
      continue;

    if (e & extban->flag)
    {
      *buf++ = extban->character;
      *buf++ = ':';

      written += 2;
      break;
    }
  }

  return written;
}

unsigned int
extban_matching_mask(void)
{
  return matching_mask;
}

unsigned int
extban_acting_mask(void)
{
  return acting_mask;
}

const char *
extban_get_isupport(void)
{
  static char buf[IRCD_BUFSIZE];
  unsigned char extban_chars[256] = { 0 };
  dlink_node *ptr;
  char *p;

  if (!dlink_list_length(&extbans))
    return NULL;

  strcpy(buf, ",");
  p = buf + 1;

  DLINK_FOREACH(ptr, extbans.head)
  {
    struct Extban *extban = ptr->data;

    extban_chars[(unsigned char) extban->character] = extban->character;
  }

  for (int i = 0; i < 256; ++i)
    if (extban_chars[i])
      *p++ = extban_chars[i];
  *p++ = 0;

  return buf;
}

