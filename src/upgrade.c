/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "memory.h"
#include "irc_string.h"
#include "hash.h"
#include "userhost.h"
#include "packet.h"
#include "s_bsd.h"
#include "conf.h"
#include "parse.h"
#include "send.h"
#include "s_user.h"
#include "restart.h"
#include "hostmask.h"
#include "channel_mode.h"
#include "s_serv.h"
#include "s_misc.h"
#include "upgrade.h"
#include "extban.h"
#include "listener.h"

extern mp_pool_t *ban_pool;

#ifdef HAVE_LIBJANSSON

#ifndef json_array_foreach
#define json_array_foreach(array, index, value) \
        for(index = 0; \
                index < json_array_size(array) && (value = json_array_get(array, index)); \
                index++)
#endif

#define UPGRADE_VER 1

/* libjansson requires valid UTF-8 strings, so we base64 all of our strings. */
static const char *
encode_string(const char *in)
{
  static char buf[IRCD_BUFSIZE * 2];
  if (!in)
    return NULL;
  if (base64_encode(in, buf, sizeof(buf)))
    strlcpy(buf, "<unknown>", sizeof(buf));
  return buf;
}

static const char *
decode_string(const char *in)
{
  static char buf[IRCD_BUFSIZE * 2];
  if (!in)
    return NULL;
  if (!*in)
    return "";
  if (base64_decode(in, buf, sizeof(buf)))
    strlcpy(buf, "<unknown>", sizeof(buf));
  return buf;
}

static json_t *
serialize_client(struct Client *client, int migration)
{
  json_t *j = json_object();

  json_object_set_new(j, "serv", json_string(client->servptr->name));
  json_object_set_new(j, "tsinfo", json_integer(client->tsinfo));
  json_object_set_new(j, "flags", json_integer(client->flags));
  json_object_set_new(j, "umodes", json_integer(client->umodes));
  json_object_set_new(j, "hopcount", json_integer(client->hopcount));
  json_object_set_new(j, "status", json_integer(client->status));
  json_object_set_new(j, "handler", json_integer(client->handler));
  json_object_set_new(j, "away", json_string(encode_string(client->away)));
  json_object_set_new(j, "name", json_string(encode_string(client->name)));
  json_object_set_new(j, "svid", json_string(encode_string(client->svid)));
  json_object_set_new(j, "suser", json_string(encode_string(client->suser)));
  json_object_set_new(j, "id", json_string(client->id));
  json_object_set_new(j, "username", json_string(client->username));
  json_object_set_new(j, "host", json_string(client->host));
  json_object_set_new(j, "realhost", json_string(client->realhost));
  json_object_set_new(j, "info", json_string(encode_string(client->info)));
  json_object_set_new(j, "sockhost", json_string(client->sockhost));
  json_object_set_new(j, "certfp", json_string(client->certfp));
  json_object_set_new(j, "cgisockhost", json_string(client->cgisockhost));
  json_object_set_new(j, "cgihost", json_string(client->cgihost));

  if (client->localClient != NULL)
  {
    if (client->localClient->listener != NULL)
    {
      json_object_set_new(j, "listener.name", json_string(client->localClient->listener->name));
      json_object_set_new(j, "listener.port", json_integer(client->localClient->listener->port));
    }
    json_object_set_new(j, "registration", json_integer(client->localClient->registration));
    json_object_set_new(j, "cap_client", json_integer(client->localClient->cap_client));
    json_object_set_new(j, "cap_active", json_integer(client->localClient->cap_active));
    json_object_set_new(j, "caps", json_integer(client->localClient->caps));
    json_object_set_new(j, "operflags", json_integer(client->localClient->operflags));
    json_object_set_new(j, "lasttime", json_integer(client->localClient->lasttime));
    json_object_set_new(j, "firsttime", json_integer(client->localClient->firsttime));
    json_object_set_new(j, "since", json_integer(client->localClient->since));
    json_object_set_new(j, "last_privmsg", json_integer(client->localClient->last_privmsg));
    json_object_set_new(j, "recv.messages", json_integer(client->localClient->recv.messages));
    json_object_set_new(j, "recv.bytes", json_integer(client->localClient->recv.bytes));
    json_object_set_new(j, "send.messages", json_integer(client->localClient->send.messages));
    json_object_set_new(j, "send.bytes", json_integer(client->localClient->send.bytes));
    if (!migration)
    {
      json_object_set_new(j, "fd", json_integer(client->localClient->fd.fd));
      json_object_set_new(j, "fd.ssl", json_integer((intptr_t) client->localClient->fd.ssl));
    }
    json_object_set_new(j, "passwd", json_string(encode_string(client->localClient->passwd)));
    if (!migration)
    {
      json_object_set_new(j, "cert", json_integer((intptr_t) client->localClient->cert));
    }
    json_object_set_new(j, "snomask.local", json_integer(client->localClient->snomask.local));
    json_object_set_new(j, "snomask.remote", json_integer(client->localClient->snomask.remote));
    json_object_set_new(j, "client_host", json_string(encode_string(client->localClient->client_host)));
    json_object_set_new(j, "client_server", json_string(encode_string(client->localClient->client_server)));
    json_object_set_new(j, "captured", json_integer(client->localClient->captured));

    if (HasUMode(client, UMODE_OPER))
    {
      dlink_node *cnode = client->localClient->confs.head;
      if (cnode)
      {
        const struct MaskItem *conf = cnode->data;

        if (IsConfOperator(conf))
          json_object_set_new(j, "oper", json_string(conf->name));
      }
    }

    json_t *q = json_array();
    dlink_node *ptr;
    DLINK_FOREACH(ptr, client->localClient->buf_sendq.blocks.head)
    {
      struct dbuf_block *block = ptr->data;
      int offset = ptr == client->localClient->buf_sendq.blocks.head ? client->localClient->buf_sendq.pos : 0;
      char *p = xstrndup(block->data + offset, block->size - offset);
      json_array_append_new(q, json_string(encode_string(p)));
      MyFree(p);
    }
    json_object_set_new(j, "sendq", q);

    q = json_array();
    DLINK_FOREACH(ptr, client->localClient->buf_recvq.blocks.head)
    {
      struct dbuf_block *block = ptr->data;
      int offset = ptr == client->localClient->buf_recvq.blocks.head ? client->localClient->buf_recvq.pos : 0;
      char *p = xstrndup(block->data + offset, block->size - offset);
      json_array_append_new(q, json_string(encode_string(p)));
      MyFree(p);
    }
    json_object_set_new(j, "recvq", q);

    if (IsServer(client))
    {
      q = json_array();
      DLINK_FOREACH(ptr, client->serv->migrate_sendq.blocks.head)
      {
        struct dbuf_block *block = ptr->data;
        char *p = xstrndup(block->data, block->size);
        json_array_append_new(q, json_string(encode_string(p)));
        MyFree(p);
      }
      json_object_set_new(j, "migrate_sendq", q);
    }

    q = json_array();
    DLINK_FOREACH(ptr, client->localClient->acceptlist.head)
    {
      struct split_nuh_item *accept_p = ptr->data;
      json_t *accept = json_object();

      json_object_set_new(accept, "nick", json_string(encode_string(accept_p->nickptr)));
      json_object_set_new(accept, "user", json_string(encode_string(accept_p->userptr)));
      json_object_set_new(accept, "host", json_string(encode_string(accept_p->hostptr)));

      json_array_append_new(q, accept);
    }
    json_object_set_new(j, "accepts", q);
  }

  return j;
}

static json_t *
serialize_ban(struct Ban *ban)
{
  json_t *b = json_object();

  json_object_set_new(b, "extban", json_integer(ban->extban));
  json_object_set_new(b, "banstr", json_string(encode_string(ban->banstr)));
  json_object_set_new(b, "name", json_string(encode_string(ban->name)));
  json_object_set_new(b, "user", json_string(encode_string(ban->user)));
  json_object_set_new(b, "host", json_string(encode_string(ban->host)));
  json_object_set_new(b, "forward", json_string(encode_string(ban->forward)));
  json_object_set_new(b, "who", json_string(ban->who));
  json_object_set_new(b, "when", json_integer(ban->when));

  return b;
}

static json_t *
serialize_channel(struct Channel *channel)
{
  json_t *j = json_object();

  json_object_set_new(j, "chname", json_string(encode_string(channel->chname)));
  json_object_set_new(j, "mode.mode", json_integer(channel->mode.mode));
  json_object_set_new(j, "mode.limit", json_integer(channel->mode.limit));
  json_object_set_new(j, "mode.key", json_string(encode_string(channel->mode.key)));
  json_object_set_new(j, "topic", json_string(encode_string(channel->topic)));
  json_object_set_new(j, "topic_info", json_string(channel->topic_info));
  json_object_set_new(j, "chnanelts", json_integer(channel->channelts));
  json_object_set_new(j, "topic_time", json_integer(channel->topic_time));
  json_object_set_new(j, "flags", json_integer(channel->flags));

  json_t *members = json_array();
  dlink_node *ptr;
  DLINK_FOREACH(ptr, channel->members.head)
  {
    struct Membership *mem = ptr->data;
    json_t *m = json_object();

    json_object_set_new(m, "client", json_string(mem->client_p->name));
    json_object_set_new(m, "flags", json_integer(mem->flags));

    json_array_append_new(members, m);
  }
  json_object_set_new(j, "members", members);

  json_t *bans = json_array();
  DLINK_FOREACH(ptr, channel->banlist.head)
  {
    struct Ban *ban = ptr->data;
    json_array_append_new(bans, serialize_ban(ban));
  }
  json_object_set_new(j, "bans", bans);

  json_t *excepts = json_array();
  DLINK_FOREACH(ptr, channel->exceptlist.head)
  {
    struct Ban *ban = ptr->data;
    json_array_append_new(excepts, serialize_ban(ban));
  }
  json_object_set_new(j, "excepts", excepts);

  json_t *invexs = json_array();
  DLINK_FOREACH(ptr, channel->invexlist.head)
  {
    struct Ban *ban = ptr->data;
    json_array_append_new(invexs, serialize_ban(ban));
  }
  json_object_set_new(j, "invexs", invexs);

  return j;
}

static void
recurse_serialize_servers(json_t *clients, struct Client *root, int migration)
{
  dlink_node *node;

  DLINK_FOREACH(node, root->serv->server_list.head)
  {
    struct Client *client = node->data;
    json_array_append_new(clients, serialize_client(client, migration));
    recurse_serialize_servers(clients, client, migration);
  }
}

static json_t *
serialize_clients(int migration)
{
  json_t *clients = json_array();

  recurse_serialize_servers(clients, &me, migration);

  dlink_node *ptr;
  DLINK_FOREACH(ptr, global_client_list.head)
  {
    struct Client *cptr = ptr->data;
    if (!IsMe(cptr) && !IsServer(cptr))
      json_array_append_new(clients, serialize_client(cptr, migration));
  }

  return clients;
}

static json_t *
serialize_channels()
{
  json_t *channels = json_array();

  dlink_node *ptr;
  DLINK_FOREACH(ptr, global_channel_list.head)
  {
    struct Channel *ch = ptr->data;
    json_array_append_new(channels, serialize_channel(ch));
  }

  return channels;
}

json_t *
upgrade_serialize(int migration)
{
  json_t *j = json_object();
  if (j == NULL)
    return NULL;

  json_object_set_new(j, "version", json_integer(UPGRADE_VER));

  json_object_set_new(j, "me.name", json_string(me.name));
  json_object_set_new(j, "me.id", json_string(me.id));

  json_object_set_new(j, "clients", serialize_clients(migration));
  json_object_set_new(j, "channels", serialize_channels());

  json_object_set_new(j, "count.totalrestartcount", json_integer(Count.totalrestartcount));
  json_object_set_new(j, "count.max_loc", json_integer(Count.max_loc));
  json_object_set_new(j, "count.max_tot", json_integer(Count.max_tot));
  json_object_set_new(j, "count.max_loc_con", json_integer(Count.max_loc_con));
  json_object_set_new(j, "count.max_loc_cli", json_integer(Count.max_loc_cli));

  json_object_set_new(j, "config.cloak_key", json_string(cloak_key));

  return j;
}

static struct Client *
find_from(struct Client *client_p)
{
  return IsMe(client_p->servptr) ? client_p : find_from(client_p->servptr);
}

static void
unserialize_client(json_t *client, int migrate)
{
  struct Client *client_p;
  const char *p;

  client_p = mp_pool_get(client_pool);
  memset(client_p, 0, sizeof(*client_p));

  client_p->servptr = hash_find_server(json_string_value(json_object_get(client, "serv")));
  client_p->from = find_from(client_p);
  client_p->tsinfo = json_integer_value(json_object_get(client, "tsinfo"));
  client_p->flags = json_integer_value(json_object_get(client, "flags"));
  client_p->umodes = json_integer_value(json_object_get(client, "umodes"));
  client_p->hopcount = json_integer_value(json_object_get(client, "hopcount"));
  client_p->status = json_integer_value(json_object_get(client, "status"));
  client_p->handler = json_integer_value(json_object_get(client, "handler"));
  strlcpy(client_p->away, decode_string(json_string_value(json_object_get(client, "away"))), sizeof(client_p->away));
  strlcpy(client_p->name, decode_string(json_string_value(json_object_get(client, "name"))), sizeof(client_p->name));
  strlcpy(client_p->svid, decode_string(json_string_value(json_object_get(client, "svid"))), sizeof(client_p->svid));
  strlcpy(client_p->suser, decode_string(json_string_value(json_object_get(client, "suser"))), sizeof(client_p->suser));
  strlcpy(client_p->id, json_string_value(json_object_get(client, "id")), sizeof(client_p->id));
  strlcpy(client_p->username, json_string_value(json_object_get(client, "username")), sizeof(client_p->username));
  strlcpy(client_p->host, json_string_value(json_object_get(client, "host")), sizeof(client_p->host));
  strlcpy(client_p->realhost, json_string_value(json_object_get(client, "realhost")), sizeof(client_p->realhost));
  strlcpy(client_p->info, decode_string(json_string_value(json_object_get(client, "info"))), sizeof(client_p->info));
  strlcpy(client_p->sockhost, json_string_value(json_object_get(client, "sockhost")), sizeof(client_p->sockhost));
  p = json_string_value(json_object_get(client, "certfp"));
  if (p)
    client_p->certfp = xstrdup(p);
  p = json_string_value(json_object_get(client, "cgisockhost"));
  if (p)
    client_p->cgisockhost = xstrdup(p);
  p = json_string_value(json_object_get(client, "cgihost"));
  if (p)
    client_p->cgihost = xstrdup(p);

  if (IsMe(client_p->servptr))
  {
    client_p->localClient = mp_pool_get(lclient_pool);
    memset(client_p->localClient, 0, sizeof(*client_p->localClient));

    client_set_ip(client_p, client_p->sockhost);
    p = json_string_value(json_object_get(client, "listener.name"));
    if (p)
    {
      int port = json_integer_value(json_object_get(client, "listener.port"));
      struct Listener *listener = find_listener(p, port);

      if (listener != NULL)
      {
        client_p->localClient->listener = listener;
        ++listener->ref_count;
      }
    }
    client_p->localClient->registration = json_integer_value(json_object_get(client, "registration"));
    client_p->localClient->cap_client = json_integer_value(json_object_get(client, "cap_client"));
    client_p->localClient->cap_active = json_integer_value(json_object_get(client, "cap_active"));
    client_p->localClient->caps = json_integer_value(json_object_get(client, "caps"));
    client_p->localClient->operflags = json_integer_value(json_object_get(client, "operflags"));
    client_p->localClient->lasttime = json_integer_value(json_object_get(client, "lasttime"));
    client_p->localClient->firsttime = json_integer_value(json_object_get(client, "firsttime"));
    client_p->localClient->since = json_integer_value(json_object_get(client, "since"));
    client_p->localClient->last_privmsg = json_integer_value(json_object_get(client, "last_privmsg"));
    client_p->localClient->recv.messages = json_integer_value(json_object_get(client, "recv.messages"));
    client_p->localClient->recv.bytes = json_integer_value(json_object_get(client, "recv.bytes"));
    client_p->localClient->send.messages = json_integer_value(json_object_get(client, "send.messages"));
    client_p->localClient->send.bytes = json_integer_value(json_object_get(client, "send.bytes"));
    if (!migrate)
    {
      fd_open(&client_p->localClient->fd, json_integer_value(json_object_get(client, "fd")), 1, NULL);
      client_p->localClient->fd.ssl = (SSL *) json_integer_value(json_object_get(client, "fd.ssl"));
    }
    p = decode_string(json_string_value(json_object_get(client, "passwd")));
    if (p)
      client_p->localClient->passwd = xstrdup(p);
    if (!migrate)
    {
      client_p->localClient->cert = (X509 *) json_integer_value(json_object_get(client, "cert"));
    }
    client_p->localClient->snomask.local = json_integer_value(json_object_get(client, "snomask.local"));
    client_p->localClient->snomask.remote = json_integer_value(json_object_get(client, "snomask.remote"));
    p = decode_string(json_string_value(json_object_get(client, "client_host")));
    if (p)
      strlcpy(client_p->localClient->client_host, p, sizeof(client_p->localClient->client_host));
    p = decode_string(json_string_value(json_object_get(client, "client_server")));
    if (p)
      strlcpy(client_p->localClient->client_server, p, sizeof(client_p->localClient->client_server));
    client_p->localClient->captured = json_integer_value(json_object_get(client, "captured"));

    {
      json_t *q = json_object_get(client, "sendq");
      size_t index;
      json_t *value;

      json_array_foreach(q, index, value)
      {
        struct dbuf_block *block = dbuf_alloc();
        dbuf_put_fmt(block, "%s", decode_string(json_string_value(value)));
        dbuf_add(&client_p->localClient->buf_sendq, block);
        dbuf_ref_free(block);
      }
    }

    {
      json_t *q = json_object_get(client, "recvq");
      size_t index;
      json_t *value;

      json_array_foreach(q, index, value)
      {
        struct dbuf_block *block = dbuf_alloc();
        dbuf_put_fmt(block, "%s", decode_string(json_string_value(value)));
        dbuf_add(&client_p->localClient->buf_recvq, block);
        dbuf_ref_free(block);
      }
    }

    if (IsServer(client_p))
    {
      json_t *q = json_object_get(client, "migrate_sendq");
      size_t index;
      json_t *value;

      make_server(client_p);

      json_array_foreach(q, index, value)
      {
        struct dbuf_block *block = dbuf_alloc();
        dbuf_put_fmt(block, "%s", decode_string(json_string_value(value)));
        dbuf_add(&client_p->serv->migrate_sendq, block);
        dbuf_ref_free(block);
      }
    }

    {
      json_t *q = json_object_get(client, "accepts");
      size_t index;
      json_t *value;

      json_array_foreach(q, index, value)
      {
        struct split_nuh_item *accept_p = MyMalloc(sizeof(struct split_nuh_item));

        accept_p->nickptr = xstrdup(decode_string(json_string_value(json_object_get(value, "nick"))));
        accept_p->userptr = xstrdup(decode_string(json_string_value(json_object_get(value, "user"))));
        accept_p->hostptr = xstrdup(decode_string(json_string_value(json_object_get(value, "host"))));

        dlinkAddTail(accept_p, &accept_p->node, &client_p->localClient->acceptlist);
      }
    }
  }

  if (IsClient(client_p))
  {
    dlinkAdd(client_p, &client_p->node, &global_client_list);

    dlinkAdd(client_p, &client_p->lnode, &client_p->servptr->serv->client_list);

    hash_add_client(client_p);
    hash_add_id(client_p);

    userhost_add(client_p->realhost, MyClient(client_p) ? 0 : 1);

    if (MyClient(client_p))
    {
      dlinkAdd(client_p, &client_p->localClient->lclient_node, &local_client_list);

      /* try to locate iline, if not we will fall back to the default class */
      struct MaskItem *conf = find_address_conf(client_p, client_p->username, client_p->localClient->passwd);
      if (conf && IsConfClient(conf) && !IsConfRedir(conf) && !IsNeedPassword(conf))
        attach_conf(client_p, conf);

      if (HasUMode(client_p, UMODE_OPER))
      {
        p = json_string_value(json_object_get(client, "oper"));
        conf = NULL;

        if (p)
          conf = find_exact_name_conf(CONF_OPER, NULL, p, NULL, NULL);
        if (conf)
          attach_conf(client_p, conf);

        dlinkAdd(client_p, make_dlink_node(), &oper_list);
      }
    }
  }
  else if (IsServer(client_p))
  {
    make_server(client_p);

    hash_add_id(client_p);
    hash_add_client(client_p);

    dlinkAdd(client_p, &client_p->node, &global_client_list);
    dlinkAdd(client_p, make_dlink_node(), &global_serv_list);

    if (MyConnect(client_p))
    {
      set_chcap_usage_counts(client_p);

      dlinkAdd(client_p, &client_p->lnode, &me.serv->server_list);
      dlinkAdd(client_p, &client_p->localClient->lclient_node, &serv_list);

      struct MaskItem *conf = find_matching_name_conf(CONF_SERVER, client_p->name, NULL, NULL, 0);
      if (conf)
        attach_conf(client_p, conf);
    }
    else
    {
      dlinkAddTail(client_p, &client_p->lnode, &client_p->servptr->serv->server_list);
    }
  }
}

static struct Ban *
unserialize_ban(json_t *value)
{
    struct Ban *ban = mp_pool_get(ban_pool);
    const char *p;

    memset(ban, 0, sizeof(*ban));

    ban->extban = json_integer_value(json_object_get(value, "extban"));
    strlcpy(ban->name, decode_string(json_string_value(json_object_get(value, "name"))), sizeof(ban->name));
    strlcpy(ban->user, decode_string(json_string_value(json_object_get(value, "user"))), sizeof(ban->user));
    strlcpy(ban->host, decode_string(json_string_value(json_object_get(value, "host"))), sizeof(ban->host));
    p = decode_string(json_string_value(json_object_get(value, "forward")));
    if (p != NULL)
      strlcpy(ban->forward, p, sizeof(ban->forward));
    strlcpy(ban->who, json_string_value(json_object_get(value, "who")), sizeof(ban->who));
    ban->when = json_integer_value(json_object_get(value, "when"));
    if (ban->extban != EXTBAN_MATCHING)
      ban->type = parse_netmask(ban->host, &ban->addr, &ban->bits);

    p = decode_string(json_string_value(json_object_get(value, "banstr")));
    if (p != NULL)
    {
      strlcpy(ban->banstr, p, sizeof(ban->banstr));
    }
    else
    {
      strlcpy(ban->banstr, get_mask(ban), sizeof(ban->banstr));
    }

    return ban;
}

static void
unserialize_channel(json_t *channel)
{
  struct Channel *ch = make_channel(decode_string(json_string_value(json_object_get(channel, "chname"))));
  ch->mode.mode = json_integer_value(json_object_get(channel, "mode.mode"));
  ch->mode.limit = json_integer_value(json_object_get(channel, "mode.limit"));
  strlcpy(ch->mode.key, decode_string(json_string_value(json_object_get(channel, "mode.key"))), sizeof(ch->mode.key));
  strlcpy(ch->topic, decode_string(json_string_value(json_object_get(channel, "topic"))), sizeof(ch->topic));
  strlcpy(ch->topic_info, json_string_value(json_object_get(channel, "topic_info")), sizeof(ch->topic_info));
  ch->channelts = json_integer_value(json_object_get(channel, "chnanelts"));
  ch->topic_time = json_integer_value(json_object_get(channel, "topic_time"));
  ch->flags = json_integer_value(json_object_get(channel, "flags"));

  json_t *members = json_object_get(channel, "members");
  size_t index;
  json_t *value;

  json_array_foreach(members, index, value)
  {
    struct Client *client = hash_find_user(json_string_value(json_object_get(value, "client")));

    add_user_to_channel(ch, client, json_integer_value(json_object_get(value, "flags")), 0);
  }

  json_t *bans = json_object_get(channel, "bans");
  json_array_foreach(bans, index, value)
  {
    struct Ban *ban = unserialize_ban(value);
    dlinkAdd(ban, &ban->node, &ch->banlist);
  }

  json_t *excepts = json_object_get(channel, "excepts");
  json_array_foreach(excepts, index, value)
  {
    struct Ban *ban = unserialize_ban(value);
    dlinkAdd(ban, &ban->node, &ch->exceptlist);
  }

  json_t *invexs = json_object_get(channel, "invexs");
  json_array_foreach(invexs, index, value)
  {
    struct Ban *ban = unserialize_ban(value);
    dlinkAdd(ban, &ban->node, &ch->invexlist);
  }
}

static void
unserlialize_clients(json_t *clients, int migrate)
{
  size_t index;
  json_t *value;

  json_array_foreach(clients, index, value)
    unserialize_client(value, migrate);
}

static void
unserialize_channels(json_t *channels)
{
  size_t index;
  json_t *value;

  json_array_foreach(channels, index, value)
    unserialize_channel(value);
}

static void
fix_counts()
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, global_client_list.head)
  {
    struct Client *cptr = ptr->data;

    if (IsClient(cptr))
    {
      if (MyClient(cptr))
        ++Count.local;
      ++Count.total;

      if (HasUMode(cptr, UMODE_INVISIBLE))
        ++Count.invisi;

      if (HasUMode(cptr, UMODE_OPER))
        ++Count.oper;
    }
    else if (IsServer(cptr))
    {
      if (MyConnect(cptr))
        ++Count.myserver;
    }
  }
}

static void
integrity_check(json_t *root)
{
  dlink_node *ptr, *next;
  DLINK_FOREACH_SAFE(ptr, next, local_client_list.head)
  {
    struct Client *target_p = ptr->data;
    dlink_node *head = target_p->localClient->confs.head, *tail = target_p->localClient->confs.tail;
    struct MaskItem *conf_head = head ? head->data : NULL;
    struct MaskItem *conf_tail = tail ? tail->data : NULL;

    assert(!!conf_head == !!conf_tail);

    if (!conf_tail || !IsConfClient(conf_tail))
    {
      exit_client(target_p, &me, "Lost config");
      continue;
    }

    if (HasUMode(target_p, UMODE_OPER))
    {
      if (!(conf_head->type & CONF_OPER))
      {
        unsigned int setflags = target_p->umodes;
        struct Snomasks setsnomask = target_p->localClient->snomask;

        deoper(target_p);

        send_umode_out(target_p, target_p, setflags, &setsnomask);
      }
    }
  }

  DLINK_FOREACH_SAFE(ptr, next, serv_list.head)
  {
    struct Client *target_p = ptr->data;
    dlink_node *head = target_p->localClient->confs.head;
    struct MaskItem *conf = head ? head->data : NULL;

    if (!conf || !(conf->type & CONF_SERVER))
    {
      exit_client(target_p, &me, "Lost config");
      continue;
    }
  }

  const char *name = json_string_value(json_object_get(root, "me.name")),
             *id = json_string_value(json_object_get(root, "me.id"));

  if (!name || !id || strcmp(name, me.name) || strcmp(id, me.id))
    restart("Upgrade changed server name or ID");
}

static void
build_cloaks()
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, local_client_list.head)
  {
    struct Client *target_p = ptr->data;

    if (MyClient(target_p))
      cloak_client(&target_p->localClient->cloaks, target_p->realhost, target_p->sockhost);
  }
}

static void
start_io()
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, local_client_list.head)
  {
    struct Client *target_p = ptr->data;

    comm_setselect(&target_p->localClient->fd, COMM_SELECT_READ, read_packet, target_p);
    comm_setselect(&target_p->localClient->fd, COMM_SELECT_WRITE, (PF *) sendq_unblocked, target_p);
  }

  DLINK_FOREACH(ptr, serv_list.head)
  {
    struct Client *target_p = ptr->data;

    comm_setselect(&target_p->localClient->fd, COMM_SELECT_READ, read_packet, target_p);
    comm_setselect(&target_p->localClient->fd, COMM_SELECT_WRITE, (PF *) sendq_unblocked, target_p);
  }
}

void
upgrade_unserialize(json_t *root, int migrate)
{
  unserlialize_clients(json_object_get(root, "clients"), migrate);
  unserialize_channels(json_object_get(root, "channels"));

  Count.totalrestartcount = json_integer_value(json_object_get(root, "count.totalrestartcount"));
  Count.max_loc = json_integer_value(json_object_get(root, "count.max_loc"));
  Count.max_tot = json_integer_value(json_object_get(root, "count.max_tot"));
  Count.max_loc_con = json_integer_value(json_object_get(root, "count.max_loc_con"));
  Count.max_loc_cli = json_integer_value(json_object_get(root, "count.max_loc_cli"));
  fix_counts();

  const char *p = json_string_value(json_object_get(root, "config.cloak_key"));
  if (p)
  {
    MyFree(cloak_key);
    cloak_key = xstrdup(p);
  }

  integrity_check(root);

  build_cloaks();

  /* let mirgate() reestablish server connections and start io */
  if (!migrate)
    start_io();
}

void
upgrade(void)
{
  if (!plexus_upgrade)
    return;

  upgrade_unserialize(plexus_upgrade, 0);

  json_decref(plexus_upgrade);
  plexus_upgrade = NULL;

  sendto_snomask(SNO_ALL, L_ALL, "UPGRADE complete");
}

#endif // HAVE_LIBJANSSON
