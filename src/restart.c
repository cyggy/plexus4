/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file restart.c
 * \brief Functions to allow the ircd to restart.
 * \version $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "restart.h"
#include "fdlist.h"
#include "ircd.h"
#include "irc_string.h"
#include "send.h"
#include "log.h"
#include "client.h"
#include "memory.h"
#include "listener.h"
#include "upgrade.h"
#include "s_bsd.h"
#include "modules.h"
#include "hook.h"
#include "conf.h"
#include "ircd_signal.h"
#include "httpd.h"

#include <setjmp.h>

void
restart(const char *mesg)
{
  server_die(mesg, 1);
}

void
server_die(const char *mesg, int rboot)
{
  char buffer[IRCD_BUFSIZE];
  dlink_node *ptr = NULL;
  struct Client *target_p = NULL;
  static int was_here = 0;

  if (rboot && was_here++)
    abort();

  if (EmptyString(mesg))
    snprintf(buffer, sizeof(buffer), "Server %s",
             rboot ? "Restarting" : "Terminating");
  else
    snprintf(buffer, sizeof(buffer), "Server %s: %s",
             rboot ? "Restarting" : "Terminating", mesg);

  sendto_snomask(SNO_ALL, L_ALL, "%s", buffer);

  DLINK_FOREACH(ptr, serv_list.head)
  {
    target_p = ptr->data;

    sendto_one(target_p, ":%s ERROR :%s", me.name, buffer);
  }

  ilog(LOG_TYPE_IRCD, "%s", buffer);

  send_queued_all();
  close_fds(NULL);

  unlink(pidFileName);

  if (rboot)
  {
    execv(SPATH, myargv);
    exit(1);
  }
  else
    exit(0);
}

#ifdef HAVE_LIBJANSSON

extern jmp_buf plexus_upgrade_jmp;
extern json_t *plexus_upgrade;

void
server_upgrade(void)
{
  httpd_stop();

  remove_signals();

  /* we do not support upgrading with unregistered users */
  dlink_node *ptr, *ptr_next;
  DLINK_FOREACH_SAFE(ptr, ptr_next, unknown_list.head)
  {
    struct Client *target_p = ptr->data;

    exit_client(target_p, &me, "Upgrade in progress");
  }

  send_queued_all();

  plexus_upgrade = upgrade_serialize(0);

  close_listeners();
  stop_resolver();

  /* if we do not close all of our loaded libraries, when ircd closes us, ldl will not actually unload us,
   * which will cause ircd to abort the upgrade.
   */
  unload_all_modules();

  modules_deinit(); /* for libltdl */

  /* ssl profiles are included in config items, which are deleted here. identity_free clears the
   * callbacks into ircd.
   */
  clear_out_old_conf();

  log_close();

  shutdown_netio();

  unlink(pidFileName);

  longjmp(plexus_upgrade_jmp, 1);
}

#endif
