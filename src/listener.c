/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1999-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file listener.c
 * \brief Implementation for handling listening sockets.
 * \version $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "listener.h"
#include "client.h"
#include "fdlist.h"
#include "irc_string.h"
#include "ircd.h"
#include "ircd_defs.h"
#include "s_bsd.h"
#include "numeric.h"
#include "conf.h"
#include "send.h"
#include "memory.h"
#include "httpd.h"
#include <netinet/tcp.h>

static PF accept_connection;

static dlink_list ListenerPollList = { NULL, NULL, 0 };
static void close_listener(struct Listener *listener);

static struct Listener *
make_listener(const char *vhost_ip, int port, struct irc_ssaddr *addr)
{
  struct Listener *listener = MyMalloc(sizeof(struct Listener));

  if (!EmptyString(vhost_ip))
    strlcpy(listener->vhost_ip, vhost_ip, sizeof(listener->vhost_ip));
  listener->port = port;
  memcpy(&listener->addr, addr, sizeof(struct irc_ssaddr));

  return listener;
}

void
free_listener(struct Listener *listener)
{
  assert(listener != NULL);

  dlinkDelete(&listener->node, &ListenerPollList);
  MyFree(listener);
}

/*
 * get_listener_name - return displayable listener name and port
 * returns "host.foo.org/6667" for a given listener
 */
const char *
get_listener_name(const struct Listener *const listener)
{
  static char buf[HOSTLEN + HOSTLEN + PORTNAMELEN + 4];

  snprintf(buf, sizeof(buf), "%s[%s/%u]", me.name,
           listener->name, listener->port);
  return buf;
}

dlink_list *
get_listeners()
{
  return &ListenerPollList;
}

/* show_ports()
 *
 * inputs       - pointer to client to show ports to
 * output       - none
 * side effects - send port listing to a client
 */
void
show_ports(struct Client *source_p)
{
  char buf[IRCD_BUFSIZE];
  char *p = NULL;
  dlink_node *ptr;

  DLINK_FOREACH(ptr, ListenerPollList.head)
  {
    const struct Listener *listener = ptr->data;
    p = buf;

    if (listener->flags & LISTENER_HIDDEN)
    {
      if (!HasUMode(source_p, UMODE_ADMIN))
        continue;
      *p++ = 'H';
    }

    if (IsListenerServer(listener))
      *p++ = 'S';
    if (listener->flags & LISTENER_SSL)
      *p++ = 's';
    if (listener->flags & LISTENER_HTTP)
      *p++ = 'h';
    *p = '\0';
    sendto_one(source_p, form_str(RPL_STATSPLINE),
               me.name, source_p->name, 'P', listener->port,
               HasOFlag(source_p, OPER_FLAG_ADMIN) ? listener->name : me.name,
               listener->ref_count, buf,
               listener->active ? "active" : "disabled");
  }
}

/*
 * inetport - create a listener socket in the AF_INET or AF_INET6 domain,
 * bind it to the port given in 'port' and listen to it
 * returns true (1) if successful false (0) on error.
 *
 * If the operating system has a define for SOMAXCONN, use it, otherwise
 * use HYBRID_SOMAXCONN
 */
#ifdef SOMAXCONN
#undef HYBRID_SOMAXCONN
#define HYBRID_SOMAXCONN SOMAXCONN
#endif

static int
inetport(struct Listener *listener)
{
  struct irc_ssaddr lsin;
  socklen_t opt = 1;

  memset(&lsin, 0, sizeof(lsin));
  memcpy(&lsin, &listener->addr, sizeof(lsin));

  getnameinfo((struct sockaddr *)&lsin, lsin.ss_len, listener->name,
              sizeof(listener->name), NULL, 0, NI_NUMERICHOST);

  /*
   * At first, open a new socket
   */
  if (comm_open(&listener->fd, listener->addr.ss.ss_family, SOCK_STREAM, 0,
                "Listener socket") == -1)
  {
    report_error(L_ALL, "opening listener socket %s:%s",
                 get_listener_name(listener), errno);
    return 0;
  }

  if (setsockopt(listener->fd.fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)))
  {
    report_error(L_ALL, "setting SO_REUSEADDR for listener %s:%s",
                 get_listener_name(listener), errno);
    fd_close(&listener->fd);
    return 0;
  }

#ifdef IP_FREEBIND
  if (setsockopt(listener->fd.fd, SOL_IP, IP_FREEBIND, &opt, sizeof(opt)))
  {
    report_error(L_ALL, "setting IP_FREEBIND for listener %s:%s",
                 get_listener_name(listener), errno);
    // ignore
  }
#endif

#ifdef IPV6
#ifdef IPV6_V6ONLY
  /* This OS supports IPv6 sockets that can also listen for IPv4
   * connections. If our address is in6addr_any, enable both v4 and v6 to
   * allow for simpler configuration on dual-stack hosts. Otherwise, if it
   * an IPv6 address, disable support so that an IPv4 bind will
   * work on the port (by us or another application).
   */
  if (EmptyString(listener->vhost_ip) && listener->addr.ss.ss_family == AF_INET6)
  {
    struct sockaddr_in6 *v6 = (struct sockaddr_in6 *) &listener->addr;

    const int enable = !memcmp(&v6->sin6_addr, &in6addr_any, sizeof(v6->sin6_addr)) ? 0 : 1;
    setsockopt(listener->fd.fd, IPPROTO_IPV6, IPV6_V6ONLY, &enable, sizeof(enable));
    // errors ignored intentionally
  }
#endif
#endif

  /*
   * Bind a port to listen for new connections if port is non-null,
   * else assume it is already open and try get something from it.
   */
  lsin.ss_port = htons(listener->port);

  if (bind(listener->fd.fd, (struct sockaddr *)&lsin, lsin.ss_len))
  {
    report_error(L_ALL, "binding listener socket %s:%s",
                 get_listener_name(listener), errno);
    fd_close(&listener->fd);
    return 0;
  }

  if (listen(listener->fd.fd, HYBRID_SOMAXCONN))
  {
    report_error(L_ALL, "listen failed for %s:%s",
                 get_listener_name(listener), errno);
    fd_close(&listener->fd);
    return 0;
  }

#if defined TCP_DEFER_ACCEPT
  int timeout = DEFER_TIMEOUT;
  if ((listener->flags & LISTENER_DEFER) && timeout > 0)
    setsockopt(listener->fd.fd, IPPROTO_TCP, TCP_DEFER_ACCEPT, &timeout, sizeof(timeout));
#endif

  /* Listen completion events are READ events .. */

  accept_connection(&listener->fd, listener);
  return 1;
}

static struct Listener *
find_listener_addr(int port, struct irc_ssaddr *addr)
{
  dlink_node *ptr;
  struct Listener *listener    = NULL;
  struct Listener *last_closed = NULL;

  DLINK_FOREACH(ptr, ListenerPollList.head)
  {
    listener = ptr->data;

    if ((port == listener->port) &&
        (!memcmp(addr, &listener->addr, sizeof(struct irc_ssaddr))))
    {
      /* Try to return an open listener, otherwise reuse a closed one */
      if (!listener->fd.flags.open)
        last_closed = listener;
      else
        return (listener);
    }
  }

  return (last_closed);
}

struct Listener *
find_listener(const char *name, int port)
{
  struct addrinfo hints, *res;
  struct irc_ssaddr addr;
  char portname[PORTNAMELEN + 1];

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE | AI_NUMERICHOST;

  snprintf(portname, sizeof(portname), "%d", port);

  if (getaddrinfo(name, portname, &hints, &res))
    return NULL;

  assert(res != NULL);
  memset(&addr, 0, sizeof(addr));
  memcpy(&addr, res->ai_addr, res->ai_addrlen);
  addr.ss_port = htons(port);
  addr.ss_len = res->ai_addrlen;

  freeaddrinfo(res);

  return find_listener_addr(port, &addr);
}

/*
 * add_listener- create a new listener
 * port - the port number to listen on
 * vhost_ip - if non-null must contain a valid IP address string in
 * the format "255.255.255.255"
 */
struct Listener *
add_listener(int port, const char *vhost_ip, unsigned int flags, struct sslprofile *ssl_profile)
{
  struct Listener *listener;
  struct irc_ssaddr vaddr;
  struct addrinfo hints, *res;

  /*
   * if no or invalid port in conf line, don't bother
   */
  if (!(port > 0 && port <= 0xFFFF))
    return NULL;

  memset(&vaddr, 0, sizeof(vaddr));

  /* Set up the hints structure */
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  /* Get us ready for a bind() and don't bother doing dns lookup */
  hints.ai_flags = AI_PASSIVE | AI_NUMERICHOST;

  if (!EmptyString(vhost_ip))
  {
    char portname[PORTNAMELEN + 1];

    snprintf(portname, PORTNAMELEN, "%d", port);

    if (getaddrinfo(vhost_ip, portname, &hints, &res))
      return NULL;

    assert(res != NULL);

    memcpy(&vaddr, res->ai_addr, res->ai_addrlen);
    vaddr.ss_port = htons(port);
    vaddr.ss_len = res->ai_addrlen;
    freeaddrinfo(res);
  }
#ifdef IPV6
  else if (ServerInfo.can_use_v6)
  {
    struct sockaddr_in6 *v6 = (struct sockaddr_in6 *) &vaddr;

    vaddr.ss.ss_family = AF_INET6;
    vaddr.ss_port = htons(port);
    vaddr.ss_len = sizeof(struct sockaddr_in6);

    v6->sin6_addr = in6addr_any;
    v6->sin6_port = htons(port);
  }
#endif
  else
  {
    struct sockaddr_in *v4 = (struct sockaddr_in *) &vaddr;

    vaddr.ss.ss_family = AF_INET;
    vaddr.ss_port = htons(port);
    vaddr.ss_len = sizeof(struct sockaddr_in);

    v4->sin_addr.s_addr = INADDR_ANY;
    v4->sin_port = htons(port);
  }

  if ((listener = find_listener_addr(port, &vaddr)))
  {
    listener->flags = flags;
    if (listener->fd.flags.open)
      return listener;
  }
  else
  {
    listener = make_listener(vhost_ip, port, &vaddr);
    dlinkAdd(listener, &listener->node, &ListenerPollList);
    listener->flags = flags;
  }

  listener->ssl_profile = ssl_profile;

  if (inetport(listener))
    listener->active = 1;
  else
    close_listener(listener);

  return listener;
}

/*
 * close_listener - close a single listener
 */
static void
close_listener(struct Listener *listener)
{
  assert(listener != NULL);

  if (listener == NULL)
    return;

  if (listener->fd.flags.open)
    fd_close(&listener->fd);

  listener->active = 0;

  if (listener->ref_count)
    return;

  free_listener(listener);
}

/*
 * close_listeners - close and free all listeners that are not being used
 */
void
close_listeners(void)
{
  dlink_node *ptr = NULL, *next_ptr = NULL;

  /* close all 'extra' listening ports we have */
  DLINK_FOREACH_SAFE(ptr, next_ptr, ListenerPollList.head)
    close_listener(ptr->data);
}

static void
accept_connection(fde_t *pfd, void *data)
{
  static time_t last_oper_notice = 0;
  struct irc_ssaddr addr;
  int fd;
  const char *reason;
  struct Listener *listener = data;

  memset(&addr, 0, sizeof(addr));

  assert(listener != NULL);

  /* There may be many reasons for error return, but
   * in otherwise correctly working environment the
   * probable cause is running out of file descriptors
   * (EMFILE, ENFILE or others?). The man pages for
   * accept don't seem to list these as possible,
   * although it's obvious that it may happen here.
   * Thus no specific errors are tested at this
   * point, just assume that connections cannot
   * be accepted until some old is closed first.
   */
  while ((fd = comm_accept(listener, &addr)) != -1)
  {
    if (listener->flags & LISTENER_HTTP)
    {
      if (!httpd_add_connection(fd, &addr))
      {
        close(fd);
      }
      continue;
    }

    /*
     * check for connection limit
     */
    if (number_fd > hard_fdlimit - 10)
    {
      ++ServerStats.is_ref;

      /*
       * slow down the whining to opers bit
       */
      if ((last_oper_notice + 20) <= CurrentTime)
      {
        sendto_snomask(SNO_ALL, L_ALL,
                       "All connections in use. (%s)",
                       get_listener_name(listener));
        last_oper_notice = CurrentTime;
      }

      if (!(listener->flags & LISTENER_SSL))
      {
        const char buf[] = "ERROR :All connections in use\r\n";
        send(fd, buf, sizeof(buf) - 1, 0);
      }

      struct linger linger = { .l_onoff = 1, .l_linger = 0 };
      setsockopt(fd, SOL_SOCKET, SO_LINGER, &linger, sizeof(linger));

      close(fd);
      break;    /* jump out and re-register a new io request */
    }

    /*
     * Do an initial check we aren't connecting too fast or with too many
     * from this IP...
     */
    reason = conf_connect_allowed(&addr, addr.ss.ss_family);
    if (reason != NULL)
    {
      ++ServerStats.is_ref;

      if (!(listener->flags & LISTENER_SSL))
      {
        char buf[IRCD_BUFSIZE];
        int i = snprintf(buf, sizeof(buf), "ERROR :%s\r\n", reason);
        if (i > sizeof(buf))
          i = sizeof(buf);
        send(fd, buf, i, 0);
      }

      struct linger linger = { .l_onoff = 1, .l_linger = 0 };
      setsockopt(fd, SOL_SOCKET, SO_LINGER, &linger, sizeof(linger));

      close(fd);
      continue;    /* drop the one and keep on clearing the queue */
    }

    ++ServerStats.is_ac;
    add_connection(listener, &addr, fd);
  }

  /* Re-register a new IO request for the next accept .. */
  comm_setselect(&listener->fd, COMM_SELECT_READ, accept_connection,
                 listener);
}

void
listener_remove_sslprofile(struct sslprofile *profile)
{
  dlink_node *ptr;

  DLINK_FOREACH(ptr, ListenerPollList.head)
  {
    struct Listener *listener = ptr->data;

    if (listener->ssl_profile == profile)
      listener->ssl_profile = NULL;
  }
}
