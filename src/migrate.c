#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "memory.h"
#include "irc_string.h"
#include "hash.h"
#include "userhost.h"
#include "packet.h"
#include "s_bsd.h"
#include "conf.h"
#include "parse.h"
#include "send.h"
#include "s_user.h"
#include "restart.h"
#include "hostmask.h"
#include "channel_mode.h"
#include "s_serv.h"
#include "s_misc.h"
#include "upgrade.h"
#include "log.h"
#include "ssl.h"

#ifdef HAVE_LIBJANSSON

static void
serv_connect_callback(fde_t *fd, int status, void *data);

static int
serv_resume(struct Client *client_p)
{
  struct MaskItem *conf = find_matching_name_conf(CONF_SERVER, client_p->name, NULL, NULL, 0);
  struct sockaddr_in *v4;
  char buf[HOSTIPLEN + 1];

  if (conf == NULL)
  {
    ilog(LOG_TYPE_IRCD, "migrate: Unable to resume server %s, lost conf", client_p->name);
    return 0;
  }

  getnameinfo((struct sockaddr *)&conf->addr, conf->addr.ss_len,
              buf, sizeof(buf), NULL, 0, NI_NUMERICHOST);

  ilog(LOG_TYPE_IRCD, "Resuming %s[%s] @%s", conf->name, conf->host,
       buf);

  /* Still processing a DNS lookup? -> exit */
  if (conf->dns_query.ns != NULL)
  {
    ilog(LOG_TYPE_IRCD, "migrate: Unable to resume server %s, dns lookup in progress...", client_p->name);
    return (0);
  }

  if (conf->dns_failed)
  {
    ilog(LOG_TYPE_IRCD, "migrate: Unable to resume server %s, dns lookup failed", client_p->name);
    sendto_snomask(SNO_LINK, L_ALL | SNO_ROUTE,
                   "Error connecting to %s: DNS lookup for connect{} failed.",
                   conf->name);
    return (0);
  }

  strlcpy(client_p->sockhost, buf, sizeof(client_p->sockhost));

  /* create a socket for the server connection */
  if (comm_open(&client_p->localClient->fd, conf->addr.ss.ss_family, SOCK_STREAM, 0, NULL) < 0)
  {
    /* Eek, failure to create the socket */
    report_error(L_ALL, "opening stream socket to %s: %s",
                 conf->name, errno);
    SetDead(client_p);
    exit_client(client_p, &me, "Connection failed");
    return 0;
  }

  /* servernames are always guaranteed under HOSTLEN chars */
  fd_note(&client_p->localClient->fd, "Server: %s", client_p->name);

  /* Attach config entries to client here rather than in
   * serv_connect_callback(). This to avoid null pointer references.
   */
  if (!attach_connect_block(client_p, conf->name, conf->host))
  {
    ilog(LOG_TYPE_IRCD, "migrate: Unable to attach connect block on %s", client_p->name);
    SetDead(client_p);
    exit_client(client_p, client_p, "Connection failed");
    return 0;
  }

  client_p->localClient->aftype = conf->aftype;

  /* Now, initiate the connection */
  /* XXX assume that a non 0 type means a specific bind address
   * for this connect.
   */
  switch (conf->aftype)
  {
    case AF_INET:
      v4 = (struct sockaddr_in*)&conf->bind;
      if (v4->sin_addr.s_addr != 0)
      {
        struct irc_ssaddr ipn;
        memset(&ipn, 0, sizeof(struct irc_ssaddr));
        ipn.ss.ss_family = AF_INET;
        ipn.ss_port = 0;
        memcpy(&ipn, &conf->bind, sizeof(struct irc_ssaddr));
        return comm_connect_tcp(&client_p->localClient->fd, conf->host, conf->port,
                         (struct sockaddr *)&ipn, ipn.ss_len,
                         serv_connect_callback, client_p, conf->aftype,
                         CONNECTTIMEOUT);
      }
      else if (ServerInfo.specific_ipv4_vhost)
      {
        struct irc_ssaddr ipn;
        memset(&ipn, 0, sizeof(struct irc_ssaddr));
        ipn.ss.ss_family = AF_INET;
        ipn.ss_port = 0;
        memcpy(&ipn, &ServerInfo.ip, sizeof(struct irc_ssaddr));
        return comm_connect_tcp(&client_p->localClient->fd, conf->host, conf->port,
                         (struct sockaddr *)&ipn, ipn.ss_len,
                         serv_connect_callback, client_p, conf->aftype,
                         CONNECTTIMEOUT);
      }
      else
        return comm_connect_tcp(&client_p->localClient->fd, conf->host, conf->port,
                         NULL, 0, serv_connect_callback, client_p, conf->aftype,
                         CONNECTTIMEOUT);
      break;
#ifdef IPV6
    case AF_INET6:
      {
        struct irc_ssaddr ipn;
        struct sockaddr_in6 *v6;
        struct sockaddr_in6 *v6conf;

        memset(&ipn, 0, sizeof(struct irc_ssaddr));
        v6conf = (struct sockaddr_in6 *)&conf->bind;
        v6 = (struct sockaddr_in6 *)&ipn;

        if (memcmp(&v6conf->sin6_addr, &v6->sin6_addr, sizeof(struct in6_addr)) != 0)
        {
          memcpy(&ipn, &conf->bind, sizeof(struct irc_ssaddr));
          ipn.ss.ss_family = AF_INET6;
          ipn.ss_port = 0;
          return comm_connect_tcp(&client_p->localClient->fd,
                           conf->host, conf->port,
                           (struct sockaddr *)&ipn, ipn.ss_len,
                           serv_connect_callback, client_p,
                           conf->aftype, CONNECTTIMEOUT);
        }
        else if (ServerInfo.specific_ipv6_vhost)
        {
          memcpy(&ipn, &ServerInfo.ip6, sizeof(struct irc_ssaddr));
          ipn.ss.ss_family = AF_INET6;
          ipn.ss_port = 0;
          return comm_connect_tcp(&client_p->localClient->fd,
                           conf->host, conf->port,
                           (struct sockaddr *)&ipn, ipn.ss_len,
                           serv_connect_callback, client_p,
                           conf->aftype, CONNECTTIMEOUT);
        }
        else
          return comm_connect_tcp(&client_p->localClient->fd,
                           conf->host, conf->port,
                           NULL, 0, serv_connect_callback, client_p,
                           conf->aftype, CONNECTTIMEOUT);
      }
#endif
  }

  return 0;
}

static void
finish_ssl_server_handshake(struct Client *client_p)
{
  struct MaskItem *conf = NULL;

  conf = find_conf_name(&client_p->localClient->confs,
                        client_p->name, CONF_SERVER);
  if (conf == NULL)
  {
    sendto_snomask(SNO_LINK, L_ADMIN,
                   "Lost connect{} block for %s", get_client_name(client_p, HIDE_IP));
    sendto_snomask(SNO_LINK, L_OPER | SNO_ROUTE,
                   "Lost connect{} block for %s", get_client_name(client_p, MASK_IP));

    exit_client(client_p, &me, "Lost connect{} block");
    return;
  }

  sendto_one(client_p, "MIGRATE RESUME %s", me.name);

  /* If we've been marked dead because a send failed, just exit
   * here now and save everyone the trouble of us ever existing.
   */
  if (IsDead(client_p))
  {
      sendto_snomask(SNO_LINK, L_ADMIN,
                     "%s[%s] went dead during handshake",
                     client_p->name,
                     client_p->host);
      sendto_snomask(SNO_LINK, L_OPER | SNO_ROUTE,
                     "%s went dead during handshake", client_p->name);
      return;
  }

  dbuf_append_queue(&client_p->localClient->buf_sendq, &client_p->serv->migrate_sendq);

  comm_setselect(&client_p->localClient->fd, COMM_SELECT_READ, read_packet, client_p);
  comm_setselect(&client_p->localClient->fd, COMM_SELECT_WRITE, (PF *) sendq_unblocked, client_p);
}

static void
ssl_server_handshake(fde_t *fd, struct Client *client_p)
{
  int ret = 0;

  ERR_clear_error();
  if ((ret = SSL_connect(client_p->localClient->fd.ssl)) <= 0)
  {
    if ((CurrentTime - client_p->localClient->firsttime) > CONNECTTIMEOUT)
    {
      exit_client(client_p, &me, "Timeout during SSL handshake");
      return;
    }

    switch (SSL_get_error(client_p->localClient->fd.ssl, ret))
    {
      case SSL_ERROR_WANT_WRITE:
        comm_settimeout(fd, CONNECTTIMEOUT, (PF *) ssl_server_handshake, client_p);
        comm_setselect(&client_p->localClient->fd, COMM_SELECT_WRITE,
                       (PF *)ssl_server_handshake, client_p);
        return;
      case SSL_ERROR_WANT_READ:
        comm_settimeout(fd, CONNECTTIMEOUT, (PF *) ssl_server_handshake, client_p);
        comm_setselect(&client_p->localClient->fd, COMM_SELECT_READ,
                       (PF *)ssl_server_handshake, client_p);
        return;
      default:
      {
        const char *sslerr = ERR_error_string(ERR_get_error(), NULL);
        sendto_snomask(SNO_LINK, L_ALL | SNO_ROUTE,
                       "Error connecting to %s: %s", client_p->name,
                       sslerr ? sslerr : "unknown SSL error");
        exit_client(client_p, client_p, "Error during SSL handshake");
        return;
      }
    }
  }

  comm_settimeout(fd, 0, NULL, NULL);

  ssl_get_cert(client_p);

  finish_ssl_server_handshake(client_p);
}

static void
ssl_connect_init(struct Client *client_p, struct MaskItem *conf, fde_t *fd)
{
  struct sslprofile_identity *identity = sslprofile_identity_get_default_from_profile(conf->ssl_profile);

  if (!identity || ((client_p->localClient->fd.ssl = SSL_new(identity->client_ctx)) == NULL))
  {
    ilog(LOG_TYPE_IRCD, "SSL_new() ERROR! -- %s",
         ERR_error_string(ERR_get_error(), NULL));
    SetDead(client_p);
    exit_client(client_p, client_p, "SSL_new failed");
    return;
  }

  SSL_set_fd(fd->ssl, fd->fd);

  SSL_set_tlsext_host_name(fd->ssl, client_p->name);

  ssl_server_handshake(NULL, client_p);
}

/* serv_connect_callback() - complete a server connection.
 *
 * This routine is called after the server connection attempt has
 * completed. If unsucessful, an error is sent to ops and the client
 * is closed. If sucessful, it goes through the initialisation/check
 * procedures, the capabilities are sent, and the socket is then
 * marked for reading.
 */
static void
serv_connect_callback(fde_t *fd, int status, void *data)
{
  struct Client *client_p = data;
  struct MaskItem *conf = NULL;

  /* First, make sure its a real client! */
  assert(client_p != NULL);
  assert(&client_p->localClient->fd == fd);

  /* Next, for backward purposes, record the ip of the server */
  memcpy(&client_p->localClient->ip, &fd->connect.hostaddr,
         sizeof(struct irc_ssaddr));
  /* Check the status */
  if (status != COMM_OK)
  {
    /* We have an error, so report it and quit
     * Admins get to see any IP, mere opers don't *sigh*
     */
     if (ConfigServerHide.hide_server_ips)
       sendto_snomask(SNO_LINK, L_ADMIN,
                      "Error connecting to %s: %s",
                      client_p->name, comm_errstr(status));
     else
       sendto_snomask(SNO_LINK, L_ADMIN,
                      "Error connecting to %s[%s]: %s", client_p->name,
                      client_p->host, comm_errstr(status));

     sendto_snomask(SNO_LINK, L_OPER | SNO_ROUTE,
                    "Error connecting to %s: %s",
                    client_p->name, comm_errstr(status));

     /* If a fd goes bad, call dead_link() the socket is no
      * longer valid for reading or writing.
      */
     dead_link_on_write(client_p, 0);
     return;
  }

  /* COMM_OK, so continue the connection procedure */
  /* Get the C/N lines */
  conf = find_conf_name(&client_p->localClient->confs,
                        client_p->name, CONF_SERVER);
  if (conf == NULL)
  {
    ilog(LOG_TYPE_IRCD, "Lost connect{} block for %s", get_client_name(client_p, HIDE_IP));
    return;
  }

  if (IsConfSSL(conf))
  {
    ssl_connect_init(client_p, conf, fd);
    return;
  }

  sendto_one(client_p, "MIGRATE RESUME %s", me.name);

  if (IsDead(client_p))
  {
      ilog(LOG_TYPE_IRCD, "Server %s went dead during migration handshake",
                          client_p->name);
      return;
  }

  /* now we've resumed append queued sendq to the current sendq */
  dbuf_append_queue(&client_p->localClient->buf_sendq, &client_p->serv->migrate_sendq);

  comm_setselect(fd, COMM_SELECT_READ, read_packet, client_p);
  /* this blocked so poll for write */
  comm_setselect(fd, COMM_SELECT_WRITE, (PF *) sendq_unblocked, client_p);
}

static void
reestablish_servers()
{
  dlink_node *node;

  DLINK_FOREACH(node, serv_list.head)
  {
    struct Client *client_p = node->data;
    AddFlag(client_p, FLAGS_BLOCKED); // block writes until we know the fd is open
  }

  DLINK_FOREACH(node, serv_list.head)
  {
    struct Client *client_p = node->data;

    if (!serv_resume(client_p))
      ilog(LOG_TYPE_IRCD, "Couldn't resume server %s", client_p->name);
  }
}

void
migrate(void)
{
  json_error_t err;

  json_t *json = json_loadf(stdin, 0, &err);
  if (json == NULL)
  {
    server_die("Error migrating", 0);
    return;
  }

  upgrade_unserialize(json, 1);
  json_decref(json);

  if (!server_state.foreground)
    close_standard_fds();

  ilog(LOG_TYPE_IRCD, "Migration of state completed, resuming server connections");

  reestablish_servers();
}
#endif

