/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2018 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "ircd_defs.h"
#include "ircd.h"
#include "list.h"
#include "mempool.h"
#include "irc_string.h"
#include "client.h"
#include "hash.h"
#include "send.h"
#include "event.h"
#include "conf.h"

struct delayed_message
{
  char uid[IDLEN + 1];
  char chname[CHANNELLEN + 1];
  time_t ts;
  int privmsg;
  unsigned int flags;
  char message[IRCD_BUFSIZE];
  time_t created;
  dlink_node node;
};

static mp_pool_t *delay_pool;
static dlink_list delay_list;

static void delay_process(void *);

void
delay_init(void)
{
  delay_pool = mp_pool_new("delay message", sizeof(struct delayed_message), 128);
  eventAddIsh("delay_process", delay_process, NULL, 1);
}

void
delay_queue(struct Client *client, struct Channel *channel, int privmsg, unsigned int flags, const char *message)
{
  struct delayed_message *dmsg = mp_pool_get(delay_pool);
  strlcpy(dmsg->uid, client->id, sizeof(dmsg->uid));
  strlcpy(dmsg->chname, channel->chname, sizeof(dmsg->chname));
  dmsg->ts = channel->channelts;
  dmsg->privmsg = privmsg;
  dmsg->flags = flags;
  strlcpy(dmsg->message, message, sizeof(dmsg->message));
  dmsg->created = CurrentTime;
  dlinkAddTail(dmsg, &dmsg->node, &delay_list);
}

static void
delay_process(void *unused)
{
  dlink_node *ptr, *ptr_next;

  DLINK_FOREACH_SAFE(ptr, ptr_next, delay_list.head)
  {
    struct delayed_message *dmsg = ptr->data;

    if (dmsg->created + ConfigFileEntry.delay_secs > CurrentTime)
    {
      break;
    }

    struct Client *client = hash_find_id(dmsg->uid);
    if (client == NULL)
    {
      goto next;
    }

    struct Channel *channel = hash_find_channel(dmsg->chname);
    if (channel == NULL || dmsg->ts != channel->channelts)
    {
      goto next;
    }

    const char *command = dmsg->privmsg ? "PRIVMSG" : "NOTICE";
    const char *text = dmsg->message;
    char c = 0;
    channel_flags_to_prefix(dmsg->flags, &c);

    if (can_send(channel, client, NULL, text, !dmsg->privmsg) >= 0)
    {
      goto next;
    }

    if (c)
      sendto_channel_butone(client->from, client, channel, dmsg->flags,
                            SEND_FLAG_LOCAL | (msg_is_ctcp(text) ? SEND_FLAG_CTCP : 0), "%s %c%s :%s",
                            command, c, channel->chname, text);
    else
      sendto_channel_butone(client->from, client, channel, 0,
                            SEND_FLAG_LOCAL | (msg_is_ctcp(text) ? SEND_FLAG_CTCP : 0), "%s %s :%s",
                            command, channel->chname, text);

   next:
    dlinkDelete(ptr, &delay_list);
    mp_pool_release(dmsg);
  }
}
