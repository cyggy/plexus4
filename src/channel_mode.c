/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file channel_mode.c
 * \brief Controls modes on channels.
 * \version $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "channel.h"
#include "channel_mode.h"
#include "client.h"
#include "conf.h"
#include "hostmask.h"
#include "irc_string.h"
#include "ircd.h"
#include "numeric.h"
#include "s_serv.h"
#include "send.h"
#include "memory.h"
#include "mempool.h"
#include "parse.h"
#include "extban.h"
#include "hash.h"

/* 10 is a magic number in hybrid 6 NFI where it comes from -db */
#define BAN_FUDGE       10
#define NCHCAPS         (sizeof(channel_capabs)/sizeof(int))
#define NCHCAP_COMBOS   (1 << NCHCAPS)


static char nuh_mask[MAXPARA][IRCD_BUFSIZE];
/* some buffers for rebuilding channel/nick lists with ,'s */
static char modebuf[IRCD_BUFSIZE];
static char parabuf[MODEBUFLEN];
static struct ChModeChange mode_changes[IRCD_BUFSIZE];
static unsigned int mode_count;
static unsigned int mode_limit;  /* number of modes set other than simple */
static unsigned int simple_modes_mask;  /* bit mask of simple modes already set */
static int channel_capabs[] = { CAP_TS6 };
static struct ChCapCombo chcap_combos[NCHCAP_COMBOS];
extern mp_pool_t *ban_pool;


/* check_string()
 *
 * inputs       - string to check
 * side effects - Fixes a string so that the first white space found
 *                becomes an end of string marker (`\0`).
 *                returns the 'fixed' string or "*" if the string
 *                was empty.
 */
static void
check_string(char *s)
{
  char *str = s;

  assert(s);

  for (; *s; ++s)
  {
    if (IsSpace(*s))
    {
      *s = '\0';
      break;
    }
  }

  if (EmptyString(str))
  {
    strcpy(str, "*");
  }
}

const char *
get_mask(struct Ban *ban)
{
  static char buf[MODEBUFLEN];

  unsigned int i = extban_format(ban->extban, buf);
  assert(i <= sizeof(buf));

  /* matching extbans only use ban->host */
  if (ban->extban & extban_matching_mask())
    strlcpy(buf + i, ban->host, sizeof(buf) - i);
  else
    snprintf(buf + i, sizeof(buf) - i, "%s!%s@%s", ban->name, ban->user, ban->host);

  if (!EmptyString(ban->forward))
    strlcat(buf, ban->forward, sizeof(buf));

  return buf;
}


/*
 * Ban functions to work with mode +b/e/d/I
 */
/* add the specified ID to the channel.
 *   -is 8/9/00
 */

struct Ban *
add_id(struct Client *client_p, struct Channel *chptr, const char *banid, unsigned int type)
{
  dlink_list *list = NULL, *unlist = NULL;
  dlink_node *ban = NULL;
  struct Ban *ban_p = NULL;
  unsigned int num_mask;
  char banmask[IRCD_BUFSIZE];
  char *banptr = banmask;
  struct split_nuh_item nuh;
  enum extban_type etype = EXTBAN_NONE;
  unsigned int extbans = 0, offset;

  strlcpy(banmask, banid, sizeof(banmask));

  /* dont let local clients overflow the b/e/I lists */
  if (MyClient(client_p))
  {
    num_mask = dlink_list_length(&chptr->banlist) +
               dlink_list_length(&chptr->exceptlist) +
               dlink_list_length(&chptr->invexlist);

    if (num_mask >= ConfigChannel.max_bans)
    {
      sendto_one(client_p, form_str(ERR_BANLISTFULL),
                 me.name, client_p->name, chptr->chname, banmask);
      return NULL;
    }

    collapse(banmask);
  }

  if (ConfigFileEntry.enable_extbans)
  {
    etype = extban_parse(banmask, &extbans, &offset);
    banptr += offset;

    if (MyClient(client_p))
    {
      unsigned int extban_acting, extban_matching;

      if (etype == EXTBAN_INVALID)
      {
        sendto_one(client_p, form_str(ERR_INVALIDBAN),
                   me.name, client_p->name,
                   chptr->chname, banmask);
        return NULL;
      }

      extban_acting = extbans & extban_acting_mask();
      extban_matching = extbans & extban_matching_mask();

      if (extban_acting)
      {
        struct Extban *extban = extban_find_flag(extban_acting);
        if (extban == NULL || !(extban->types & type))
        {
          sendto_one(client_p, form_str(ERR_INVALIDBAN),
                     me.name, client_p->name,
                     chptr->chname, banmask);
          return NULL;
        }
      }

      if (extban_matching)
      {
        struct Extban *extban = extban_find_flag(extban_matching);
        if (extban == NULL || !(extban->types & type))
        {
          sendto_one(client_p, form_str(ERR_INVALIDBAN),
                     me.name, client_p->name,
                     chptr->chname, banmask);
          return NULL;
        }
      }
    }
  }

  /* don't allow empty bans */
  if (EmptyString(banptr))
    return NULL;

  ban_p = mp_pool_get(ban_pool);
  memset(ban_p, 0, sizeof(*ban_p));

  ban_p->extban = extbans;

  check_string(banptr);

  if (etype == EXTBAN_MATCHING)
  {
    /* matching extbans have their own format, don't try to parse it */
    strlcpy(ban_p->host, banptr, sizeof(ban_p->host));
  }
  else
  {
    nuh.nuhmask  = banptr;
    nuh.nickptr  = ban_p->name;
    nuh.userptr  = ban_p->user;
    nuh.hostptr  = ban_p->host;

    nuh.nicksize = sizeof(ban_p->name);
    nuh.usersize = sizeof(ban_p->user);
    nuh.hostsize = sizeof(ban_p->host);

    split_nuh(&nuh);

    ban_p->type = parse_netmask(ban_p->host, &ban_p->addr, &ban_p->bits);
  }

  if (ConfigFileEntry.enable_forwarding)
  {
    char *forward = strchr(ban_p->host, '#');
    if (forward != NULL && type == CHFL_BAN)
    {
      if (MyClient(client_p))
      {
        /* forwards with acting extbans don't make sense */
        if (etype == EXTBAN_ACTING)
        {
          mp_pool_release(ban_p);
          sendto_one(client_p, form_str(ERR_INVALIDBAN),
                     me.name, client_p->name,
                     chptr->chname, banmask);
          return NULL;
        }

        struct Channel *target_channel = hash_find_channel(forward);
        /* with extbans, don't check access, instead +e f: will be checked at join time  */
        if (!ConfigFileEntry.enable_extbans && (!target_channel || !has_member_flags(find_channel_link(client_p, target_channel), CHFL_CHANOP)))
        {
          mp_pool_release(ban_p);
          sendto_one(client_p, form_str(ERR_INVALIDBAN),
                     me.name, client_p->name,
                     chptr->chname, banmask);
          return NULL;
        }
      }

      strlcpy(ban_p->forward, forward, sizeof(ban_p->forward));
      *forward = 0;
    }
  }

  if (MyClient(client_p))
  {
    strlcpy(ban_p->banstr, get_mask(ban_p), sizeof(ban_p->banstr));
  }
  else
  {
    strlcpy(ban_p->banstr, banid, sizeof(ban_p->banstr));
  }

  switch (type)
  {
    case CHFL_BAN:
      list = &chptr->banlist;
      unlist = &chptr->unbanlist;
      clear_ban_cache(chptr);
      break;
    case CHFL_EXCEPTION:
      list = &chptr->exceptlist;
      unlist = &chptr->unexceptlist;
      clear_ban_cache(chptr);
      break;
    case CHFL_INVEX:
      list = &chptr->invexlist;
      unlist = &chptr->uninvexlist;
      break;
    default:
      assert(0);
      mp_pool_release(ban_p);
      return NULL;
  }

  DLINK_FOREACH(ban, list->head)
  {
    struct Ban *b = ban->data;

    if (!irccmp(b->banstr, ban_p->banstr))
    {
      mp_pool_release(ban_p);
      return NULL;
    }
  }

  ban_p->when = CurrentTime;

  if (IsClient(client_p))
    snprintf(ban_p->who, sizeof(ban_p->who), "%s!%s@%s",
             client_p->name, client_p->username, client_p->host);
  else if (IsHidden(client_p) || (IsServer(client_p) && ConfigServerHide.hide_servers))
    strlcpy(ban_p->who, me.name, sizeof(ban_p->who));
  else
    strlcpy(ban_p->who, client_p->name, sizeof(ban_p->who));

  dlinkAdd(ban_p, &ban_p->node, list);

  /* remove from unban list */
  DLINK_FOREACH(ban, unlist->head)
  {
    struct Ban *b = ban->data;

    if (!irccmp(b->banstr, ban_p->banstr))
    {
      remove_ban(b, unlist);
      break;
    }
  }

  return ban_p;
}

/*
 * inputs	- pointer to channel
 *		- pointer to ban id
 *		- type of ban, i.e. ban, exception, invex
 * output	- 0 for failure, 1 for success
 * side effects	-
 */
static const char *
del_id(struct Channel *chptr, const char *banid, unsigned int type)
{
  dlink_list *list, *unlist;
  dlink_node *ban;
  struct Ban *banptr;
  char banmask[IRCD_BUFSIZE];

  assert(banid);

  strlcpy(banmask, banid, sizeof(banmask));

  switch (type)
  {
    case CHFL_BAN:
      list = &chptr->banlist;
      unlist = &chptr->unbanlist;
      clear_ban_cache(chptr);
      break;
    case CHFL_EXCEPTION:
      list = &chptr->exceptlist;
      unlist = &chptr->unexceptlist;
      clear_ban_cache(chptr);
      break;
    case CHFL_INVEX:
      list = &chptr->invexlist;
      unlist = &chptr->uninvexlist;
      break;
    default:
      assert(0);
      return NULL;
  }

  DLINK_FOREACH(ban, list->head)
  {
    banptr = ban->data;

    if (!irccmp(banmask, banptr->banstr))
    {
      /* move to unban list */

      /* remove oldest entry if list is too long */
      if (dlink_list_length(unlist) >= ConfigChannel.max_bans / 3)
      {
        dlink_node *node = unlist->tail;

        if (node)
        {
          struct Ban *b = node->data;
          remove_ban(b, unlist);
        }
      }

      dlink_move_node(ban, list, unlist);

      return banptr->banstr;
    }
  }

  return NULL;
}

/* channel_modes()
 *
 * inputs       - pointer to channel
 *              - pointer to client
 *              - pointer to mode buf
 *              - pointer to parameter buf
 * output       - NONE
 * side effects - write the "simple" list of channel modes for channel
 * chptr onto buffer mbuf with the parameters in pbuf.
 */
void
channel_modes(struct Channel *chptr, struct Client *client_p,
              char *mbuf, char *pbuf)
{
  int i;

  *mbuf++ = '+';
  *pbuf = '\0';

  for (i = 'A'; i <= 'z'; ++i)
  {
    const struct ChannelMode *tab = &mode_table[i];

    if (chptr->mode.mode & tab->mode)
      *mbuf++ = tab->letter;
  }

  if (chptr->mode.limit)
  {
    *mbuf++ = 'l';

    if (IsServer(client_p) || HasFlag(client_p, FLAGS_SERVICE) || IsMember(client_p, chptr))
      pbuf += sprintf(pbuf, "%d ", chptr->mode.limit);
  }

  if (chptr->mode.key[0])
  {
    *mbuf++ = 'k';

    if (IsServer(client_p) || HasFlag(client_p, FLAGS_SERVICE) || IsMember(client_p, chptr))
      sprintf(pbuf, "%s ", chptr->mode.key);
  }

  *mbuf = '\0';
}

/* fix_key()
 *
 * inputs       - pointer to key to clean up
 * output       - pointer to cleaned up key
 * side effects - input string is modified
 *
 * stolen from Undernet's ircd  -orabidoo
 */
static char *
fix_key(char *arg)
{
  unsigned char *s, *t, c;

  for (s = t = (unsigned char *)arg; (c = *s); s++)
  {
    c &= 0x7f;

    if (c != ':' && c > ' ' && c != ',')
      *t++ = c;
  }

  *t = '\0';
  return arg;
}

/* bitmasks for various error returns that set_channel_mode should only return
 * once per call  -orabidoo
 */

#define SM_ERR_NOTS         0x00000001 /* No TS on channel  */
#define SM_ERR_NOOPS        0x00000002 /* No chan ops       */
#define SM_ERR_UNKNOWN      0x00000004
#define SM_ERR_RPL_B        0x00000008
#define SM_ERR_RPL_E        0x00000010
#define SM_ERR_NOTONCHANNEL 0x00000020 /* Not on channel    */
#define SM_ERR_RPL_I        0x00000040
#define SM_ERR_NOTOPER      0x00000080
#define SM_ERR_ONLYSERVER   0x00000100

/* Now lets do some stuff to keep track of what combinations of
 * servers exist...
 * Note that the number of combinations doubles each time you add
 * something to this list. Each one is only quick if no servers use that
 * combination, but if the numbers get too high here MODE will get too
 * slow. I suggest if you get more than 7 here, you consider getting rid
 * of some and merging or something. If it wasn't for irc+cs we would
 * probably not even need to bother about most of these, but unfortunately
 * we do. -A1kmm
 */

/* void init_chcap_usage_counts(void)
 *
 * Inputs	- none
 * Output	- none
 * Side-effects	- Initialises the usage counts to zero. Fills in the
 *                chcap_yes and chcap_no combination tables.
 */
void
init_chcap_usage_counts(void)
{
  unsigned long m, c, y, n;

  memset(chcap_combos, 0, sizeof(chcap_combos));

  /* For every possible combination */
  for (m = 0; m < NCHCAP_COMBOS; m++)
  {
    /* Check each capab */
    for (c = y = n = 0; c < NCHCAPS; c++)
    {
      if ((m & (1 << c)) == 0)
        n |= channel_capabs[c];
      else
        y |= channel_capabs[c];
    }

    chcap_combos[m].cap_yes = y;
    chcap_combos[m].cap_no  = n;
  }
}

/* void set_chcap_usage_counts(struct Client *serv_p)
 * Input: serv_p; The client whose capabs to register.
 * Output: none
 * Side-effects: Increments the usage counts for the correct capab
 *               combination.
 */
void
set_chcap_usage_counts(struct Client *serv_p)
{
  int n;

  for (n = 0; n < NCHCAP_COMBOS; n++)
  {
    if (((serv_p->localClient->caps & chcap_combos[n].cap_yes) ==
         chcap_combos[n].cap_yes) &&
        ((serv_p->localClient->caps & chcap_combos[n].cap_no) == 0))
    {
      chcap_combos[n].count++;
      return;
    }
  }

  /* This should be impossible -A1kmm. */
  assert(0);
}

/* void set_chcap_usage_counts(struct Client *serv_p)
 *
 * Inputs	- serv_p; The client whose capabs to register.
 * Output	- none
 * Side-effects	- Decrements the usage counts for the correct capab
 *                combination.
 */
void
unset_chcap_usage_counts(struct Client *serv_p)
{
  int n;

  for (n = 0; n < NCHCAP_COMBOS; n++)
  {
    if ((serv_p->localClient->caps & chcap_combos[n].cap_yes) ==
        chcap_combos[n].cap_yes &&
        (serv_p->localClient->caps & chcap_combos[n].cap_no) == 0)
    {
      /* Hopefully capabs can't change dynamically or anything... */
      assert(chcap_combos[n].count > 0);
      chcap_combos[n].count--;
      return;
    }
  }

  /* This should be impossible -A1kmm. */
  assert(0);
}

/* Mode functions handle mode changes for a particular mode... */
static void
chm_nosuch(struct Client *client_p, struct Client *source_p,
           struct Channel *chptr, int parc, int *parn,
           char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  if (*errors & SM_ERR_UNKNOWN)
    return;

  *errors |= SM_ERR_UNKNOWN;
  sendto_one(source_p, form_str(ERR_UNKNOWNMODE), me.name,
             source_p->name, c);
}

static void
chm_simple(struct Client *client_p, struct Client *source_p, struct Channel *chptr,
           int parc, int *parn, char **parv, int *errors, int alev, int dir,
           char c, unsigned int d)
{
  if (alev < CHACCESS_REMOTE && ConfigFileEntry.disable_chmodes && strchr(ConfigFileEntry.disable_chmodes, c))
    return chm_nosuch(client_p, source_p, chptr, parc, parn, parv, errors, alev, dir, c, d);

  if (alev < CHACCESS_HALFOP || (d == MODE_PRIVATE && alev < CHACCESS_CHANOP))
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if (d == MODE_PERSIST && alev < CHACCESS_REMOTE)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(ERR_NOPRIVS), me.name, source_p->name, "mode");
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if (d == MODE_SSLONLY && dir == MODE_ADD && alev < CHACCESS_REMOTE)
  {
    dlink_node *ptr;
    struct Client *target_p;
    int nossl = 0;

    DLINK_FOREACH(ptr, chptr->members.head)
    {
      target_p = ((struct Membership *) ptr->data)->client_p;

      if (HasFlag(target_p, FLAGS_SERVICE))
        continue;

      if (!HasUMode(target_p, UMODE_SSL))
      {
        nossl = 1;
        break;
      }
    }

    if (nossl)
    {
      if (!(*errors & SM_ERR_NOOPS))
        sendto_one(source_p, form_str(ERR_CANNOTCHANGECHANMODE), me.name,
            source_p->name, c, "all members must be connected via SSL");
      *errors |= SM_ERR_NOOPS;
      return;
    }
  }

  /* If have already dealt with this simple mode, ignore it */
  if (simple_modes_mask & d)
    return;

  simple_modes_mask |= d;

  /* setting + */
  if (dir == MODE_ADD && !(chptr->mode.mode & d))
  {
    chptr->mode.mode |= d;

    mode_changes[mode_count].letter = c;
    mode_changes[mode_count].dir = MODE_ADD;
    mode_changes[mode_count].caps = 0;
    mode_changes[mode_count].nocaps = 0;
    mode_changes[mode_count].id = NULL;
    mode_changes[mode_count].mems = ALL_MEMBERS;
    mode_changes[mode_count++].arg = NULL;
  }
  else if (dir == MODE_DEL && (chptr->mode.mode & d))
  {
    /* setting - */

    chptr->mode.mode &= ~d;

    mode_changes[mode_count].letter = c;
    mode_changes[mode_count].dir = MODE_DEL;
    mode_changes[mode_count].caps = 0;
    mode_changes[mode_count].nocaps = 0;
    mode_changes[mode_count].mems = ALL_MEMBERS;
    mode_changes[mode_count].id = NULL;
    mode_changes[mode_count++].arg = NULL;
  }
}

static void
chm_operonly(struct Client *client_p, struct Client *source_p, struct Channel *chptr,
            int parc, int *parn, char **parv, int *errors, int alev, int dir,
            char c, unsigned int d)
{
  if (alev < CHACCESS_HALFOP)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }
  else if (MyClient(source_p) && !HasUMode(source_p, UMODE_OPER))
  {
    if (!(*errors & SM_ERR_NOTOPER))
    {
      if (alev == CHACCESS_NOTONCHAN)
        sendto_one(source_p, form_str(ERR_NOTONCHANNEL),
                   me.name, source_p->name, chptr->chname);
      else
        sendto_one(source_p, form_str(ERR_NOPRIVILEGES),
                   me.name, source_p->name);
    }

    *errors |= SM_ERR_NOTOPER;
    return;
  }

  /* If have already dealt with this simple mode, ignore it */
  if (simple_modes_mask & d)
    return;

  simple_modes_mask |= d;

  if (dir == MODE_ADD && !(chptr->mode.mode & d))
  {
    chptr->mode.mode |= d;

    mode_changes[mode_count].letter = c;
    mode_changes[mode_count].dir = MODE_ADD;
    mode_changes[mode_count].caps = 0;
    mode_changes[mode_count].nocaps = 0;
    mode_changes[mode_count].id = NULL;
    mode_changes[mode_count].mems = ALL_MEMBERS;
    mode_changes[mode_count++].arg = NULL;
  }
  else if (dir == MODE_DEL && (chptr->mode.mode & d))
  {
    /* setting - */

    chptr->mode.mode &= ~d;

    mode_changes[mode_count].letter = c;
    mode_changes[mode_count].dir = MODE_DEL;
    mode_changes[mode_count].caps = 0;
    mode_changes[mode_count].nocaps = 0;
    mode_changes[mode_count].mems = ALL_MEMBERS;
    mode_changes[mode_count].id = NULL;
    mode_changes[mode_count++].arg = NULL;
  }
}

static void
chm_ban(struct Client *client_p, struct Client *source_p,
        struct Channel *chptr, int parc, int *parn,
        char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  char *mask = NULL;
  const char *rawmask;
  const char *realmask;

  if (dir == MODE_QUERY || parc <= *parn)
  {
    dlink_node *ptr = NULL;

    if (*errors & SM_ERR_RPL_B)
      return;

    *errors |= SM_ERR_RPL_B;

    DLINK_FOREACH(ptr, chptr->banlist.head)
    {
      struct Ban *banptr = ptr->data;
      sendto_one(client_p, form_str(RPL_BANLIST),
                 me.name, client_p->name, chptr->chname,
                 banptr->banstr,
                 banptr->who, banptr->when);
    }

    sendto_one(source_p, form_str(RPL_ENDOFBANLIST), me.name,
               source_p->name, chptr->chname);
    return;
  }

  if (alev < CHACCESS_HALFOP)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if (MyClient(source_p) && (++mode_limit > MAXMODEPARAMS))
    return;

  int arn = *parn;
  ++*parn;
  rawmask = parv[arn];

  if (EmptyString(rawmask) || *rawmask == ':')
    return;

  if (IsServer(client_p))
    if (strchr(rawmask, ' '))
      return;

  switch (dir)
  {
    case MODE_ADD:
    {
      struct Ban *ban = add_id(source_p, chptr, rawmask, CHFL_BAN);
      if (!ban)
        return;
      realmask = ban->banstr;
      break;
    }
    case MODE_DEL:
    {
      realmask = del_id(chptr, rawmask, CHFL_BAN);
      if (!realmask)
        return;
      break;
    }
    default:
      assert(0);
      return;
  }

  mask = nuh_mask[arn];
  strlcpy(mask, realmask, sizeof(nuh_mask[arn]));

  mode_changes[mode_count].letter = c;
  mode_changes[mode_count].dir = dir;
  mode_changes[mode_count].caps = 0;
  mode_changes[mode_count].nocaps = 0;
  mode_changes[mode_count].mems = ALL_MEMBERS;
  mode_changes[mode_count].id = NULL;
  mode_changes[mode_count++].arg = mask;
}

static void
chm_except(struct Client *client_p, struct Client *source_p,
           struct Channel *chptr, int parc, int *parn,
           char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  char *mask = NULL;
  const char *rawmask;
  const char *realmask;

  if ((dir == MODE_QUERY || parc <= *parn) && (alev >= CHACCESS_HALFOP || HasUMode(source_p, UMODE_OPER)))
  {
    dlink_node *ptr = NULL;

    if (*errors & SM_ERR_RPL_E)
      return;

    *errors |= SM_ERR_RPL_E;

    DLINK_FOREACH(ptr, chptr->exceptlist.head)
    {
      struct Ban *banptr = ptr->data;
      sendto_one(client_p, form_str(RPL_EXCEPTLIST),
                 me.name, client_p->name, chptr->chname,
                 banptr->banstr,
                 banptr->who, banptr->when);
    }

    sendto_one(source_p, form_str(RPL_ENDOFEXCEPTLIST), me.name,
               source_p->name, chptr->chname);
    return;
  }

  if (alev < CHACCESS_HALFOP)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if (MyClient(source_p) && (++mode_limit > MAXMODEPARAMS))
    return;

  int arn = *parn;
  ++*parn;
  rawmask = parv[arn];

  if (EmptyString(rawmask) || *rawmask == ':')
    return;

  if (IsServer(client_p))
    if (strchr(rawmask, ' '))
      return;

  switch (dir)
  {
    case MODE_ADD:
    {
      struct Ban *ban = add_id(source_p, chptr, rawmask, CHFL_EXCEPTION);
      if (!ban)
        return;
      realmask = ban->banstr;
      break;
    }
    case MODE_DEL:
    {
      realmask = del_id(chptr, rawmask, CHFL_EXCEPTION);
      if (!realmask)
        return;
      break;
    }
    default:
      assert(0);
      return;
  }

  mask = nuh_mask[arn];
  strlcpy(mask, realmask, sizeof(nuh_mask[arn]));

  mode_changes[mode_count].letter = c;
  mode_changes[mode_count].dir = dir;
  mode_changes[mode_count].caps = 0;
  mode_changes[mode_count].nocaps = 0;
  mode_changes[mode_count].mems = ONLY_CHANOPS;
  mode_changes[mode_count].id = NULL;
  mode_changes[mode_count++].arg = mask;
}

static void
chm_invex(struct Client *client_p, struct Client *source_p,
          struct Channel *chptr, int parc, int *parn,
          char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  char *mask = NULL;
  const char *rawmask;
  const char *realmask;

  if ((dir == MODE_QUERY || parc <= *parn) && (alev >= CHACCESS_HALFOP || HasUMode(source_p, UMODE_OPER)))
  {
    dlink_node *ptr = NULL;

    if (*errors & SM_ERR_RPL_I)
      return;

    *errors |= SM_ERR_RPL_I;

    DLINK_FOREACH(ptr, chptr->invexlist.head)
    {
      struct Ban *banptr = ptr->data;
      sendto_one(client_p, form_str(RPL_INVITELIST), me.name,
                 client_p->name, chptr->chname,
                 banptr->banstr,
                 banptr->who, banptr->when);
    }

    sendto_one(source_p, form_str(RPL_ENDOFINVITELIST), me.name,
               source_p->name, chptr->chname);
    return;
  }

  if (alev < CHACCESS_HALFOP)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if (MyClient(source_p) && (++mode_limit > MAXMODEPARAMS))
    return;

  int arn = *parn;
  ++*parn;
  rawmask = parv[arn];

  if (EmptyString(rawmask) || *rawmask == ':')
    return;

  if (IsServer(client_p))
    if (strchr(rawmask, ' '))
      return;

  switch (dir)
  {
    case MODE_ADD:
    {
      struct Ban *ban = add_id(source_p, chptr, rawmask, CHFL_INVEX);
      if (!ban)
        return;
      realmask = ban->banstr;
      break;
    }
    case MODE_DEL:
    {
      realmask = del_id(chptr, rawmask, CHFL_INVEX);
      if (!realmask)
        return;
      break;
    }
    default:
      assert(0);
      return;
  }

  mask = nuh_mask[arn];
  strlcpy(mask, realmask, sizeof(nuh_mask[arn]));

  mode_changes[mode_count].letter = c;
  mode_changes[mode_count].dir = dir;
  mode_changes[mode_count].caps = 0;
  mode_changes[mode_count].nocaps = 0;
  mode_changes[mode_count].mems = ONLY_CHANOPS;
  mode_changes[mode_count].id = NULL;
  mode_changes[mode_count++].arg = mask;
}

/*
 * inputs	- pointer to channel
 * output	- none
 * side effects	- clear ban cache
 */
void
clear_ban_cache(struct Channel *chptr)
{
  dlink_node *ptr = NULL;

  DLINK_FOREACH(ptr, chptr->localmembers.head)
  {
    struct Membership *ms = ptr->data;
    ms->flags &= ~(CHFL_BAN_SILENCED|CHFL_BAN_CHECKED|CHFL_MUTE_CHECKED);
  }
}

void
clear_ban_cache_client(struct Client *client_p)
{
  dlink_node *ptr = NULL;

  DLINK_FOREACH(ptr, client_p->channel.head)
  {
    struct Membership *ms = ptr->data;
    ms->flags &= ~(CHFL_BAN_SILENCED|CHFL_BAN_CHECKED|CHFL_MUTE_CHECKED);
  }
}

static int
check_mode_change(struct Client *source_p, struct Client *targ_p, struct Membership *member, int dir, int alev, int *errors, char c)
{
  if (dir != MODE_DEL)
    return 0;

  if (HasFlag(targ_p, FLAGS_SERVICE) && alev < CHACCESS_REMOTE)
  {
    if (!(*errors & SM_ERR_NOOPS))
    {
      char buf[NICKLEN + 30];
      snprintf(buf, sizeof(buf), "%s is a network service", targ_p->name);
      sendto_one(source_p, form_str(ERR_CANNOTCHANGECHANMODE),
          me.name, source_p->name, c, buf);
      *errors |= SM_ERR_NOOPS;
    }
    return 1;
  }

#if CHANAQ
  if (has_member_flags(member, CHFL_OWNER) && alev < CHACCESS_OWNER && source_p != targ_p)
  {
    if(!(*errors & SM_ERR_NOOPS))
    {
      char buf[NICKLEN + 30];
      sprintf(buf, "%s is a channel owner", targ_p->name);
      sendto_one(source_p, form_str(ERR_CANNOTCHANGECHANMODE),
          me.name, source_p->name, c, buf);
      *errors |= SM_ERR_NOOPS;
    }
    return 1;
  }

  if (has_member_flags(member, CHFL_PROTECTED) && alev < CHACCESS_OWNER && source_p != targ_p)
  {
    if (!(*errors & SM_ERR_NOOPS))
    {
      char buf[NICKLEN + 30];
      sprintf(buf, "%s is a channel admin", targ_p->name);
      sendto_one(source_p, form_str(ERR_CANNOTCHANGECHANMODE),
          me.name, source_p->name, c, buf);
      *errors |= SM_ERR_NOOPS;
    }
    return 1;
  }
#endif

  return 0;
}

static void
chm_voice(struct Client *client_p, struct Client *source_p,
          struct Channel *chptr, int parc, int *parn,
          char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  const char *opnick = NULL;
  struct Client *targ_p;
  struct Membership *member;

  if (alev < CHACCESS_HALFOP)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if ((dir == MODE_QUERY) || parc <= *parn)
    return;

  opnick = parv[(*parn)++];

  if ((targ_p = find_chasing(source_p, opnick)) == NULL)
    return;

  if ((member = find_channel_link(targ_p, chptr)) == NULL)
  {
    if (!(*errors & SM_ERR_NOTONCHANNEL))
      sendto_one(source_p, form_str(ERR_USERNOTINCHANNEL),
                 me.name, source_p->name, opnick, chptr->chname);
    *errors |= SM_ERR_NOTONCHANNEL;
    return;
  }

  if (MyClient(source_p) && (++mode_limit > MAXMODEPARAMS))
    return;

  /* no redundant mode changes */
  if (dir == MODE_ADD &&  has_member_flags(member, CHFL_VOICE))
    return;
  if (dir == MODE_DEL && !has_member_flags(member, CHFL_VOICE))
    return;

  if (check_mode_change(source_p, targ_p, member, dir, alev, errors, c))
    return;

  mode_changes[mode_count].letter = 'v';
  mode_changes[mode_count].dir = dir;
  mode_changes[mode_count].caps = 0;
  mode_changes[mode_count].nocaps = 0;
  mode_changes[mode_count].mems = ALL_MEMBERS;
  mode_changes[mode_count].id = targ_p->id;
  mode_changes[mode_count].arg = targ_p->name;
  mode_changes[mode_count++].client = targ_p;

  if (dir == MODE_ADD)
    AddMemberFlag(member, CHFL_VOICE);
  else
    DelMemberFlag(member, CHFL_VOICE);
}

#ifdef HALFOPS
static void
chm_hop(struct Client *client_p, struct Client *source_p,
       struct Channel *chptr, int parc, int *parn,
       char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  const char *opnick = NULL;
  struct Client *targ_p;
  struct Membership *member;

  /* don't allow halfops to set h if +p */
  if (alev < ((chptr->mode.mode & MODE_PRIVATE) ? CHACCESS_CHANOP : CHACCESS_HALFOP))
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if ((dir == MODE_QUERY) || (parc <= *parn))
    return;

  opnick = parv[(*parn)++];

  if ((targ_p = find_chasing(source_p, opnick)) == NULL)
    return;

  if ((member = find_channel_link(targ_p, chptr)) == NULL)
  {
    if (!(*errors & SM_ERR_NOTONCHANNEL))
      sendto_one(source_p, form_str(ERR_USERNOTINCHANNEL),
                 me.name, source_p->name, opnick, chptr->chname);
    *errors |= SM_ERR_NOTONCHANNEL;
    return;
  }

  if (MyClient(source_p) && (++mode_limit > MAXMODEPARAMS))
    return;

  /* no redundant mode changes */
  if (dir == MODE_ADD &&  has_member_flags(member, CHFL_HALFOP))
    return;
  if (dir == MODE_DEL && !has_member_flags(member, CHFL_HALFOP))
    return;

  if (check_mode_change(source_p, targ_p, member, dir, alev, errors, c))
    return;

  mode_changes[mode_count].letter = 'h';
  mode_changes[mode_count].dir = dir;
  mode_changes[mode_count].caps = 0;
  mode_changes[mode_count].nocaps = 0;
  mode_changes[mode_count].mems = ALL_MEMBERS;
  mode_changes[mode_count].id = targ_p->id;
  mode_changes[mode_count].arg = targ_p->name;
  mode_changes[mode_count++].client = targ_p;

  if (dir == MODE_ADD)
  {
    AddMemberFlag(member, CHFL_HALFOP);
    DelMemberFlag(member, CHFL_DEOPPED);
  }
  else
    DelMemberFlag(member, CHFL_HALFOP);
}
#endif

static void
chm_op(struct Client *client_p, struct Client *source_p,
       struct Channel *chptr, int parc, int *parn,
       char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  const char *opnick = NULL;
  struct Client *targ_p;
  struct Membership *member;

  if (alev < CHACCESS_CHANOP)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if ((dir == MODE_QUERY) || (parc <= *parn))
    return;

  opnick = parv[(*parn)++];

  if ((targ_p = find_chasing(source_p, opnick)) == NULL)
    return;

  if ((member = find_channel_link(targ_p, chptr)) == NULL)
  {
    if (!(*errors & SM_ERR_NOTONCHANNEL))
      sendto_one(source_p, form_str(ERR_USERNOTINCHANNEL), me.name,
                 source_p->name, opnick, chptr->chname);
    *errors |= SM_ERR_NOTONCHANNEL;
    return;
  }

  if (MyClient(source_p) && (++mode_limit > MAXMODEPARAMS))
    return;

  /* no redundant mode changes */
  if (dir == MODE_ADD &&  has_member_flags(member, CHFL_CHANOP))
    return;
  if (dir == MODE_DEL && !has_member_flags(member, CHFL_CHANOP))
    return;

  if (check_mode_change(source_p, targ_p, member, dir, alev, errors, c))
    return;

  mode_changes[mode_count].letter = 'o';
  mode_changes[mode_count].dir = dir;
  mode_changes[mode_count].caps = 0;
  mode_changes[mode_count].nocaps = 0;
  mode_changes[mode_count].mems = ALL_MEMBERS;
  mode_changes[mode_count].id = targ_p->id;
  mode_changes[mode_count].arg = targ_p->name;
  mode_changes[mode_count++].client = targ_p;

  if (dir == MODE_ADD)
  {
    AddMemberFlag(member, CHFL_CHANOP);
    DelMemberFlag(member, CHFL_DEOPPED);
  }
  else
    DelMemberFlag(member, CHFL_CHANOP);
}

#ifdef CHANAQ
static void
chm_protect(struct Client *client_p, struct Client *source_p,
       struct Channel *chptr, int parc, int *parn,
       char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  const char *opnick = NULL;
  struct Client *targ_p;
  struct Membership *member;

  if (alev < CHACCESS_PROTECTED)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if ((dir == MODE_QUERY) || (parc <= *parn))
    return;

  opnick = parv[(*parn)++];

  if ((targ_p = find_chasing(source_p, opnick)) == NULL)
    return;
  if (!IsClient(targ_p))
    return;

  if ((member = find_channel_link(targ_p, chptr)) == NULL)
  {
    if (!(*errors & SM_ERR_NOTONCHANNEL))
      sendto_one(source_p, form_str(ERR_USERNOTINCHANNEL), me.name,
                 source_p->name, opnick, chptr->chname);
    *errors |= SM_ERR_NOTONCHANNEL;
    return;
  }

  /* if source_p isnt +q then they can only proceed if target is themselves */
  if(alev < CHACCESS_OWNER && source_p != targ_p)
  {
    if(!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(ERR_CHANOWNPRIVNEEDED),
          me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if (MyClient(source_p) && (++mode_limit > MAXMODEPARAMS))
    return;

  /* no redundant mode changes */
  if (dir == MODE_ADD &&  has_member_flags(member, CHFL_PROTECTED))
    return;
  if (dir == MODE_DEL && !has_member_flags(member, CHFL_PROTECTED))
    return;

  if (check_mode_change(source_p, targ_p, member, dir, alev, errors, c))
    return;

  mode_changes[mode_count].letter = 'a';
  mode_changes[mode_count].dir = dir;
  mode_changes[mode_count].caps = 0;
  mode_changes[mode_count].nocaps = 0;
  mode_changes[mode_count].mems = ALL_MEMBERS;
  mode_changes[mode_count].id = targ_p->id;
  mode_changes[mode_count].arg = targ_p->name;
  mode_changes[mode_count++].client = targ_p;

  if (dir == MODE_ADD)
  {
    AddMemberFlag(member, CHFL_PROTECTED);
    DelMemberFlag(member, CHFL_DEOPPED);
  }
  else
    DelMemberFlag(member, CHFL_PROTECTED);
}

static void
chm_owner(struct Client *client_p, struct Client *source_p,
       struct Channel *chptr, int parc, int *parn,
       char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  const char *opnick = NULL;
  struct Client *targ_p;
  struct Membership *member;

  if (alev < CHACCESS_OWNER)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if ((dir == MODE_QUERY) || (parc <= *parn))
    return;

  opnick = parv[(*parn)++];

  if ((targ_p = find_chasing(source_p, opnick)) == NULL)
    return;
  if (!IsClient(targ_p))
    return;

  if ((member = find_channel_link(targ_p, chptr)) == NULL)
  {
    if (!(*errors & SM_ERR_NOTONCHANNEL))
      sendto_one(source_p, form_str(ERR_USERNOTINCHANNEL), me.name,
                 source_p->name, opnick, chptr->chname);
    *errors |= SM_ERR_NOTONCHANNEL;
    return;
  }

  if (MyClient(source_p) && (++mode_limit > MAXMODEPARAMS))
    return;

  /* no redundant mode changes */
  if (dir == MODE_ADD &&  has_member_flags(member, CHFL_OWNER))
    return;
  if (dir == MODE_DEL && !has_member_flags(member, CHFL_OWNER))
    return;

  if (check_mode_change(source_p, targ_p, member, dir, alev, errors, c))
    return;

  mode_changes[mode_count].letter = 'q';
  mode_changes[mode_count].dir = dir;
  mode_changes[mode_count].caps = 0;
  mode_changes[mode_count].nocaps = 0;
  mode_changes[mode_count].mems = ALL_MEMBERS;
  mode_changes[mode_count].id = targ_p->id;
  mode_changes[mode_count].arg = targ_p->name;
  mode_changes[mode_count++].client = targ_p;

  if (dir == MODE_ADD)
  {
    AddMemberFlag(member, CHFL_OWNER);
    DelMemberFlag(member, CHFL_DEOPPED);
  }
  else
    DelMemberFlag(member, CHFL_OWNER);
}
#endif

static void
chm_limit(struct Client *client_p, struct Client *source_p,
          struct Channel *chptr, int parc, int *parn,
          char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  unsigned int i;
  int limit;

  if (alev < CHACCESS_HALFOP)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if (dir == MODE_QUERY)
    return;

  if ((dir == MODE_ADD) && parc > *parn)
  {
    char *lstr = parv[(*parn)++];

    if (EmptyString(lstr) || (limit = atoi(lstr)) <= 0)
      return;

    sprintf(lstr, "%d", limit);

    /* if somebody sets MODE #channel +ll 1 2, accept latter --fl */
    for (i = 0; i < mode_count; i++)
      if (mode_changes[i].letter == c && mode_changes[i].dir == MODE_ADD)
        mode_changes[i].letter = 0;

    mode_changes[mode_count].letter = c;
    mode_changes[mode_count].dir = MODE_ADD;
    mode_changes[mode_count].caps = 0;
    mode_changes[mode_count].nocaps = 0;
    mode_changes[mode_count].mems = ALL_MEMBERS;
    mode_changes[mode_count].id = NULL;
    mode_changes[mode_count++].arg = lstr;

    chptr->mode.limit = limit;
  }
  else if (dir == MODE_DEL)
  {
    if (!chptr->mode.limit)
      return;

    chptr->mode.limit = 0;

    mode_changes[mode_count].letter = c;
    mode_changes[mode_count].dir = MODE_DEL;
    mode_changes[mode_count].caps = 0;
    mode_changes[mode_count].nocaps = 0;
    mode_changes[mode_count].mems = ALL_MEMBERS;
    mode_changes[mode_count].id = NULL;
    mode_changes[mode_count++].arg = NULL;
  }
}

static void
chm_key(struct Client *client_p, struct Client *source_p,
        struct Channel *chptr, int parc, int *parn,
        char **parv, int *errors, int alev, int dir, char c, unsigned int d)
{
  unsigned int i;

  if (alev < CHACCESS_HALFOP)
  {
    if (!(*errors & SM_ERR_NOOPS))
      sendto_one(source_p, form_str(alev == CHACCESS_NOTONCHAN ?
                                    ERR_NOTONCHANNEL : ERR_CHANOPRIVSNEEDED),
                 me.name, source_p->name, chptr->chname);
    *errors |= SM_ERR_NOOPS;
    return;
  }

  if (dir == MODE_QUERY)
    return;

  if ((dir == MODE_ADD) && parc > *parn)
  {
    char *key = parv[(*parn)++];

    fix_key(key);

    if (EmptyString(key))
      return;

    assert(key[0] != ' '); // fix_key removes spaces
    strlcpy(chptr->mode.key, key, sizeof(chptr->mode.key));

    /* if somebody does MODE #channel +kk a b, accept latter --fl */
    for (i = 0; i < mode_count; i++)
      if (mode_changes[i].letter == c && mode_changes[i].dir == MODE_ADD)
        mode_changes[i].letter = 0;

    mode_changes[mode_count].letter = c;
    mode_changes[mode_count].dir = MODE_ADD;
    mode_changes[mode_count].caps = 0;
    mode_changes[mode_count].nocaps = 0;
    mode_changes[mode_count].mems = ALL_MEMBERS;
    mode_changes[mode_count].id = NULL;
    mode_changes[mode_count++].arg = chptr->mode.key;
  }
  else if (dir == MODE_DEL)
  {
    if (parc > *parn)
      (*parn)++;

    if (chptr->mode.key[0] == '\0')
      return;

    chptr->mode.key[0] = '\0';

    mode_changes[mode_count].letter = c;
    mode_changes[mode_count].dir = MODE_DEL;
    mode_changes[mode_count].caps = 0;
    mode_changes[mode_count].nocaps = 0;
    mode_changes[mode_count].mems = ALL_MEMBERS;
    mode_changes[mode_count].id = NULL;
    mode_changes[mode_count++].arg = "*";
  }
}

struct ChannelMode mode_table[256] =
{
  ['B'] = {   chm_simple,    MODE_BWSAVER,    'B' },
  ['C'] = {   chm_simple,     MODE_NOCTCP,    'C' },
  ['I'] = {    chm_invex,               0,    'I' },
  ['M'] = {   chm_simple,     MODE_MODREG,    'M' },
  ['N'] = {   chm_simple,  MODE_NONOTICES,    'N' },
  ['O'] = { chm_operonly,   MODE_OPERONLY,    'O' },
  ['R'] = {   chm_simple,    MODE_REGONLY,    'R' },
  ['S'] = {   chm_simple,    MODE_SSLONLY,    'S' },
#ifdef CHANAQ
  ['a'] = {  chm_protect,               0,    'a' },
#endif
  ['b'] = {      chm_ban,               0,    'b' },
  ['c'] = {   chm_simple,     MODE_NOCTRL,    'c' },
  ['e'] = {   chm_except,               0,    'e' },
  ['h'] = {      chm_hop,               0,    'h' },
  ['i'] = {   chm_simple, MODE_INVITEONLY,    'i' },
  ['k'] = {      chm_key,               0,    'k' },
  ['l'] = {    chm_limit,               0,    'l' },
  ['m'] = {   chm_simple,  MODE_MODERATED,    'm' },
  ['n'] = {   chm_simple, MODE_NOPRIVMSGS,    'n' },
  ['o'] = {       chm_op,               0,    'o' },
  ['p'] = {   chm_simple,    MODE_PRIVATE,    'p' },
#ifdef CHANAQ
  ['q'] = {    chm_owner,               0,    'q' },
#endif
  ['s'] = {   chm_simple,     MODE_SECRET,    's' },
  ['t'] = {   chm_simple, MODE_TOPICLIMIT,    't' },
  ['v'] = {    chm_voice,               0,    'v' },
  ['z'] = {   chm_simple,    MODE_PERSIST,    'z' }
};

struct ChannelMode *
get_channel_mode(char c)
{
  return &mode_table[(unsigned char) c];
}

/* get_channel_access()
 *
 * inputs       - pointer to Client struct
 *              - pointer to Membership struct
 * output       - CHACCESS_CHANOP if we should let them have
 *                chanop level access, 0 for peon level access.
 * side effects - NONE
 */
static int
get_channel_access(const struct Client *source_p,
                   const struct Membership *member)
{
  /* Let hacked servers in for now... */
  if (!MyClient(source_p))
    return CHACCESS_REMOTE;

  if (member == NULL)
    return CHACCESS_NOTONCHAN;

  /* just to be sure.. */
  assert(source_p == member->client_p);

#ifdef CHANAQ
  if (has_member_flags(member, CHFL_OWNER))
    return CHACCESS_OWNER;

  if (has_member_flags(member, CHFL_PROTECTED))
    return CHACCESS_PROTECTED;
#endif

  if (has_member_flags(member, CHFL_CHANOP))
    return CHACCESS_CHANOP;

#ifdef HALFOPS
  if (has_member_flags(member, CHFL_HALFOP))
    return CHACCESS_HALFOP;
#endif

  return CHACCESS_PEON;
}

/* void send_cap_mode_changes(struct Client *client_p,
 *                        struct Client *source_p,
 *                        struct Channel *chptr, int cap, int nocap)
 * Input: The client sending(client_p), the source client(source_p),
 *        the channel to send mode changes for(chptr)
 * Output: None.
 * Side-effects: Sends the appropriate mode changes to capable servers.
 *
 * send_cap_mode_changes() will loop the server list itself, because
 * at this point in time we have 4 capabs for channels, CAP_IE, CAP_EX,
 * and a server could support any number of these..
 * so we make the modebufs per server, tailoring them to each servers
 * specific demand.  Its not very pretty, but its one of the few realistic
 * ways to handle having this many capabs for channel modes.. --fl_
 *
 * Reverted back to my original design, except that we now keep a count
 * of the number of servers which each combination as an optimisation, so
 * the capabs combinations which are not needed are not worked out. -A1kmm
 */
/* rewritten to ensure parabuf < MODEBUFLEN -db */

static void
send_cap_mode_changes(struct Client *client_p, struct Client *source_p,
                      struct Channel *chptr, unsigned int cap, unsigned int nocap)
{
  unsigned int i;
  int mbl, pbl, arglen, nc, mc;
  const char *arg = NULL;
  int dir = MODE_QUERY;

  mc = 0;
  nc = 0;
  pbl = 0;

  parabuf[0] = '\0';

  if ((cap & CAP_TS6) && source_p->id[0] != '\0')
    mbl = snprintf(modebuf, sizeof(modebuf), ":%s TMODE %lu %s ", source_p->id,
                   (unsigned long)chptr->channelts, chptr->chname);
  else
    mbl = snprintf(modebuf, sizeof(modebuf), ":%s MODE %s ", source_p->name,
                   chptr->chname);

  /* loop the list of - modes we have */
  for (i = 0; i < mode_count; i++)
  {
    /* if they dont support the cap we need, or they do support a cap they
     * cant have, then dont add it to the modebuf.. that way they wont see
     * the mode
     */
    if ((mode_changes[i].letter == 0) ||
        ((cap & mode_changes[i].caps) != mode_changes[i].caps) ||
        ((nocap & mode_changes[i].nocaps) != mode_changes[i].nocaps))
      continue;

    arg = "";

    if ((cap & CAP_TS6) && mode_changes[i].id)
      arg = mode_changes[i].id;
    if (*arg == '\0')
      arg = mode_changes[i].arg;

    /* if we're creeping past the buf size, we need to send it and make
     * another line for the other modes
     * XXX - this could give away server topology with uids being
     * different lengths, but not much we can do, except possibly break
     * them as if they were the longest of the nick or uid at all times,
     * which even then won't work as we don't always know the uid -A1kmm.
     */
    if (arg != NULL)
      arglen = strlen(arg);
    else
      arglen = 0;

    if ((mc == MAXMODEPARAMS) ||
        ((arglen + mbl + pbl + 2) > IRCD_BUFSIZE) ||
        (pbl + arglen + BAN_FUDGE) >= MODEBUFLEN)
    {
      if (nc != 0)
        sendto_server(client_p, cap, nocap, "%s %s", modebuf, parabuf);
      nc = 0;
      mc = 0;

      if ((cap & CAP_TS6) && source_p->id[0] != '\0')
        mbl = snprintf(modebuf, sizeof(modebuf), ":%s TMODE %lu %s ", source_p->id,
                       (unsigned long)chptr->channelts, chptr->chname);
      else
        mbl = snprintf(modebuf, sizeof(modebuf), ":%s MODE %s ", source_p->name,
                       chptr->chname);

      pbl = 0;
      parabuf[0] = '\0';
      dir = MODE_QUERY;
    }

    if (dir != mode_changes[i].dir)
    {
      modebuf[mbl++] = (mode_changes[i].dir == MODE_ADD) ? '+' : '-';
      dir = mode_changes[i].dir;
    }

    modebuf[mbl++] = mode_changes[i].letter;
    modebuf[mbl] = '\0';
    nc++;

    if (arg != NULL)
    {
      snprintf(parabuf + pbl, sizeof(parabuf) - pbl, "%s ", arg);
      pbl += arglen + 1;
      mc++;
    }
  }

  if (pbl && pbl <= sizeof(parabuf) && parabuf[pbl - 1] == ' ')
    parabuf[pbl - 1] = 0;

  if (nc != 0)
    sendto_server(client_p, cap, nocap, "%s %s", modebuf, parabuf);
}

/* void send_mode_changes(struct Client *client_p,
 *                        struct Client *source_p,
 *                        struct Channel *chptr)
 * Input: The client sending(client_p), the source client(source_p),
 *        the channel to send mode changes for(chptr),
 *        mode change globals.
 * Output: None.
 * Side-effects: Sends the appropriate mode changes to other clients
 *               and propagates to servers.
 */
/* ensure parabuf < MODEBUFLEN -db */
static void
send_mode_changes(struct Client *client_p, struct Client *source_p,
                  struct Channel *chptr)
{
  unsigned int i;
  int mbl, pbl, arglen, nc, mc;
  const char *arg = NULL;
  int dir = MODE_QUERY;

  /* bail out if we have nothing to do... */
  if (!mode_count)
    return;

  if (IsServer(source_p))
    mbl = snprintf(modebuf, sizeof(modebuf), ":%s MODE %s ", (IsHidden(source_p) ||
                   ConfigServerHide.hide_servers) ?
                   me.name : source_p->name, chptr->chname);
  else
    mbl = snprintf(modebuf, sizeof(modebuf), ":%s!%s@%s MODE %s ", source_p->name,
                   source_p->username, source_p->host, chptr->chname);

  mc = 0;
  nc = 0;
  pbl = 0;

  parabuf[0] = '\0';

  for (i = 0; i < mode_count; i++)
  {
    if (mode_changes[i].letter == 0 ||
        mode_changes[i].mems == NON_CHANOPS ||
        mode_changes[i].mems == ONLY_SERVERS)
      continue;

    arg = mode_changes[i].arg;
    if (arg != NULL)
      arglen = strlen(arg);
    else
      arglen = 0;

    if ((mc == MAXMODEPARAMS)  ||
        ((arglen + mbl + pbl + 2) > IRCD_BUFSIZE) ||
        ((arglen + pbl + BAN_FUDGE) >= MODEBUFLEN))
    {
      if (mbl && modebuf[mbl - 1] == '-')
        modebuf[mbl - 1] = '\0';

      if (nc != 0)
        sendto_channel_local(ALL_MEMBERS, 0, chptr, "%s %s", modebuf, parabuf);

      nc = 0;
      mc = 0;

      if (IsServer(source_p))
        mbl = snprintf(modebuf, sizeof(modebuf), ":%s MODE %s ", (IsHidden(source_p) ||
                       ConfigServerHide.hide_servers) ?
                       me.name : source_p->name, chptr->chname);
      else
        mbl = snprintf(modebuf, sizeof(modebuf), ":%s!%s@%s MODE %s ", source_p->name,
                       source_p->username, source_p->host, chptr->chname);

      pbl = 0;
      parabuf[0] = '\0';
      dir = MODE_QUERY;
    }

    if (dir != mode_changes[i].dir)
    {
      modebuf[mbl++] = (mode_changes[i].dir == MODE_ADD) ? '+' : '-';
      dir = mode_changes[i].dir;
    }

    modebuf[mbl++] = mode_changes[i].letter;
    modebuf[mbl] = '\0';
    nc++;

    if (arg != NULL)
    {
      snprintf(parabuf + pbl, sizeof(parabuf) - pbl, "%s ", arg);
      pbl += arglen + 1;
      mc++;
    }
  }

  if (pbl && pbl <= sizeof(parabuf) && parabuf[pbl - 1] == ' ')
    parabuf[pbl - 1] = 0;

  if (nc != 0)
    sendto_channel_local(ALL_MEMBERS, 0, chptr, "%s %s", modebuf, parabuf);

  nc = 0;
  mc = 0;

  /* Now send to servers... */
  for (i = 0; i < NCHCAP_COMBOS; i++)
    if (chcap_combos[i].count != 0)
      send_cap_mode_changes(client_p, source_p, chptr,
                            chcap_combos[i].cap_yes,
                            chcap_combos[i].cap_no);
}

/* void set_channel_mode(struct Client *client_p, struct Client *source_p,
 *               struct Channel *chptr, int parc, char **parv,
 *               char *chname)
 * Input: The client we received this from, the client this originated
 *        from, the channel, the parameter count starting at the modes,
 *        the parameters, the channel name.
 * Output: None.
 * Side-effects: Changes the channel membership and modes appropriately,
 *               sends the appropriate MODE messages to the appropriate
 *               clients.
 */
void
set_channel_mode(struct Client *client_p, struct Client *source_p, struct Channel *chptr,
                 struct Membership *member, int parc, char *parv[])
{
  int dir = MODE_ADD;
  int parn = 1;
  int alevel, errors = 0;
  char *ml = parv[0], c;

  mode_count = 0;
  mode_limit = 0;
  simple_modes_mask = 0;

  alevel = get_channel_access(source_p, member);

  for (; (c = *ml); ++ml)
  {
    switch (c)
    {
      case '+':
        dir = MODE_ADD;
        break;
      case '-':
        dir = MODE_DEL;
        break;
      case '=':
        dir = MODE_QUERY;
        break;
      default:
      {
        struct ChannelMode *tptr = get_channel_mode(c);
        if (tptr && tptr->func)
          tptr->func(client_p, source_p, chptr, parc, &parn,
                   parv, &errors, alevel, dir, c, tptr->mode);
        else
          chm_nosuch(client_p, source_p, chptr, parc, &parn,
              parv, &errors, alevel, dir, c, 0);
        break;
      }
    }
  }

  send_mode_changes(client_p, source_p, chptr);

  if (!PersistChannel(chptr) && !dlink_list_length(&chptr->members))
    destroy_channel(chptr);
}
