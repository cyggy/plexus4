/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file packet.c
 * \brief Packet handlers.
 * \version $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "s_bsd.h"
#include "conf.h"
#include "s_serv.h"
#include "client.h"
#include "ircd.h"
#include "parse.h"
#include "fdlist.h"
#include "packet.h"
#include "irc_string.h"
#include "memory.h"
#include "send.h"
#include "s_misc.h"

#define READBUF_SIZE 16384

static char readBuf[READBUF_SIZE];
static void client_dopacket(struct Client *, char *, size_t);

/* extract_one_line()
 *
 * inputs       - pointer to a dbuf queue
 *              - pointer to buffer to copy data to
 * output       - length of <buffer>
 * side effects - one line is copied and removed from the dbuf
 */
int
extract_one_line(struct dbuf_queue *qptr, char *buffer)
{
  int line_bytes = 0, eol_bytes = 0;
  dlink_node *ptr;

  DLINK_FOREACH(ptr, qptr->blocks.head)
  {
    struct dbuf_block *block = ptr->data;
    unsigned int idx;

    if (ptr == qptr->blocks.head)
      idx = qptr->pos;
    else
      idx = 0;

    for (; idx < block->size; idx++)
    {
      char c = block->data[idx];
      if (IsEol(c))
      {
        ++eol_bytes;
        /* allow 2 eol bytes per message */
        if (eol_bytes == 2)
          goto out;
      }
      else if (eol_bytes)
        goto out;
      else if (line_bytes++ < IRCD_BUFSIZE - 2)
        *buffer++ = c;
    }
  }

out:

  /*
   * Now, if we haven't found an EOL, ignore all line bytes
   * that we have read, since this is a partial line case.
   */
  if (eol_bytes)
    *buffer = 0;
  else
    line_bytes = 0;

  /* Remove what is now unnecessary */
  dbuf_delete(qptr, line_bytes + eol_bytes);

  return IRCD_MIN(line_bytes, IRCD_BUFSIZE - 2);
}

/*
 * parse_client_queued - parse client queued messages
 */
static void
parse_client_queued(struct Client *client_p)
{
  int dolen = 0;
  struct LocalUser *lclient_p = client_p->localClient;

  if (IsUnknown(client_p))
  {
    unsigned int allow_read = MAX_FLOOD_BURST;

    for (; ;)
    {
      if (IsDefunct(client_p))
        return;

      /* rate unknown clients at MAX_FLOOD per loop */

      if (lclient_p->sent_parsed >= allow_read)
        break;

      dolen = extract_one_line(&lclient_p->buf_recvq, readBuf);
      if (dolen == 0)
        break;

      client_dopacket(client_p, readBuf, dolen);
      lclient_p->sent_parsed++;

      /* if they've dropped out of the unknown state, break and move
       * to the parsing for their appropriate status.  --fl
       */
      if(!IsUnknown(client_p))
      {
        lclient_p->sent_parsed = 0;
        break;
      }
    }
  }

  if (IsServer(client_p) || IsConnecting(client_p) || IsHandshake(client_p))
  {
    while (1)
    {
      if (IsDefunct(client_p))
        return;
      if ((dolen = extract_one_line(&lclient_p->buf_recvq,
                                    readBuf)) == 0)
        break;
      client_dopacket(client_p, readBuf, dolen);
    }
  }
  else if (IsClient(client_p))
  {
    int checkflood = 1;
    unsigned int allow_read = IsFloodDone(client_p) ? MAX_FLOOD : MAX_FLOOD_BURST;
    unsigned int flood_burst = get_flood_burst(&lclient_p->confs);
    unsigned int soft_limit = get_soft_sendq(&lclient_p->confs);

    /* if a higher limit is set in the class, use it */
    if (flood_burst > allow_read)
      allow_read = flood_burst;

    if (ConfigFileEntry.no_oper_flood && HasUMode(client_p, UMODE_OPER))
      checkflood = 0;
    else if (IsCanFlood(client_p))
      checkflood = 0;

    /*
     * Handle flood protection here - if we exceed our flood limit on
     * messages in this loop, we simply drop out of the loop prematurely.
     *   -- adrian
     */
    for (; ;)
    {
      if (IsDefunct(client_p))
        break;

      /* This flood protection works as follows:
       *
       * A client is given allow_read lines to send to the server.  Every
       * time a line is parsed, sent_parsed is increased. 
       *
       * Thus a client can 'burst' allow_read lines to the server, any
       * excess lines will be parsed one per flood_recalc() call.
       *
       * Therefore a client will be penalised more if they keep flooding,
       * as sent_parsed will always hover around the allow_read limit
       * and no 'bursts' will be permitted.
       */
      if (checkflood)
      {
        if(lclient_p->sent_parsed >= allow_read)
          break;

        if (soft_limit && dbuf_length(&lclient_p->buf_sendq) > soft_limit)
          break;
      }

      dolen = extract_one_line(&lclient_p->buf_recvq, readBuf);
      if (dolen == 0)
        break;

      client_dopacket(client_p, readBuf, dolen);
      lclient_p->sent_parsed++;
    }
  }
}

/* flood_endgrace()
 *
 * marks the end of the clients grace period
 */
void
flood_endgrace(struct Client *client_p)
{
  /* Drop their flood limit back down */
  SetFloodDone(client_p);

  /* sent_parsed could be way over MAX_FLOOD but under MAX_FLOOD_BURST,
   * so reset it.
   */
  client_p->localClient->sent_parsed = 0;
}

void
flood_recalc(void *notused)
{
  dlink_node *ptr, *next_ptr;

  DLINK_FOREACH_SAFE(ptr, next_ptr, local_client_list.head)
  {
    struct Client *client_p = ptr->data;
    unsigned int flood_limit = get_flood_limit(&client_p->localClient->confs);

    if (!flood_limit)
      flood_limit = 2;

    /* allow a bursting client their allocation per second, allow
     * a client whos flooding an extra 2 per second
     */
    if (IsFloodDone(client_p))
      client_p->localClient->sent_parsed -= flood_limit;
    else
      client_p->localClient->sent_parsed = 0;

    if (client_p->localClient->sent_parsed < 0)
      client_p->localClient->sent_parsed = 0;

    /* NOTE: parse_client_queued(client_p) can call exit_client on next_ptr->data. this will dlinkDelete
     * next_ptr from local_client_list, which sets next_ptr->next to NULL. On the next loop, we will set
     * client_p to the now dead client, which is okay because parse_client_queued() checks IsDefunct(),
     * however any clients in the local list after this will be skipped on this flood recalc cycle.
     */

    parse_client_queued(client_p);
  }

  DLINK_FOREACH_SAFE(ptr, next_ptr, unknown_list.head)
  {
    struct Client *client_p = ptr->data;

    client_p->localClient->sent_parsed -= 2;
    if (client_p->localClient->sent_parsed < 0)
      client_p->localClient->sent_parsed = 0;

    parse_client_queued(client_p);
  }
}

/*
 * read_packet - Read a 'packet' of data from a connection and process it.
 */
void
read_packet(fde_t *fd, void *data)
{
  struct Client *client_p = data;
  int length = 0;

  if (IsDefunct(client_p))
    return;

  do
  {
    /*
     * Read some data. We *used to* do anti-flood protection here, but
     * I personally think it makes the code too hairy to make sane.
     *     -- adrian
     */
    if (fd->ssl)
    {
      ERR_clear_error();
      length = SSL_read(fd->ssl, readBuf, READBUF_SIZE);

      /* translate openssl error codes, sigh */
      if (length < 0)
        switch (SSL_get_error(fd->ssl, length))
        {
          case SSL_ERROR_WANT_WRITE:
            comm_setselect(fd, COMM_SELECT_WRITE, (PF *) sendq_unblocked, client_p);
            return;
          case SSL_ERROR_WANT_READ:
              errno = EWOULDBLOCK;
          case SSL_ERROR_SYSCALL:
              break;
          case SSL_ERROR_SSL:
            if (errno == EAGAIN)
              break;
          default:
            length = errno = 0;
        }
    }
    else
    {
      length = recv(fd->fd, readBuf, READBUF_SIZE, 0);
    }

    if (length <= 0)
    {
      if (length < 0 && ignoreErrno(errno))
      {
        comm_setselect(fd, COMM_SELECT_READ, read_packet, client_p);
        return;
      }

      dead_link_on_read(client_p, length);
      return;
    }

    dbuf_put(&client_p->localClient->buf_recvq, readBuf, length);

    if (client_p->localClient->lasttime < CurrentTime)
      client_p->localClient->lasttime = CurrentTime;
    if (client_p->localClient->lasttime > client_p->localClient->since)
      client_p->localClient->since = CurrentTime;
    ClearPingSent(client_p);

    /* Attempt to parse what we have */
    parse_client_queued(client_p);

    if (IsDefunct(client_p))
      return;

    if (IsServer(client_p) && HasSFlag(client_p, SERVER_FLAGS_MIGRATING_BLOCK_READ))
      return;

    /* Check to make sure we're not flooding */
    if (!(IsServer(client_p) || IsHandshake(client_p) || IsConnecting(client_p))
        && (dbuf_length(&client_p->localClient->buf_recvq) >
            get_recvq(&client_p->localClient->confs)))
    {
      exit_client(client_p, client_p, "Excess Flood");
      return;
    }
  }
  while (length == sizeof(readBuf) || fd->ssl != NULL);

  /* If we get here, we need to register for another COMM_SELECT_READ */
  comm_setselect(fd, COMM_SELECT_READ, read_packet, client_p);
}

/*
 * client_dopacket - copy packet to client buf and parse it
 *      client_p - pointer to client structure for which the buffer data
 *             applies.
 *      buffer - pointr to the buffer containing the newly read data
 *      length - number of valid bytes of data in the buffer
 *
 * Note:
 *      It is implicitly assumed that dopacket is called only
 *      with client_p of "local" variation, which contains all the
 *      necessary fields (buffer etc..)
 */
static void
client_dopacket(struct Client *client_p, char *buffer, size_t length)
{
  /*
   * Update messages received
   */
  ++me.localClient->recv.messages;
  ++client_p->localClient->recv.messages;

  /*
   * Update bytes received
   */
  client_p->localClient->recv.bytes += length;
  me.localClient->recv.bytes += length;

  parse(client_p, buffer, buffer + length);
}
