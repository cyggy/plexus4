/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "client.h"
#include "send.h"
#include "conf.h"
#include "numeric.h"
#include "parse.h"
#include "ircd_defs.h"
#include "irc_string.h"
#include "memory.h"
#include "hash.h"
#include "s_serv.h"

static void
alias_msg_ignore(struct Message *msg, struct Client *source_p, int parc, char *parv[])
{
}

static void
alias_msg_unregistered(struct Message *msg, struct Client *source_p, int parc, char *parv[])
{
    m_unregistered(source_p->from, source_p, parc, parv);
}

static void
alias_msg_unknown(struct Message *msg, struct Client *source_p, int parc, char *parv[])
{
    sendto_one(source_p, form_str(ERR_UNKNOWNCOMMAND),
               me.name, source_p->name, parv[0]);
}

static void
alias_msg_dispatch(struct Message *msg, struct Client *source_p,
                   int parc, char *parv[])
{
    if (EmptyString(parv[1]))
    {
        sendto_one(source_p, form_str(ERR_NOTEXTTOSEND),
                   me.name, source_p->name);
        return;
    }

    struct alias_name *aname = (struct alias_name *) msg;
    struct alias_entry *amsg = aname->parent;

    const char *server = amsg->server;
    if (EmptyString(server))
        server = ConfigFileEntry.service_name;

    struct Client *target_p = find_person(source_p, amsg->user);
    struct Client *server_p = hash_find_server_name(server);
    if (target_p == NULL || server_p == NULL || IsMe(server_p) || target_p->servptr != server_p)
    {
      sendto_one(source_p, form_str(ERR_SERVICESDOWN),
                 me.name, source_p->name, amsg->user);
      return;
    }

    sendto_one(target_p, ":%s PRIVMSG %s@%s :%s",
               ID_or_name(source_p, target_p), target_p->name, server_p->name, parv[1]);
}

void
alias_remove(struct alias_entry *conf)
{
    dlink_node *ptr = NULL, *next_ptr = NULL;

    DLINK_FOREACH_SAFE(ptr, next_ptr, conf->names.head)
    {
        struct alias_name *nm = (struct alias_name *) ptr->data;

        if (!EmptyString(nm->name) && find_command(nm->name) == &nm->msg)
            mod_del_cmd(&nm->msg);

        dlinkDelete(ptr, &conf->names);
        MyFree(nm);
    }
}

void
alias_add(struct alias_entry *conf)
{
    dlink_node *ptr = NULL;

    /*
     * make sure the alias block has the required fields
     */
    if (EmptyString(conf->user))
        return;

    DLINK_FOREACH(ptr, conf->names.head)
    {
        struct alias_name *nm = (struct alias_name *) ptr->data;

        if (EmptyString(nm->name))
            continue;

        /* populate message info for this alias */
        nm->msg.cmd = nm->name;
        nm->msg.flags = MFLG_EX_DISPATCH;
        nm->msg.args_min = 1;

        /* handlers: { m_unregistered, m_hostserv, m_ignore, m_ignore, m_hostserv, m_ignore } */
        nm->msg.handlers[UNREGISTERED_HANDLER] = (MessageHandler) alias_msg_unregistered;

        nm->msg.handlers[CLIENT_HANDLER] = conf->flags & CONF_FLAGS_OPER_ONLY  ?
                                           (MessageHandler) alias_msg_unknown  :
                                           (MessageHandler) alias_msg_dispatch;

        nm->msg.handlers[SERVER_HANDLER] = (MessageHandler) alias_msg_ignore;
        nm->msg.handlers[ENCAP_HANDLER] = (MessageHandler) alias_msg_ignore;
        nm->msg.handlers[OPER_HANDLER] = (MessageHandler) alias_msg_dispatch;

        mod_add_cmd(&nm->msg);
    }
}
