/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

#include <jansson.h>
#include <setjmp.h>

/* symbols imported by libplexus for upgrade stuff */
json_t *plexus_upgrade;
jmp_buf plexus_upgrade_jmp;

static void
add_testcases(Suite *s)
{
  invite_setup(s);
  join_setup(s);
  mode_setup(s);
  upgrade_setup(s);
  dnsbl_setup(s);
  nick_setup(s);
  extban_setup(s);
  session_setup(s);
  privmsg_setup(s);
  ping_setup(s);
  webirc_setup(s);
  cloak_setup(s);
  hostmask_setup(s);
}

int
main()
{
  Suite *s = suite_create("plexus");

  add_testcases(s);

  SRunner *runner = srunner_create(s);
  srunner_set_tap(runner, "-");
  srunner_run_all(runner, CK_NORMAL);

  int number_failed = srunner_ntests_failed(runner);
  srunner_free(runner);

  return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
