/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *
 *  Copyright (C) 2016 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

static struct MaskItem *
session_find(const char *mask)
{
  struct irc_ssaddr addr;
  int bits;

  int masktype = parse_netmask(mask, &addr, &bits);

  struct MaskLookup lookup = {
    .name = mask,
    .fam = masktype != HM_HOST ? addr.ss.ss_family : 0,
    .addr = masktype != HM_HOST ? &addr : NULL,
    .cmpfunc = irccmp
  };

  return find_conf_by_address(CONF_SESSION, &lookup);
}


START_TEST(session_test)
{
  struct PlexusClient *server = server_register("plexus4.2");

  struct MaskItem *conf = conf_make(CONF_ULINE);
  conf->flags = SHARED_SESSION;
  conf->name = xstrdup(server->name);

  const time_t time = 123456789;

  io_write(server, "ENCAP %s SESSION %s %d %s %lu %lu :%s",
           me.name, "test.mask", 42, "Adam", time, time, "test session");

  expect_pingwait(server, &me);

  struct MaskItem *session = session_find("test.mask");
  ck_assert_ptr_ne(session, NULL);

  ck_assert_int_eq(session->count, 42);
  ck_assert_str_eq(session->user, "Adam");
  ck_assert_str_eq(session->host, "test.mask");
  ck_assert_int_eq(session->setat, time);
  ck_assert_int_eq(session->until, time);
  ck_assert_str_eq(session->reason, "test session");

  io_write(server, "ENCAP %s UNSESSION %s",
           me.name, "test.mask");

  expect_pingwait(server, &me);

  session = session_find("test.mask");
  ck_assert_ptr_eq(session, NULL);
}
END_TEST

void
session_setup(Suite *s)
{
  TCase *tc = tcase_create("session");

  tcase_add_checked_fixture(tc, plexus_up, plexus_down);
  tcase_add_test(tc, session_test);

  suite_add_tcase(s, tc);
}
