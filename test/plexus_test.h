#include "stdinc.h"
#include "listener.h"
#include "s_bsd.h"
#include "memory.h"
#include "client.h"
#include "irc_string.h"
#include "packet.h"
#include "numeric.h"
#include "hash.h"
#include "ircd.h"
#include "restart.h"
#include "conf.h"
#include "s_serv.h"
#include "channel_mode.h"
#include "upgrade.h"
#include "channel.h"
#include "hostmask.h"

#include <check.h>

#define WAIT_FOR(cond) while (!(cond)) io_loop()

struct PlexusClient
{
  struct Client *client;

  char name[HOSTLEN + 1]; /**< unique name for a client nick or host */
  fde_t         fd;
  //struct dbuf_queue buf_sendq;
  struct dbuf_queue buf_recvq;
};

extern void plexus_up(void);
extern void plexus_up_conf(const char *);
extern void plexus_down(void);

extern void io_write(struct PlexusClient *client, const char *buf, ...);
extern void io_read(fde_t *fd, struct PlexusClient *client);

extern struct PlexusClient *client_create(const char *);
extern struct PlexusClient *client_create_ip(const char *, const char *);
extern struct PlexusClient *client_register(const char *);
extern struct PlexusClient *client_register_host(const char *, const char *);

extern struct PlexusClient *server_create(const char *name);
extern struct PlexusClient *server_register(const char *name);

extern void expect(struct PlexusClient *client, const char *str);
extern void expect_numeric(struct PlexusClient *client, enum irc_numerics numeric);
extern void expect_message(struct PlexusClient *client, struct Client *from, const char *message);
extern void expect_message_args(struct PlexusClient *client, struct Client *from, const char *fmt, ...);
extern void expect_pingwait(struct PlexusClient *client, struct Client *to);
extern void expect_nickchange_to(struct PlexusClient *client, struct Client *from, const char *to);
extern void expect_nickchange_from(struct PlexusClient *client, struct Client *to, const char *from);

extern struct irc_ssaddr ip_parse(const char *);

extern void irc_client_set_cmode(struct PlexusClient *client, struct Channel *chptr, const char *modes);
extern void irc_client_set_umode(struct PlexusClient *client, const char *modes);
extern void irc_invite(struct PlexusClient *from, struct Channel *chptr, struct Client *to);
extern void irc_join(struct PlexusClient *client, const char *channel);
extern void irc_join_key(struct PlexusClient *client, const char *channel, const char *key);
extern void irc_part(struct PlexusClient *client, const char *channel);
extern void irc_privmsg(struct PlexusClient *client, const char *target, const char *message);
extern void irc_nick(struct PlexusClient *client, const char *newnick);
extern void irc_notice(struct PlexusClient *client, const char *target, const char *message);
extern void irc_ping(struct PlexusClient *client, const char *origin, const char *destination);

extern void plexus_fork(void (*callback)());
extern bool plexus_branch(void);
extern void plexus_join_all_and_exit(void);

extern void invite_setup(Suite *);
extern void join_setup(Suite *);
extern void mode_setup(Suite *);
extern void upgrade_setup(Suite *);
extern void dnsbl_setup(Suite *);
extern void nick_setup(Suite *s);
extern void extban_setup(Suite *s);
extern void session_setup(Suite *s);
extern void privmsg_setup(Suite *s);
extern void ping_setup(Suite *s);
extern void webirc_setup(Suite *s);
extern void cloak_setup(Suite *s);
extern void hostmask_setup(Suite *s);

#define tlog(fmt, ...) plexus_log(__FILE__, __LINE__, fmt, ##__VA_ARGS__)
extern void plexus_log(const char *, int, const char *, ...);
